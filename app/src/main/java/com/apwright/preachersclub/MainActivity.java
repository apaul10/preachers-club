package com.apwright.preachersclub;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.apwright.preachersclub.fragments.ActivityFeedFragment;
import com.apwright.preachersclub.fragments.PlaceholderFragment;
import com.apwright.preachersclub.fragments.PresenterViewFragment;
import com.apwright.preachersclub.fragments.SermonViewFragment;
import com.apwright.preachersclub.fragments.TutorialViewFragment;
import com.apwright.preachersclub.model.CustomerViewPager;
import com.apwright.preachersclub.model.DifficultQuestion;
import com.apwright.preachersclub.model.User;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.ErrorCodes;
import com.firebase.ui.auth.IdpResponse;
import com.firebase.ui.auth.ResultCodes;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static int RC_SIGN_IN = 1001;

    CustomerViewPager mViewPager;
    TabLayout mTabLayout;
    Adapter mAdapter;
    Toolbar mToolbar;
    AppBarLayout mAppBarLayout;

    FirebaseAuth mAuth;
    FirebaseUser mUser;
    View parentLayout;

    MenuItem mSignInMenuItem;
    MenuItem mSignOutMenuItem;

    FloatingActionMenu mFloatingActionMenu;
    FloatingActionButton mAddSermonFAB;
    FloatingActionButton mAddTutorialFAB;
    FloatingActionButton mAddContributorFAB;
    FloatingActionButton mAddAssignmentFAB;
    FloatingActionButton mAddUsefulResourceFAB;
    FloatingActionButton mAddDifficultQuestionFAB;
    FloatingActionButton mAddPresentationFAB;

    View mShadowView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        //apply dynamic theme
        if (PreachersApp.enableThemeChange) {
            Utility.onActivityCreateSetTheme(this);
        }

        //load layout
        setContentView(R.layout.activity_main);

        parentLayout = findViewById(R.id.parentLayout);

        //get FireBaseUI Auth
        if (!isUserSignedIn() || mUser == null) {
            signIn();
        }

        //configure toolbars
        setupToolbar();

        //configure viewpager - loads fragments into tabs
        setupViewPager();

        //configure tabs based on viewpager
        setupTabs();

        //configure floating action menu
        setupFloatingActionMenu();


//      Toast.makeText(getApplicationContext(), PreachersApp.currentTab + " selected", Toast.LENGTH_SHORT).show();


    }

    private void setupFloatingActionMenu() {
        mShadowView = findViewById(R.id.shadowView);
        mShadowView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (mFloatingActionMenu.isOpened()) {
                    mFloatingActionMenu.close(true);
                    return true;
                }
                return false;
            }
        });

        mFloatingActionMenu = (FloatingActionMenu) findViewById(R.id.floating_action_menu);
        mFloatingActionMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                mShadowView.setVisibility((opened ? View.VISIBLE : View.GONE));
            }
        });

        mAddTutorialFAB = (FloatingActionButton) findViewById(R.id.fabAddTutorial);
        mAddTutorialFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddTutorial();
            }
        });

        mAddContributorFAB = (FloatingActionButton) findViewById(R.id.fabAddContributor);
        mAddContributorFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddContributor();
            }
        });

        mAddAssignmentFAB = (FloatingActionButton) findViewById(R.id.fabAddAssignment);
        mAddAssignmentFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddAssignment();
            }
        });

        mAddUsefulResourceFAB = (FloatingActionButton) findViewById(R.id.fabAddUsefulResource);
        mAddUsefulResourceFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddUsefulResource();
            }
        });

        mAddDifficultQuestionFAB = (FloatingActionButton) findViewById(R.id.fabAddDifficultQuestion);
        mAddDifficultQuestionFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddDifficultQuestion();
            }
        });

        mAddSermonFAB = (FloatingActionButton) findViewById(R.id.fabAddSermon);
        mAddSermonFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSermon();
            }
        });

        mAddPresentationFAB = (FloatingActionButton) findViewById(R.id.fabAddPresentation);
        mAddPresentationFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPresentation();
            }
        });

    }

    public void closeFloatingActionMenu() {
        if (PreachersApp.closeFAB) {
            mFloatingActionMenu.close(true);
        }
    }

    private void openAddDifficultQuestion() {
        Intent intent = new Intent(getApplicationContext(), DifficultQuestionEditActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    private void openAddUsefulResource() {
        Intent intent = new Intent(getApplicationContext(), UsefulResourceEditActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    private void openAddAssignment() {
        Intent intent = new Intent(getApplicationContext(), AssignmentEditActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    private void openAddTutorial() {
        Intent intent = new Intent(getApplicationContext(), TutorialEditActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    private void openSermon() {
        Intent intent = new Intent(getApplicationContext(), SermonEditActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    private void openPresentation() {
        Intent intent = new Intent(getApplicationContext(), PresentationCreateActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    private void openAddContributor() {
        Intent intent = new Intent(getApplicationContext(), ContributorEditActivity.class);
        startActivity(intent);
        closeFloatingActionMenu();
    }

    public void signIn() {
        //FireBaseUI Sign-in Function
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder()
                        .setTheme(getFireBaseUITheme())
                        .setProviders(getFireBaseUIProviders())
                        .setIsSmartLockEnabled(true)
                        //.setLogo(R.drawable.firebase_theme_login_image)
                        .build(),
                RC_SIGN_IN);
    }

    private void removeAccount() {
        //FireBaseUI Delete Account
        AuthUI.getInstance()
                .delete(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Utility.showSnackBar(parentLayout, getString(R.string.account_removed));
                        } else {
                            Utility.showSnackBar(parentLayout, getString(R.string.account_not_removed));
                        }
                    }
                });
    }

    public void signOut() {
        //FireBaseUI Sign-Out Function
        AuthUI.getInstance()
                .signOut(this)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        //user is now signed out
                        Utility.showSnackBar(parentLayout, getString(R.string.user_signed_out));
                        updateSignInStatus();

                    }
                });
    }

    public boolean isUserSignedIn() {
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        if (mUser != null) {
            return true;
        } else {
            return false;
        }
    }

    public void updateSignInStatus() {

        if (isUserSignedIn()) {
            mSignInMenuItem.setVisible(false);
            mSignOutMenuItem.setVisible(true);
        } else {
            mSignInMenuItem.setVisible(true);
            mSignOutMenuItem.setVisible(false);
        }
    }

    private List<AuthUI.IdpConfig> getFireBaseUIProviders() {
        return Arrays.asList(
                new AuthUI.IdpConfig.Builder(AuthUI.EMAIL_PROVIDER).build()
                , new AuthUI.IdpConfig.Builder(AuthUI.GOOGLE_PROVIDER).build()
                //, new AuthUI.IdpConfig.Builder(AuthUI.FACEBOOK_PROVIDER).build()
        );
    }

    private int getFireBaseUITheme() {
        return R.style.AppTheme;
    }

    private void updateUserAccount(final User user) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference userReference = database
                .getReference()
                .child(Utility.REFERENCE_USERS)
                .child(user.getUserId());

        userReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.exists()) {
                    //user account does not exist - add new account
                    Utility.showSnackBar(parentLayout, "Setting up Account");

                    userReference.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Utility.showSnackBar(parentLayout, "Setting up Account - Complete");
                            }
                        }
                    });

                } else {
                    //Utility.showSnackBar(parentLayout, "Account already setup");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            //sign-in successful
            if (resultCode == ResultCodes.OK) {
                //update user information
                mUser = mAuth.getCurrentUser();
                User user = new User();
                user.setUserId(mUser.getUid());
                user.setDisplayName(mUser.getDisplayName());
                user.setEmailAddress(mUser.getEmail());
                user.setProvider(mUser.getProviders().toString());
                if (mUser.getPhotoUrl() != null) {
                    user.setPhotoUrl(mUser.getPhotoUrl().toString());
                }
                updateUserAccount(user);

                //get user information
                updateSignInStatus();

                Utility.showSnackBar(parentLayout, getString(R.string.sign_in_success));
                return;

            } else {
                //sign-in failed
                if (response == null) {
                    //user pressed back button, sign in cancelled
                    Utility.showSnackBar(parentLayout, getString(R.string.sign_in_cancelled));
                    return;
                }

                //internet connection not available
                if (response.getErrorCode() == ErrorCodes.NO_NETWORK) {

                    Snackbar.make(parentLayout,
                            getString(R.string.no_internet_connection),
                            Snackbar.LENGTH_LONG
                    ).setAction(R.string.retry, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            signIn();
                        }
                    }).show();

                    return;
                }

                //unknown error occured
                if (response.getErrorCode() == ErrorCodes.UNKNOWN_ERROR) {
                    Utility.showSnackBar(parentLayout, getString(R.string.unknown_error));
                }
            }

            //something really wrong if we get here
            Utility.showSnackBar(parentLayout, getString(R.string.unknown_sign_in_response));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();

        menuInflater.inflate(R.menu.menu_main, menu);

        mSignInMenuItem = menu.findItem(R.id.menu_sign_in);
        mSignOutMenuItem = menu.findItem(R.id.menu_sign_out);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_help:
                openHelp();
                break;

            case R.id.menu_sign_in:
                signIn();
                break;

            case R.id.menu_sign_out:
                signOut();
                break;

            case R.id.test_activity:

                String verse = "John 3:16, Rev 12:14-16;";

                Utility.openBibleVerse(getApplicationContext(), verse);

//                DifficultQuestion difficultQuestion = DifficultQuestion.getSampleData();
//
//                FirebaseDatabase database = FirebaseDatabase.getInstance();
//                DatabaseReference reference = database.getReference().child(Utility.REFERENCE_QUESTIONS);
//
//                String key = reference.push().getKey();
//
////                reference.child(key).setValue("test value");
//                reference.child(key).setValue(difficultQuestion);


//                Toast.makeText(getApplicationContext(), "added", Toast.LENGTH_LONG).show();

                break;

        }
        return true;
    }

    private void openHelp() {
        Intent intent = new Intent(getApplicationContext(), HelpActivity.class);
        intent.putExtra(HelpActivity.IGNORE_PREFERENCE, true);
        startActivity(intent);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        updatePageTitle();
    }

    private void setupToolbar() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.appbar);
        setSupportActionBar(mToolbar);
    }

    private void setupViewPager() {
        mViewPager = (CustomerViewPager) findViewById(R.id.viewpager);

        mAdapter = new Adapter(getSupportFragmentManager());
        mAdapter.addFragment(new ActivityFeedFragment(), "Home", R.drawable.ic_home);
        mAdapter.addFragment(new TutorialViewFragment(), "Tutorials", R.drawable.ic_flashlight);
        mAdapter.addFragment(new SermonViewFragment(), "Sermons", R.drawable.ic_book);
        mAdapter.addFragment(new PresenterViewFragment(), "Presenter", R.drawable.ic_tv);

        mViewPager.setAdapter(mAdapter);
    }

    private void setupTabs() {
        mTabLayout = (TabLayout) findViewById(R.id.tabs);
        mTabLayout.setupWithViewPager(mViewPager);

        //set icons for each tab
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            mTabLayout.getTabAt(i).setIcon(mAdapter.getIconId(i));
            mTabLayout.getTabAt(i).setText("");
        }

        //select tab after activity reload
        if (PreachersApp.enableThemeChange) {
            mTabLayout.getTabAt(PreachersApp.currentTab).select();
        }


        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();

                if (position != PreachersApp.currentTab && PreachersApp.enableThemeChange) {
                    Utility.changeToTheme(MainActivity.this, position);
                }

                mAppBarLayout.setExpanded(true);

                if (!PreachersApp.enableSwipeOnHomePage) {
                    //disable page swiping when on the HOME TAB
                    if (position == 0) {
                        mViewPager.setSwipingEnabled(false);
                    } else {
                        mViewPager.setSwipingEnabled(true);
                    }
                }

                //update application tab position
                PreachersApp.currentTab = position;

                updatePageTitle();

                updateColors(position);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.setText("");
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void updateColors(int position) {
        position += 1;

        int colorPrimary;
        int colorPrimaryDark;
        int colorAccent;

        switch (position) {
            default:
            case 1:
                colorPrimary = getResources().getColor(R.color.colorPrimary);
                colorPrimaryDark = getResources().getColor(R.color.colorPrimaryDark);
                colorAccent = getResources().getColor(R.color.colorAccent);
                break;
            case 2:
                colorPrimary = getResources().getColor(R.color.colorPrimary2);
                colorPrimaryDark = getResources().getColor(R.color.colorPrimaryDark2);
                colorAccent = getResources().getColor(R.color.colorAccent2);
                break;
            case 3:
                colorPrimary = getResources().getColor(R.color.colorPrimary3);
                colorPrimaryDark = getResources().getColor(R.color.colorPrimaryDark3);
                colorAccent = getResources().getColor(R.color.colorAccent3);
                break;
            case 4:
                colorPrimary = getResources().getColor(R.color.colorPrimary4);
                colorPrimaryDark = getResources().getColor(R.color.colorPrimaryDark4);
                colorAccent = getResources().getColor(R.color.colorAccent4);
                break;
        }

        if (mAppBarLayout != null) {
            mAppBarLayout.setBackgroundColor(colorPrimary);
        }

        if (mToolbar != null) {
            mToolbar.setBackgroundColor(colorPrimary);
        }

        if (mTabLayout != null) {
            mTabLayout.setSelectedTabIndicatorColor(colorAccent);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            //window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(colorPrimaryDark);
        }

        if (mFloatingActionMenu != null) {
            mFloatingActionMenu.setMenuButtonColorNormal(colorAccent);
            mFloatingActionMenu.setMenuButtonColorPressed(colorPrimary);
        }
    }

    private void updatePageTitle() {
        int position = PreachersApp.currentTab;
        String title = mAdapter.getPageTitle(position).toString();

        //update title for toolbar
        if (position <= 0) {
            mToolbar.setTitle("Preachers Club");
        } else {
            mToolbar.setTitle(title);
        }

        if (PreachersApp.setTitleForTabs) {
            //set text for selected tab
            mTabLayout
                    .getTabAt(PreachersApp.currentTab)
                    .setText(mAdapter.getPageTitle(PreachersApp.currentTab));
        }

    }

    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final List<Integer> mFragmentIconList = new ArrayList<>();

        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title, Integer iconId) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            mFragmentIconList.add(iconId);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        public Integer getIconId(int position) {
            return mFragmentIconList.get(position);
        }
    }
}
