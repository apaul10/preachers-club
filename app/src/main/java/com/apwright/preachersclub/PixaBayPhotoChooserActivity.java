package com.apwright.preachersclub;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.apwright.preachersclub.adapters.ImageContentAdapter;
import com.apwright.preachersclub.api.PixaBayResponse;
import com.apwright.preachersclub.model.ImageContent;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;

public class PixaBayPhotoChooserActivity extends AppCompatActivity {

    ArrayList<ImageContent> mImageContents;

    RecyclerView mRecyclerView;
    ImageContentAdapter mAdapter;

    ProgressDialog mProgressDialog;

    FloatingSearchView mFloatingSearchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_chooser);

        mFloatingSearchView = (FloatingSearchView) findViewById(R.id.floating_search_view);
        mFloatingSearchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {
            @Override
            public void onSearchTextChanged(String oldQuery, String newQuery) {
                if (oldQuery != newQuery) {
                    //Toast.makeText(getBaseContext(), "searching for: " + newQuery, Toast.LENGTH_LONG).show();
                }
            }
        });
        mFloatingSearchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(SearchSuggestion searchSuggestion) {

            }

            @Override
            public void onSearchAction(String currentQuery) {
//                Toast.makeText(getBaseContext(), "searching for: " + currentQuery, Toast.LENGTH_LONG).show();
                searchForImage(currentQuery);
            }
        });

        mImageContents = new ArrayList<ImageContent>();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        mRecyclerView = (RecyclerView) findViewById(R.id.photo_chooser_results_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
//        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(1, 1));
        mAdapter = new ImageContentAdapter(this, mImageContents);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void showProgressDialog() {
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void searchForImage(String searchTerm) {

        String url = ImageContent.getPixabaySearchURL(searchTerm);

        showProgressDialog();

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Gson gson = new Gson();
                PixaBayResponse pixabayResponse = gson.fromJson(response.toString(), PixaBayResponse.class);

                ArrayList<ImageContent> imageContents = new PixaBayResponse().convertToImageContentList(pixabayResponse);

                mAdapter.update(imageContents);

//                Log.i("PhotoChooser", response.toString());

                if (imageContents.size() <= 0) {
                    //Utility.showToast(getApplicationContext(), "No image(s) found! Try another search term");
                    Utility.showSnackBar(getCurrentFocus(), "No image(s) found! Try another search term", true);
                }

                hideProgressDialog();

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

//                Log.e("PhotoChooser", error.toString());

                //Utility.showToast(getApplicationContext(), "Oops! Something went wrong, try again later");
                Utility.showSnackBar(getCurrentFocus(), "Oops! Something went wrong, try again later", true);

                hideProgressDialog();

            }
        });

        // Access the RequestQueue through your singleton class.
        PreachersApp.getInstance().addToRequestQueue(jsonObjectRequest);

    }
}
