package com.apwright.preachersclub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

public class SermonSupportingPointEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sermon_supporting_point_edit);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Add Supporting Point</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
