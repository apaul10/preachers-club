package com.apwright.preachersclub.model;

/**
 * Created by apwright on 6/11/2017.
 */

public class WebMetaData {
    String title;
    String imageURL;
    String description;
    String webURL;

    public WebMetaData(String title, String imageURL, String description, String webURL) {
        this.title = title;
        this.imageURL = imageURL;
        this.description = description;
        this.webURL = webURL;
    }

    public WebMetaData() {
    }

    public WebMetaData(String title, String imageURL, String description) {
        this.title = title;
        this.imageURL = imageURL;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebURL() {
        return webURL;
    }

    public void setWebURL(String webURL) {
        this.webURL = webURL;
    }
}
