package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.apwright.preachersclub.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apwright on 8/8/2017.
 */

public class Sermon implements Parcelable {
    private String sermonTitle;
    private String sermonKeyText;
    private String sermonSubject;
    private String sermonComplement;
    private String sermonIdea;
    private String sermonTakeaway;

    public final static String SERMON_KEY = "sermon";
    public final static String SERMON_FIRE_BASE_KEY = "sermon_fb_key";


    public String getSermonTakeaway() {
        return sermonTakeaway;
    }

    public void setSermonTakeaway(String sermonTakeaway) {
        this.sermonTakeaway = sermonTakeaway;
    }

    private String photoURL;

    private long creationDate;
    private long creationDateReversed;
    private long modifiedDate;
    private long modifiedDateReversed;

    private List<ReferenceMaterial> referenceMaterialList;
    private List<SupportingPoint> supportingPointList;
    private List<Illustration> illustrationList;

    private String contributorName;
    private String contributorId;
    private String contributorEmailAddress;

    public static class Illustration implements Parcelable {
        String content;
        String reference_source;
        String title;
        int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getTitle() {
            return this.title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getReference_source() {
            return this.reference_source;
        }

        public void setReference_source(String reference_source) {
            this.reference_source = reference_source;
        }

        public String getContent() {
            return this.content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.content);
            dest.writeString(this.reference_source);
            dest.writeString(this.title);
            dest.writeInt(this.position);
        }

        public Illustration() {
        }

        protected Illustration(Parcel in) {
            this.content = in.readString();
            this.reference_source = in.readString();
            this.title = in.readString();
            this.position = in.readInt();
        }

        public static final Creator<Illustration> CREATOR = new Creator<Illustration>() {
            @Override
            public Illustration createFromParcel(Parcel source) {
                return new Illustration(source);
            }

            @Override
            public Illustration[] newArray(int size) {
                return new Illustration[size];
            }
        };
    }

    public static class ReferenceMaterial implements Parcelable {
        String content;
        String reference_source;
        String title;
        int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getTitle() {
            return this.title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getReference_source() {
            return this.reference_source;
        }

        public void setReference_source(String reference_source) {
            this.reference_source = reference_source;
        }

        public String getContent() {
            return this.content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.content);
            dest.writeString(this.reference_source);
            dest.writeString(this.title);
            dest.writeInt(this.position);
        }

        public ReferenceMaterial() {
        }

        protected ReferenceMaterial(Parcel in) {
            this.content = in.readString();
            this.reference_source = in.readString();
            this.title = in.readString();
            this.position = in.readInt();
        }

        public static final Creator<ReferenceMaterial> CREATOR = new Creator<ReferenceMaterial>() {
            @Override
            public ReferenceMaterial createFromParcel(Parcel source) {
                return new ReferenceMaterial(source);
            }

            @Override
            public ReferenceMaterial[] newArray(int size) {
                return new ReferenceMaterial[size];
            }
        };
    }

    public static class SupportingPoint implements Parcelable {
        String application;
        String description;
        String outcome;
        String reference;
        int position;

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public String getDescription() {
            return this.description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getOutcome() {
            return this.outcome;
        }

        public void setOutcome(String outcome) {
            this.outcome = outcome;
        }

        public String getReference() {
            return this.reference;
        }

        public void setReference(String reference) {
            this.reference = reference;
        }

        public String getApplication() {
            return this.application;
        }

        public void setApplication(String application) {
            this.application = application;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.application);
            dest.writeString(this.description);
            dest.writeString(this.outcome);
            dest.writeString(this.reference);
            dest.writeInt(this.position);
        }

        public SupportingPoint() {
        }

        protected SupportingPoint(Parcel in) {
            this.application = in.readString();
            this.description = in.readString();
            this.outcome = in.readString();
            this.reference = in.readString();
            this.position = in.readInt();
        }

        public static final Creator<SupportingPoint> CREATOR = new Creator<SupportingPoint>() {
            @Override
            public SupportingPoint createFromParcel(Parcel source) {
                return new SupportingPoint(source);
            }

            @Override
            public SupportingPoint[] newArray(int size) {
                return new SupportingPoint[size];
            }
        };
    }

    public String getSermonTitle() {
        return sermonTitle;
    }

    public void setSermonTitle(String sermonTitle) {
        this.sermonTitle = sermonTitle;
    }

    public String getSermonKeyText() {
        return sermonKeyText;
    }

    public void setSermonKeyText(String sermonKeyText) {
        this.sermonKeyText = sermonKeyText;
    }

    public String getSermonSubject() {
        return sermonSubject;
    }

    public void setSermonSubject(String sermonSubject) {
        this.sermonSubject = sermonSubject;
    }

    public String getSermonComplement() {
        return sermonComplement;
    }

    public void setSermonComplement(String sermonComplement) {
        this.sermonComplement = sermonComplement;
    }

    public String getSermonIdea() {
        return sermonIdea;
    }

    public void setSermonIdea(String sermonIdea) {
        this.sermonIdea = sermonIdea;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
        this.creationDateReversed = creationDate * -1;
        this.setModifiedDate(creationDate);
    }

    public long getCreationDateReversed() {
        return creationDateReversed;
    }

    public void setCreationDateReversed(long creationDateReversed) {
        this.creationDateReversed = creationDateReversed;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
        this.modifiedDateReversed = modifiedDate * -1;
    }

    public long getModifiedDateReversed() {
        return modifiedDateReversed;
    }

    public void setModifiedDateReversed(long modifiedDateReversed) {
        this.modifiedDateReversed = modifiedDateReversed;
    }

    public List<ReferenceMaterial> getReferenceMaterialList() {
        return referenceMaterialList;
    }

    public void setReferenceMaterialList(List<ReferenceMaterial> referenceMaterialList) {
        this.referenceMaterialList = referenceMaterialList;
    }

    public List<SupportingPoint> getSupportingPointList() {
        return supportingPointList;
    }

    public void setSupportingPointList(List<SupportingPoint> supportingPointList) {
        this.supportingPointList = supportingPointList;
    }

    public List<Illustration> getIllustrationList() {
        return illustrationList;
    }

    public void setIllustrationList(List<Illustration> illustrationList) {
        this.illustrationList = illustrationList;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getShareContent() {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder content = new StringBuilder();

        //add sermon title
        content.append("Sermon Title: " + sermonTitle);
        content.append("\n");

        //add sermon key text
        if (!getSermonKeyText().isEmpty()) {
            content.append("\n");
            content.append("Key Text: " + getSermonKeyText().toUpperCase());
            content.append("\n");
        }
        //add sermon subject
        if (!getSermonSubject().isEmpty()) {
            content.append("\n");
            content.append("Subject: " + getSermonSubject());
            content.append("\n");
        }
        //add sermon complement
        if (!getSermonComplement().isEmpty()) {
            content.append("\n");
            content.append("Complement: " + getSermonComplement());
            content.append("\n");
        }
        //add sermon idea
        if (!getSermonIdea().isEmpty()) {
            content.append("\n");
            content.append("Idea: " + getSermonIdea());
            content.append("\n");
        }
        //add sermon take away
        if (!getSermonTakeaway().isEmpty()) {
            content.append("\n");
            content.append("Congregation Takeaway:\n " + getSermonTakeaway().trim());
            content.append("\n");
        }

        //add supporting points
        if (supportingPointList.size() > 0) {
            content.append("\nSupporting Points (Sermon Pillars):");
            for (SupportingPoint s : supportingPointList) {
                if (!s.getDescription().isEmpty()) {
                    content.append("\n");
                    content.append("-> " + s.getDescription());
                    content.append("\n");
                }
            }
        }

        //add illustrations
        if (illustrationList.size() > 0) {
            content.append("\nIllustrations:");
            for (Illustration i : illustrationList) {
                if (!i.getTitle().isEmpty()) {
                    content.append("\n");
                    content.append("-> " + i.getTitle());
                    content.append("\n");
                }
            }
        }

        //add related material
        if (referenceMaterialList.size() > 0) {
            content.append("\nReference Material:");
            for (ReferenceMaterial r : referenceMaterialList) {
                if (!r.getTitle().isEmpty()) {
                    content.append("\n");
                    content.append("-> " + r.getTitle());
                    content.append("\n");
                }
            }
        }

        stringBuilder.append(content.toString());
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        stringBuilder.append("Written by: " + contributorName);
        stringBuilder.append("\n");
        stringBuilder.append("#PreachersClubApp");

        return stringBuilder.toString();
    }

    public String getShareContentSubject() {
        return Utility.toTitleCase(getSermonTitle());
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getContributorEmailAddress() {
        return contributorEmailAddress;
    }

    public void setContributorEmailAddress(String contributorEmailAddress) {
        this.contributorEmailAddress = contributorEmailAddress;
    }

    public Sermon() {
        this.supportingPointList = new ArrayList<>();
        this.illustrationList = new ArrayList<>();
        this.referenceMaterialList = new ArrayList<>();
        this.setCreationDate(System.currentTimeMillis());
    }

    public Sermon(String sermonTitle) {
        this.supportingPointList = new ArrayList();
        this.referenceMaterialList = new ArrayList();
        this.illustrationList = new ArrayList();
        this.sermonTitle = sermonTitle;
        this.setCreationDate(System.currentTimeMillis());
    }

    public Sermon(String sermonTitle, String keyText) {
        this.supportingPointList = new ArrayList();
        this.referenceMaterialList = new ArrayList();
        this.illustrationList = new ArrayList();
        this.sermonTitle = sermonTitle;
        this.sermonKeyText = keyText;
        this.setCreationDate(System.currentTimeMillis());
    }

    public void setIllustrationsByPosition(Illustration illustration, int position) {
        this.illustrationList.set(position, illustration);
    }

    public void setReferenceMaterialByPosition(ReferenceMaterial referenceMaterial, int position) {
        this.referenceMaterialList.set(position, referenceMaterial);
    }

    public void setSupportingPointByPosition(SupportingPoint supportingPoint, int position) {
        this.supportingPointList.set(position, supportingPoint);
    }

    public void addSupportingPoint(SupportingPoint supportingPoint) {
        this.supportingPointList.add(supportingPoint);
    }

    public void addReferenceMaterial(ReferenceMaterial referenceMaterial) {
        this.referenceMaterialList.add(referenceMaterial);
    }

    public void addIllustration(Illustration illustration) {
        this.illustrationList.add(illustration);
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.sermonTitle);
        dest.writeString(this.sermonKeyText);
        dest.writeString(this.sermonSubject);
        dest.writeString(this.sermonComplement);
        dest.writeString(this.sermonIdea);
        dest.writeString(this.sermonTakeaway);
        dest.writeString(this.photoURL);
        dest.writeLong(this.creationDate);
        dest.writeLong(this.creationDateReversed);
        dest.writeLong(this.modifiedDate);
        dest.writeLong(this.modifiedDateReversed);
        dest.writeList(this.referenceMaterialList);
        dest.writeList(this.supportingPointList);
        dest.writeList(this.illustrationList);
        dest.writeString(this.contributorName);
        dest.writeString(this.contributorId);
        dest.writeString(this.contributorEmailAddress);
    }

    protected Sermon(Parcel in) {
        this.sermonTitle = in.readString();
        this.sermonKeyText = in.readString();
        this.sermonSubject = in.readString();
        this.sermonComplement = in.readString();
        this.sermonIdea = in.readString();
        this.sermonTakeaway = in.readString();
        this.photoURL = in.readString();
        this.creationDate = in.readLong();
        this.creationDateReversed = in.readLong();
        this.modifiedDate = in.readLong();
        this.modifiedDateReversed = in.readLong();
        this.referenceMaterialList = new ArrayList<ReferenceMaterial>();
        in.readList(this.referenceMaterialList, ReferenceMaterial.class.getClassLoader());
        this.supportingPointList = new ArrayList<SupportingPoint>();
        in.readList(this.supportingPointList, SupportingPoint.class.getClassLoader());
        this.illustrationList = new ArrayList<Illustration>();
        in.readList(this.illustrationList, Illustration.class.getClassLoader());
        this.contributorName = in.readString();
        this.contributorId = in.readString();
        this.contributorEmailAddress = in.readString();
    }

    public static final Creator<Sermon> CREATOR = new Creator<Sermon>() {
        @Override
        public Sermon createFromParcel(Parcel source) {
            return new Sermon(source);
        }

        @Override
        public Sermon[] newArray(int size) {
            return new Sermon[size];
        }
    };
}
