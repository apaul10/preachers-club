package com.apwright.preachersclub.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by apwright on 6/30/2017.
 */

public class PrefManager {

    SharedPreferences mPreferences;
    SharedPreferences.Editor mEditor;
    Context mContext;

    //shared preference mode
    int PRIVATE_MODE = 0;

    //preferences name
    private static final String PREF_NAME = "HelpSliderIntro";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";

    public PrefManager(Context context) {
        this.mContext = context;
        mPreferences = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mPreferences.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime){
        mEditor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        mEditor.commit();
    }

    public boolean isFirstTimeLaunch(){
        return mPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
}
