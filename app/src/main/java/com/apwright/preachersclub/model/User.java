package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by apwright on 5/31/2017.
 */

public class User implements Parcelable {
    private String userId;
    private String provider;
    private String displayName;
    private String emailAddress;
    private String photoUrl;
    private boolean isContributor;

    public User() {
    }

    public User(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public boolean isContributor() {
        return isContributor;
    }

    public void setContributor(boolean contributor) {
        isContributor = contributor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.userId);
        dest.writeString(this.provider);
        dest.writeString(this.displayName);
        dest.writeString(this.emailAddress);
        dest.writeString(this.photoUrl);
        dest.writeByte(this.isContributor ? (byte) 1 : (byte) 0);
    }

    protected User(Parcel in) {
        this.userId = in.readString();
        this.provider = in.readString();
        this.displayName = in.readString();
        this.emailAddress = in.readString();
        this.photoUrl = in.readString();
        this.isContributor = in.readByte() != 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}
