package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.apwright.preachersclub.Utility;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apwright on 6/13/2017.
 */

public class DifficultQuestion implements Parcelable {

    private long dateCreated;
    private long dateCreatedReversed;
    private long dateUpdated;
    private long dateUpdatedReversed;
    private String questionDescription;
    private String responseSummary;
    private String responseDetail;
    private List<String> bibleVerses;
    private String contributorName;
    private String contributorID;
    private String photoURL;
    private String furtherReadingURL;

    public final static String DIFFICULT_QUESTION_KEY = "question";
    public final static String DIFFICULT_QUESTION_FIRE_BASE_KEY = "question_fb_key";

    public DifficultQuestion() {
        this.bibleVerses = new ArrayList<String>();
    }

    public static DifficultQuestion getSampleData() {
        DifficultQuestion sampleData = new DifficultQuestion();

        sampleData.setDateCreated(System.currentTimeMillis());
        sampleData.setQuestionDescription("Where do I go when I die?");
        sampleData.setResponseSummary("Our body goes to the ground, and our breath returns to God");
        sampleData.setResponseDetail("");
        sampleData.setContributorName("Data Provider");
        sampleData.setPhotoURL("https://www.bible.com/assets/icons/bible/200/en-df9e42178ce28600ec4049a9dc80d818.png");

        return sampleData;
    }

    public String getShareContentSubject() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Difficult Bible Question: " + Utility.toTitleCase(questionDescription));

        return stringBuilder.toString();
    }

    public String getShareContent(int limit) {
        StringBuilder stringBuilder = new StringBuilder();

        limit = (responseSummary.length() < limit ? responseSummary.length() : limit);

        stringBuilder.append(questionDescription);
        stringBuilder.append("\n");
        stringBuilder.append("Bible References: " + getBibleVerses().toString());
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        if (limit > 0) {
            stringBuilder.append(responseSummary.substring(0, limit) + "...");
        } else {
            stringBuilder.append(responseSummary);
            stringBuilder.append("\n");
            stringBuilder.append("\n");
            stringBuilder.append(responseDetail);

            if (!furtherReadingURL.isEmpty()) {
                stringBuilder.append("\n");
                stringBuilder.append("\n");
                stringBuilder.append("For even more content - visit " + furtherReadingURL);
            }
        }
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        stringBuilder.append("- written by: " + contributorName);
        stringBuilder.append("\n");
        stringBuilder.append("#PreachersClubApp");

        return stringBuilder.toString();
    }

    public String getContributorID() {
        return contributorID;
    }

    public void setContributorID(String contributorID) {
        this.contributorID = contributorID;
    }

    public String getFurtherReadingURL() {
        return furtherReadingURL;
    }

    public void setFurtherReadingURL(String furtherReadingURL) {
        this.furtherReadingURL = furtherReadingURL;
    }

    public List<String> getBibleVerses() {
        return bibleVerses;
    }

    public void setBibleVerses(List<String> bibleVerses) {
        this.bibleVerses = bibleVerses;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
        this.dateCreatedReversed = dateCreated * -1;
        setDateUpdated(dateCreated);
    }

    public long getDateCreatedReversed() {
        return dateCreatedReversed;
    }

    public void setDateCreatedReversed(long dateCreatedReversed) {
        this.dateCreatedReversed = dateCreatedReversed;
    }

    public long getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(long dateUpdated) {
        this.dateUpdated = dateUpdated;
        this.dateUpdatedReversed = dateUpdated * -1;
    }

    public long getDateUpdatedReversed() {
        return dateUpdatedReversed;
    }

    public void setDateUpdatedReversed(long dateUpdatedReversed) {
        this.dateUpdatedReversed = dateUpdatedReversed;
    }

    public String getQuestionDescription() {
        return questionDescription;
    }

    public void setQuestionDescription(String questionDescription) {
        this.questionDescription = questionDescription;
    }

    public String getResponseSummary() {
        return responseSummary;
    }

    public void setResponseSummary(String responseSummary) {
        this.responseSummary = responseSummary;
    }

    public String getResponseDetail() {
        return responseDetail;
    }

    public void setResponseDetail(String responseDetail) {
        this.responseDetail = responseDetail;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.dateCreated);
        dest.writeLong(this.dateCreatedReversed);
        dest.writeLong(this.dateUpdated);
        dest.writeLong(this.dateUpdatedReversed);
        dest.writeString(this.questionDescription);
        dest.writeString(this.responseSummary);
        dest.writeString(this.responseDetail);
        dest.writeStringList(this.bibleVerses);
        dest.writeString(this.contributorName);
        dest.writeString(this.contributorID);
        dest.writeString(this.photoURL);
        dest.writeString(this.furtherReadingURL);
    }

    protected DifficultQuestion(Parcel in) {
        this.dateCreated = in.readLong();
        this.dateCreatedReversed = in.readLong();
        this.dateUpdated = in.readLong();
        this.dateUpdatedReversed = in.readLong();
        this.questionDescription = in.readString();
        this.responseSummary = in.readString();
        this.responseDetail = in.readString();
        this.bibleVerses = in.createStringArrayList();
        this.contributorName = in.readString();
        this.contributorID = in.readString();
        this.photoURL = in.readString();
        this.furtherReadingURL = in.readString();
    }

    public static final Parcelable.Creator<DifficultQuestion> CREATOR = new Parcelable.Creator<DifficultQuestion>() {
        @Override
        public DifficultQuestion createFromParcel(Parcel source) {
            return new DifficultQuestion(source);
        }

        @Override
        public DifficultQuestion[] newArray(int size) {
            return new DifficultQuestion[size];
        }
    };
}
