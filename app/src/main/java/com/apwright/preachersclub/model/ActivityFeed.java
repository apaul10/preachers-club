package com.apwright.preachersclub.model;

/**
 * Created by apwright on 6/11/2017.
 */

public class ActivityFeed {

    private FeedType activityFeedType;
    private Object feedContent;
    private long dateCreated;
    private long dateCreatedReversed;
    private String contributorName;
    private String fireBaseKey;

    public enum FeedType {
        TUTORIAL,
        CONTRIBUTOR,
        ASSIGNMENT
    }

    public static final int TUTORIAL = 0;
    public static final int CONTRIBUTOR = 1;
    public static final int ASSIGNMENT = 2;


    public ActivityFeed() {
    }


    public ActivityFeed(ActivityFeed.FeedType activityFeedType, Object feedContent) {
        this.activityFeedType = activityFeedType;
        this.feedContent = feedContent;
    }

    public FeedType getActivityFeedType() {
        return activityFeedType;
    }

    public int getActivityFeedTypeOrdinal() {
        return activityFeedType.ordinal();
    }

    public void setActivityFeedType(FeedType activityFeedType) {
        this.activityFeedType = activityFeedType;
    }

    public String getFireBaseKey() {
        return fireBaseKey;
    }

    public void setFireBaseKey(String fireBaseKey) {
        this.fireBaseKey = fireBaseKey;
    }

    public Object getFeedContent() {
        return feedContent;
    }

    public void setFeedContent(Object feedContent) {
        this.feedContent = feedContent;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
        this.dateCreatedReversed = dateCreated * -1;
    }

    public long getDateCreatedReversed() {
        return dateCreatedReversed;
    }

    public void setDateCreatedReversed(long dateCreatedReversed) {
        this.dateCreatedReversed = dateCreatedReversed;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }
}
