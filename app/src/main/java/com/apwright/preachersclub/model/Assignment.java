package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.Time;

import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.fragments.AssignmentEditFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by apwright on 6/11/2017.
 */

public class Assignment implements Parcelable {
    private String assignmentName;
    private String assignmentDescription;
    private long dueDate;
    private long dueDateReversed;
    private long dateCreated;
    private long dateCreatedReversed;
    private String contributorEmail;
    private String contributorName;

    public final static String ASSIGNMENT_KEY = "assignment";
    public final static String ASSIGNMENT_FIRE_BASE_KEY = "assignment_fb_key";

    public Assignment() {
    }

    public String getDefaultImageURL() {
        return "https://firebasestorage.googleapis.com/v0/b/preachers-club.appspot.com/o/images%2Fdefaults%2Fassignment_default.jpg?alt=media&token=167ec04e-c42d-4fc5-b052-564b4f07be73";
    }

    public String getAssignmentName() {
        return assignmentName;
    }

    public void setAssignmentName(String assignmentName) {
        this.assignmentName = assignmentName;
    }

    public String getAssignmentDescription() {
        return assignmentDescription;
    }

    public void setAssignmentDescription(String assignmentDescription) {
        this.assignmentDescription = assignmentDescription;
    }

    public String getShareContentSubject() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Preachers Club Assignment: "+ Utility.toTitleCase(assignmentName));

        return stringBuilder.toString();
    }

    public String getShareContent(int limit) {
        StringBuilder stringBuilder = new StringBuilder();

        limit = (assignmentDescription.length() < limit ? assignmentDescription.length() : limit);

        stringBuilder.append("Assignment: " + assignmentName);
        stringBuilder.append("\n");
        stringBuilder.append("Due Date: " + getDueDateAsString());
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        if (limit > 0) {
            stringBuilder.append(assignmentDescription.substring(0, limit) + "...");
        } else {
            stringBuilder.append(assignmentDescription);
        }
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        stringBuilder.append("- written by: " + contributorName);
        stringBuilder.append("\n");
        stringBuilder.append("#PreachersClubApp");

        return stringBuilder.toString();
    }

    public long getDueDate() {
        return dueDate;
    }

    public String getDueDateAsString() {
        String formattedDate = "";

        formattedDate = Utility.getDateAsString(dueDate, AssignmentEditFragment.DATE_FORMAT);

        return formattedDate;
    }

    public String getFormattedDueDate() {
        String formattedDate = "";

        Time t = new Time();
        t.setToNow();
        int julianDay = Time.getJulianDay(dueDate, t.gmtoff);
        int currentJulianDay = Time.getJulianDay(System.currentTimeMillis(), t.gmtoff);

        int daysUntilDue = julianDay - currentJulianDay;
        int weeksUntilDue = daysUntilDue / 7;
        int monthsUntilDue = daysUntilDue / 30;

        if (daysUntilDue < 0) {
            formattedDate = "Past Due";
        } else if (daysUntilDue == 0) {
            formattedDate = "Due Today";
        } else if (daysUntilDue < 7) {
            formattedDate = "Due in " + daysUntilDue + " day(s)";
        } else if (weeksUntilDue < 4) {
            formattedDate = "Due in " + weeksUntilDue + " week(s)";
        } else {
            formattedDate = "Due in " + monthsUntilDue + " month(s)";
        }

        return formattedDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
        this.dueDateReversed = dueDate * -1;
    }

    public long getDueDateReversed() {
        return dueDateReversed;
    }

    public void setDueDateReversed(long dueDateReversed) {
        this.dueDateReversed = dueDateReversed;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
        this.dateCreatedReversed = dateCreated * -1;
    }

    public long getDateCreatedReversed() {
        return dateCreatedReversed;
    }

    public void setDateCreatedReversed(long dateCreatedReversed) {
        this.dateCreatedReversed = dateCreatedReversed;
    }

    public String getContributorEmail() {
        return contributorEmail;
    }

    public void setContributorEmail(String contributorEmail) {
        this.contributorEmail = contributorEmail;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.assignmentName);
        dest.writeString(this.assignmentDescription);
        dest.writeLong(this.dueDate);
        dest.writeLong(this.dueDateReversed);
        dest.writeLong(this.dateCreated);
        dest.writeLong(this.dateCreatedReversed);
        dest.writeString(this.contributorEmail);
        dest.writeString(this.contributorName);
    }

    protected Assignment(Parcel in) {
        this.assignmentName = in.readString();
        this.assignmentDescription = in.readString();
        this.dueDate = in.readLong();
        this.dueDateReversed = in.readLong();
        this.dateCreated = in.readLong();
        this.dateCreatedReversed = in.readLong();
        this.contributorEmail = in.readString();
        this.contributorName = in.readString();
    }

    public static final Parcelable.Creator<Assignment> CREATOR = new Parcelable.Creator<Assignment>() {
        @Override
        public Assignment createFromParcel(Parcel source) {
            return new Assignment(source);
        }

        @Override
        public Assignment[] newArray(int size) {
            return new Assignment[size];
        }
    };
}
