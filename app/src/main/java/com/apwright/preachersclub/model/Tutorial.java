package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;

import com.apwright.preachersclub.Utility;

/**
 * Created by apwright on 5/31/2017.
 */

public class Tutorial implements Parcelable {
    private String contributorName;
    private String contributorId;
    private long creationDate;
    private long creationDateReversed;
    private long lastModifiedDate;
    private long lastModifiedDateReversed;
    private String photoURL;
    private String title;
    private String subTitle;
    private String details;

    public final static String TUTORIAL_KEY = "tutorial";
    public final static String TUTORIAL_FIRE_BASE_KEY = "tutorial_fb_key";

    public Tutorial() {
    }

    public Tutorial(String title, String subTitle) {
        this.title = title;
        this.subTitle = subTitle;
    }

    public long getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(long lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
        this.lastModifiedDateReversed = lastModifiedDate * -1;
    }

    public long getLastModifiedDateReversed() {
        return lastModifiedDateReversed;
    }

    public void setLastModifiedDateReversed(long lastModifiedDateReversed) {
        this.lastModifiedDateReversed = lastModifiedDateReversed;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
        this.creationDateReversed = creationDate * -1;
        setLastModifiedDate(creationDate);
    }

    public long getCreationDateReversed() {
        return creationDateReversed;
    }

    public void setCreationDateReversed(long creationDateReversed) {
        this.creationDateReversed = creationDateReversed;
    }

    public String getPhotoURL() {
        return photoURL;
    }

    public void setPhotoURL(String photoURL) {
        this.photoURL = photoURL;
    }

    public String getTitle() {
        return title;
    }

    public String getShareContentSubject() {
        return Utility.toTitleCase(title);
    }

    public String getShareContent(int limit) {
        StringBuilder stringBuilder = new StringBuilder();

        limit = (details.length() < limit ? details.length() : limit);

        stringBuilder.append(title);
        stringBuilder.append("\n");
        stringBuilder.append(subTitle);
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        if (limit > 0) {
            stringBuilder.append(details.substring(0, limit) + "...");
        } else {
            stringBuilder.append(details);
        }
        stringBuilder.append("\n");
        stringBuilder.append("\n");
        stringBuilder.append("- written by: " + contributorName);
        stringBuilder.append("\n");
        stringBuilder.append("#PreachersClubApp");

        return stringBuilder.toString();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contributorName);
        dest.writeString(this.contributorId);
        dest.writeLong(this.creationDate);
        dest.writeLong(this.creationDateReversed);
        dest.writeLong(this.lastModifiedDate);
        dest.writeLong(this.lastModifiedDateReversed);
        dest.writeString(this.photoURL);
        dest.writeString(this.title);
        dest.writeString(this.subTitle);
        dest.writeString(this.details);
    }

    protected Tutorial(Parcel in) {
        this.contributorName = in.readString();
        this.contributorId = in.readString();
        this.creationDate = in.readLong();
        this.creationDateReversed = in.readLong();
        this.lastModifiedDate = in.readLong();
        this.lastModifiedDateReversed = in.readLong();
        this.photoURL = in.readString();
        this.title = in.readString();
        this.subTitle = in.readString();
        this.details = in.readString();
    }

    public static final Creator<Tutorial> CREATOR = new Creator<Tutorial>() {
        @Override
        public Tutorial createFromParcel(Parcel source) {
            return new Tutorial(source);
        }

        @Override
        public Tutorial[] newArray(int size) {
            return new Tutorial[size];
        }
    };
}
