package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.format.Time;

/**
 * Created by apwright on 9/15/2017.
 */

public class SermonPresentation implements Parcelable {

    private String contributorId;
    private String contributorEmailAddress;
    private Sermon referenceSermonTemplate;

    private String presentationTitle;
    private String introduction;
    private String body;
    private String conclusion;

    private long creationDate;
    private long creationDateReversed;
    private long modifiedDate;
    private long modifiedDateReversed;

    public final static String PRESENTATION_KEY = "presentation";
    public final static String PRESENTATION_FIRE_BASE_KEY = "presentation_fb_key";

    public SermonPresentation() {
    }

    public String getPresentationTitle() {
        return presentationTitle;
    }

    public void setPresentationTitle(String presentationTitle) {
        this.presentationTitle = presentationTitle;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public String getContributorEmailAddress() {
        return contributorEmailAddress;
    }

    public void setContributorEmailAddress(String contributorEmailAddress) {
        this.contributorEmailAddress = contributorEmailAddress;
    }

    public Sermon getReferenceSermonTemplate() {
        return referenceSermonTemplate;
    }

    public void setReferenceSermonTemplate(Sermon referenceSermonTemplate) {
        this.referenceSermonTemplate = referenceSermonTemplate;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
        this.creationDateReversed = creationDate * -1;
        setModifiedDate(creationDate);
    }

    public long getCreationDateReversed() {
        return creationDateReversed;
    }

    public void setCreationDateReversed(long creationDateReversed) {
        this.creationDateReversed = creationDateReversed;
    }

    public long getModifiedDate() {
        return modifiedDate;
    }

    public String getFriendlyDate() {
        String formattedDate = "";

        Time t = new Time();
        t.setToNow();
        int julianDay = Time.getJulianDay(getModifiedDate(), t.gmtoff);
        int currentJulianDay = Time.getJulianDay(System.currentTimeMillis(), t.gmtoff);

        int daysFromToday = currentJulianDay - julianDay;
        int weeks = daysFromToday / 7;
        int months = daysFromToday / 30;

        if (julianDay == currentJulianDay) {
            formattedDate = "Today";
        } else if (julianDay == currentJulianDay - 1) {
            formattedDate = "Yesterday";
        } else if (daysFromToday < 7) {
            formattedDate = daysFromToday + "d";
        } else if (weeks <= 4) {
            formattedDate = weeks + "w";
        } else {
            formattedDate = months + "m";
        }

        return formattedDate;
    }

    public String getSummary() {
        String summary = "";
        String intro = getIntroduction().substring(0, Math.min(getIntroduction().length(), 150));
        String conclusion = getConclusion().substring(0, Math.min(getConclusion().length(), 150));

        summary = intro + "......." + conclusion;

        return summary;
    }

    public void setModifiedDate(long modifiedDate) {
        this.modifiedDate = modifiedDate;
        this.modifiedDateReversed = modifiedDate * -1;
    }

    public long getModifiedDateReversed() {
        return modifiedDateReversed;
    }

    public void setModifiedDateReversed(long modifiedDateReversed) {
        this.modifiedDateReversed = modifiedDateReversed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.contributorId);
        dest.writeString(this.contributorEmailAddress);
        dest.writeParcelable(this.referenceSermonTemplate, flags);
        dest.writeString(this.presentationTitle);
        dest.writeString(this.introduction);
        dest.writeString(this.body);
        dest.writeString(this.conclusion);
        dest.writeLong(this.creationDate);
        dest.writeLong(this.creationDateReversed);
        dest.writeLong(this.modifiedDate);
        dest.writeLong(this.modifiedDateReversed);
    }

    protected SermonPresentation(Parcel in) {
        this.contributorId = in.readString();
        this.contributorEmailAddress = in.readString();
        this.referenceSermonTemplate = in.readParcelable(Sermon.class.getClassLoader());
        this.presentationTitle = in.readString();
        this.introduction = in.readString();
        this.body = in.readString();
        this.conclusion = in.readString();
        this.creationDate = in.readLong();
        this.creationDateReversed = in.readLong();
        this.modifiedDate = in.readLong();
        this.modifiedDateReversed = in.readLong();
    }

    public static final Creator<SermonPresentation> CREATOR = new Creator<SermonPresentation>() {
        @Override
        public SermonPresentation createFromParcel(Parcel source) {
            return new SermonPresentation(source);
        }

        @Override
        public SermonPresentation[] newArray(int size) {
            return new SermonPresentation[size];
        }
    };
}
