package com.apwright.preachersclub.model;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by apwright on 6/11/2017.
 */

public class CustomerViewPager extends ViewPager {

    private boolean swipingEnabled;

    public CustomerViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.swipingEnabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (swipingEnabled) {
            return super.onTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (swipingEnabled) {
            return super.onInterceptTouchEvent(ev);
        }
        return false;
    }

    public void setSwipingEnabled(boolean enabled) {
        this.swipingEnabled = enabled;
    }
}
