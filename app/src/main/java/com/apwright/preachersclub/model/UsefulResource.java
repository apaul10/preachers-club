package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.apwright.preachersclub.Utility;

/**
 * Created by apwright on 6/11/2017.
 */

public class UsefulResource implements Parcelable {
    private String resourceTitle;
    private String resourceDescription;
    private String resourceURL;
    private String resourceImageURL;
    private long submissionDate;
    private long submissionDateReversed;
    private String contributorID;
    private String contributorName;

    public final static String USEFUL_RESOURCE_KEY = "useful_resource";
    public final static String USEFUL_RESOURCE_FIRE_BASE_KEY = "useful_resource_fb_key";

    public UsefulResource() {
    }

    public void preloadMetaData(WebMetaData webMetaData) {
        //this.resourceURL = resourceURL;
        //WebMetaData webMetaData = Utility.getWebsiteMetaData(resourceURL);
        this.resourceTitle = webMetaData.title;
        this.resourceDescription = webMetaData.description;
        this.resourceImageURL = webMetaData.imageURL;
    }

    public String getContributorID() {
        return contributorID;
    }

    public void setContributorID(String contributorID) {
        this.contributorID = contributorID;
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getResourceTitle() {
        return resourceTitle;
    }

    public void setResourceTitle(String resourceTitle) {
        this.resourceTitle = resourceTitle;
    }

    public String getResourceDescription() {
        return resourceDescription;
    }

    public void setResourceDescription(String resourceDescription) {
        this.resourceDescription = resourceDescription;
    }

    public String getResourceURL() {
        return resourceURL;
    }

    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL.toLowerCase().trim();
    }

    public String getResourceImageURL() {
        return resourceImageURL;
    }

    public void setResourceImageURL(String resourceImageURL) {
        this.resourceImageURL = resourceImageURL.toLowerCase().trim();
    }

    public long getSubmissionDate() {
        return submissionDate;
    }

    public void setSubmissionDate(long submissionDate) {
        this.submissionDate = submissionDate;
        this.submissionDateReversed = -1 * submissionDate;
    }

    public long getSubmissionDateReversed() {
        return submissionDateReversed;
    }

    public void setSubmissionDateReversed(long submissionDateReversed) {
        this.submissionDateReversed = submissionDateReversed;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.resourceTitle);
        dest.writeString(this.resourceDescription);
        dest.writeString(this.resourceURL);
        dest.writeString(this.resourceImageURL);
        dest.writeLong(this.submissionDate);
        dest.writeLong(this.submissionDateReversed);
        dest.writeString(this.contributorID);
        dest.writeString(this.contributorName);
    }

    protected UsefulResource(Parcel in) {
        this.resourceTitle = in.readString();
        this.resourceDescription = in.readString();
        this.resourceURL = in.readString();
        this.resourceImageURL = in.readString();
        this.submissionDate = in.readLong();
        this.submissionDateReversed = in.readLong();
        this.contributorID = in.readString();
        this.contributorName = in.readString();
    }

    public static final Parcelable.Creator<UsefulResource> CREATOR = new Parcelable.Creator<UsefulResource>() {
        @Override
        public UsefulResource createFromParcel(Parcel source) {
            return new UsefulResource(source);
        }

        @Override
        public UsefulResource[] newArray(int size) {
            return new UsefulResource[size];
        }
    };
}
