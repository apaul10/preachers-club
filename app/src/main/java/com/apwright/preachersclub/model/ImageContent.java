package com.apwright.preachersclub.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.net.URLEncoder;

/**
 * Created by apwright on 6/5/2017.
 */

public class ImageContent implements Parcelable {
    String imageName = "";
    String previewURL = "";
    String thumbnailURL = "";
    String imageID = "";

    public final static String IMAGE_CONTENT_KEY = "image_content";

    final static String API_KEY_PIXABAY = "5557732-a0e647c8d19f226cf4d73361b";
    final static String API_KEY_PEXELS = "";

    final static String BASE_URL_PIXABAY = "https://pixabay.com/api/";

    public static String getPixabaySearchURL(String searchTerm) {
        searchTerm = URLEncoder.encode(searchTerm);

        StringBuilder sbURL = new StringBuilder();

        sbURL.append(BASE_URL_PIXABAY)
                .append("?key=" + API_KEY_PIXABAY)
                .append("&q=" + searchTerm)
                .append("&image_type=photo")
                .append("&safesearch=true")
                .append("&min_height=200")

        ;

        return sbURL.toString();
    }

    public ImageContent() {
    }

    public String getImageName() {
        String name = previewURL.substring(previewURL.lastIndexOf('/')+1, previewURL.length());
        return name;
    }

    public void setImageName(String imageName) {
        this.imageName = getImageName();
    }

    public String getPreviewURL() {
        return previewURL;
    }

    public void setPreviewURL(String previewURL) {
        this.previewURL = previewURL;
    }

    public String getThumbnailURL() {
        return thumbnailURL;
    }

    public void setThumbnailURL(String thumbnailURL) {
        this.thumbnailURL = thumbnailURL;
    }

    public String getImageID() {
        return imageID;
    }

    public void setImageID(String imageID) {
        this.imageID = imageID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageName);
        dest.writeString(this.previewURL);
        dest.writeString(this.thumbnailURL);
        dest.writeString(this.imageID);
    }

    protected ImageContent(Parcel in) {
        this.imageName = in.readString();
        this.previewURL = in.readString();
        this.thumbnailURL = in.readString();
        this.imageID = in.readString();
    }

    public static final Parcelable.Creator<ImageContent> CREATOR = new Parcelable.Creator<ImageContent>() {
        @Override
        public ImageContent createFromParcel(Parcel source) {
            return new ImageContent(source);
        }

        @Override
        public ImageContent[] newArray(int size) {
            return new ImageContent[size];
        }
    };
}
