package com.apwright.preachersclub.model;

/**
 * Created by apwright on 6/18/2017.
 */

public class Contributor {

    private String contributorName;
    private String contributorEmail;
    private String contributorShortBio;
    private String contributorPhotoURL;
    private String contributorID;
    private long dateCreated;
    private long dateCreatedReversed;

    public Contributor() {
    }

    public String getFileNameForImage() {
        return contributorEmail.replace("@", "_").replace(".", "_").toLowerCase().trim().toString() + ".jpg";
    }

    public String getContributorName() {
        return contributorName;
    }

    public void setContributorName(String contributorName) {
        this.contributorName = contributorName;
    }

    public String getContributorEmail() {
        return contributorEmail;
    }

    public void setContributorEmail(String contributorEmail) {
        this.contributorEmail = contributorEmail;
    }

    public String getContributorShortBio() {
        return contributorShortBio;
    }

    public void setContributorShortBio(String contributorShortBio) {
        this.contributorShortBio = contributorShortBio;
    }

    public String getContributorPhotoURL() {
        return contributorPhotoURL;
    }

    public void setContributorPhotoURL(String contributorPhotoURL) {
        this.contributorPhotoURL = contributorPhotoURL;
    }

    public String getContributorID() {
        return contributorID;
    }

    public void setContributorID(String contributorID) {
        this.contributorID = contributorID;
    }

    public long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(long dateCreated) {
        this.dateCreated = dateCreated;
        this.dateCreatedReversed = dateCreated * -1;
    }

    public long getDateCreatedReversed() {
        return dateCreatedReversed;
    }

    public void setDateCreatedReversed(long dateCreatedReversed) {
        this.dateCreatedReversed = dateCreatedReversed;
    }
}
