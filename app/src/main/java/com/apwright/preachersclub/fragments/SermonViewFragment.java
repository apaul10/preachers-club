package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.SermonReadActivity;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.EmptyRecyclerView;
import com.apwright.preachersclub.model.Sermon;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.io.Console;

/**
 * Created by apwright on 6/16/2017.
 */

public class SermonViewFragment extends Fragment {

    private EmptyRecyclerView mRecyclerView;
    View mEmptyView;
    View mOfflineView;

    private FirebaseDatabase mDatabase;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    private Query mSermonQuery;

    public SermonViewFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sermon_view, container, false);

        mDatabase = FirebaseDatabase.getInstance();
        mSermonQuery = mDatabase.getReference()
                .child(Utility.REFERENCE_SERMONS)
                .orderByChild("modifiedDateReversed");

        mRecyclerView = (EmptyRecyclerView) rootView.findViewById(R.id.sermons_recycler_view);
        //mRecyclerView.setHasFixedSize(true);
        //mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new StaggeredGridLayoutManager(2, 1));

        mEmptyView = rootView.findViewById(R.id.empty_view_sermon);
        mOfflineView = rootView.findViewById(R.id.empty_view_offline);

        configureEmptyView();

        mFirebaseAdapter = new FirebaseRecyclerAdapter<Sermon, SermonViewHolder>(
                Sermon.class,
                R.layout.item_sermon,
                SermonViewHolder.class,
                mSermonQuery
        ) {
            @Override
            protected void populateViewHolder(SermonViewHolder viewHolder, final Sermon model, int position) {
                final String fireBaseKey = getRef(position).getKey();

                viewHolder.setTitle(model.getSermonTitle());
                viewHolder.setKeyText(model.getSermonKeyText());
                viewHolder.setSermonIdea(model.getSermonIdea());

                viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), SermonReadActivity.class);
                        intent.putExtra(Sermon.SERMON_KEY, model);
                        intent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, fireBaseKey);
                        startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), v, "sermonTopic").toBundle());
                    }
                });

            }
        };

        mRecyclerView.setAdapter(mFirebaseAdapter);

        return rootView;
    }

    private void configureEmptyView() {
        if (mOfflineView != null && mEmptyView != null) {
            if (Utility.isOnline(getActivity())) {
                mOfflineView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
                mRecyclerView.setEmptyView(mEmptyView);
            } else {
                mOfflineView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
                mRecyclerView.setEmptyView(mOfflineView);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            configureEmptyView();
        }
    }

    public static class SermonViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        LinearLayout mOverlay;
        private TextView mSermonTitle;
        private TextView mSermonKeyText;
        private TextView mSermonIdea;

        public SermonViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.sermon_card_view);
            mOverlay = (LinearLayout) itemView.findViewById(R.id.sermon_overlay_linear_layout);
            mSermonTitle = (TextView) itemView.findViewById(R.id.sermon_title_text_view);
            mSermonKeyText = (TextView) itemView.findViewById(R.id.sermon_key_text_text_view);
            mSermonIdea = (TextView) itemView.findViewById(R.id.sermon_idea_text_view);

            mOverlay.setBackgroundColor(Utility.getRandomColor(60));

        }

        public void setTitle(String title) {
            mSermonTitle.setText(title);
        }

        public void setKeyText(String keyText) {
            mSermonKeyText.setText(keyText);
        }

        public void setSermonIdea(String idea) {
            mSermonIdea.setText(idea);
        }
    }
}
