package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Sermon;
import com.firebase.ui.auth.ResultCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by apwright on 8/13/2017.
 */

public class SermonIllustrationEditFragment extends Fragment {

    private TextInputLayout mTitleLayout;
    private TextInputLayout mContentLayout;
    private TextInputLayout mReferenceLayout;

    private TextInputEditText mTitle;
    private TextInputEditText mContent;
    private TextInputEditText mReference;

    private Button mSave;
    private Button mCancel;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDatabaseReference;

    private Sermon mSermon;
    private String mFireBaseKey;
    private int mPosition;

    private boolean mEditMode;

    private Context mContext;

    public SermonIllustrationEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sermon_illustration_edit, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mContext = getContext();
        mSermon = new Sermon();

        mTitleLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_illustration_edit_title_text_input_layout);
        mContentLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_illustration_edit_content_text_input_layout);
        mReferenceLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_illustration_edit_reference_text_input_layout);

        mTitle = (TextInputEditText) rootView.findViewById(R.id.sermon_illustration_edit_title_text_view);
        mContent = (TextInputEditText) rootView.findViewById(R.id.sermon_illustration_edit_content_text_view);
        mReference = (TextInputEditText) rootView.findViewById(R.id.sermon_illustration_edit_reference_text_view);

        mSave = (Button) rootView.findViewById(R.id.sermon_illustration_edit_save_button);
        mCancel = (Button) rootView.findViewById(R.id.sermon_illustration_edit_cancel_button);

        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveData()) {
                    //save and close activity
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(Sermon.SERMON_KEY, mSermon);
                    resultIntent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseKey);
                    getActivity().setResult(ResultCodes.OK, resultIntent);
                    getActivity().finish();
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_KEY)) {
            mSermon = (Sermon) getActivity().getIntent().getExtras().getParcelable(Sermon.SERMON_KEY);
        }

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_FIRE_BASE_KEY)) {
            mFireBaseKey = (String) getActivity().getIntent().getStringExtra(Sermon.SERMON_FIRE_BASE_KEY);
        }

        if (getActivity().getIntent().hasExtra("EditMode")) {
            if (mSermon != null) {

                mEditMode = true;
                mPosition = (Integer) getActivity().getIntent().getIntExtra("EditMode", 0);

                //load data
                Sermon.Illustration illustration = mSermon.getIllustrationList().get(mPosition);
                mTitle.setText(illustration.getTitle());
                mContent.setText(illustration.getContent());
                mReference.setText(illustration.getReference_source());
            }

        }

        return rootView;
    }

    private boolean saveData() {
        if (mUser == null) {
            Utility.showSnackBar(getView(), "Sign in to add Illustration");
            return false;
        }

        if (!validateFields()) {
            return false;
        }

        String title = Utility.toTitleCase(mTitle.getText().toString());
        String content = mContent.getText().toString();
        String reference = mReference.getText().toString();

        Sermon.Illustration illustration = new Sermon.Illustration();
        illustration.setTitle(title);
        illustration.setContent(content);
        illustration.setReference_source(reference);

        if (mEditMode) {
            //edit illustration
            mSermon.setIllustrationsByPosition(illustration, mPosition);
        } else {
            //add illustration
            mSermon.addIllustration(illustration);
        }

        if (!mFireBaseKey.isEmpty()) {
            mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).setValue(mSermon);
            Utility.showToast(getContext(), "Sermon " + mSermon.getSermonTitle() + " updated");
        }


        return true;
    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mTitleLayout.setError(null);
        this.mContentLayout.setError(null);
        this.mReferenceLayout.setError(null);

        if (mTitle.getText().toString().isEmpty()) {
            fieldsValid = false;
            mTitleLayout.setError("Please state what your illustration will be about");
        }

        if (mContent.getText().toString().isEmpty()) {
            fieldsValid = false;
            mContentLayout.setError("Please describe your sermon illustration");
        }

//        if (mReference.getText().toString().isEmpty()) {
//            fieldsValid = false;
//            mReferenceLayout.setError("Please state any references");
//        }

        return fieldsValid;
    }
}
