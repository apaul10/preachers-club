package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apwright.preachersclub.PixaBayPhotoChooserActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.ActivityFeed;
import com.apwright.preachersclub.model.DifficultQuestion;
import com.apwright.preachersclub.model.ImageContent;
import com.bumptech.glide.Glide;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by apwright on 6/27/2017.
 */

public class DifficultQuestionEditFragment extends Fragment {

    private static final int RC_GET_IMAGE = 106;
    private static int RC_PHOTO_CHOOSER = 107;

    private ImageView mPhoto;
    private TextInputLayout mQuestionLayout;
    private TextInputLayout mFurtherReadingLayout;
    private TextInputLayout mScripturesLayout;
    private TextInputLayout mSummaryLayout;
    private TextInputLayout mDetailsLayout;
    private TextInputEditText mQuestion;
    private TextInputEditText mFurtherReading;
    private TextInputEditText mScriptures;
    private TextInputEditText mSummary;
    private TextInputEditText mDetails;

    private DifficultQuestion mDifficultQuestion;
    private Uri mSelectedImage;
    private ImageContent mSelectedImageContent;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private FirebaseStorage mStorage;
    private StorageReference mImageStorageReference;

    private String mFireBaseKey;

    private boolean mImageSelected = false;

    private boolean mEditMode = false;

    private Context mContext;
    private View mView;

    public DifficultQuestionEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_difficult_question_edit, container, false);

        setRetainInstance(true);
        setHasOptionsMenu(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        mContext = getActivity().getApplicationContext();
        mView = getView();

        mDifficultQuestion = new DifficultQuestion();

        mPhoto = (ImageView) rootView.findViewById(R.id.difficult_question_edit_photo_image_view);
        mQuestionLayout = (TextInputLayout) rootView.findViewById(R.id.difficult_question_edit_question_text_input_layout);
        mFurtherReadingLayout = (TextInputLayout) rootView.findViewById(R.id.difficult_question_edit_further_reading_input_layout);
        mScripturesLayout = (TextInputLayout) rootView.findViewById(R.id.difficult_question_edit_scripture_text_input_layout);
        mSummaryLayout = (TextInputLayout) rootView.findViewById(R.id.difficult_question_edit_summary_text_input_layout);
        mDetailsLayout = (TextInputLayout) rootView.findViewById(R.id.difficult_question_edit_details_text_input_layout);

        mQuestion = (TextInputEditText) rootView.findViewById(R.id.difficult_question_edit_question_text_view);
        mFurtherReading = (TextInputEditText) rootView.findViewById(R.id.difficult_question_edit_further_reading_text_view);
        mScriptures = (TextInputEditText) rootView.findViewById(R.id.difficult_question_edit_scripture_text_view);
        mSummary = (TextInputEditText) rootView.findViewById(R.id.difficult_question_edit_summary_text_view);
        mDetails = (TextInputEditText) rootView.findViewById(R.id.difficult_question_edit_details_text_view);

        //open gallery chooser when photo icon is selected
        mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPhotoChooser();
            }
        });

        //check if edit mode is applicable
        if (getActivity().getIntent().hasExtra(DifficultQuestion.DIFFICULT_QUESTION_KEY) && getActivity().getIntent().hasExtra(DifficultQuestion.DIFFICULT_QUESTION_FIRE_BASE_KEY)) {
            DifficultQuestion difficultQuestion = (DifficultQuestion) getActivity().getIntent().getExtras().getParcelable(DifficultQuestion.DIFFICULT_QUESTION_KEY);
            if (difficultQuestion != null) {
                mDifficultQuestion = difficultQuestion;
                mEditMode = true;
                mFireBaseKey = (String) getActivity().getIntent().getStringExtra(DifficultQuestion.DIFFICULT_QUESTION_FIRE_BASE_KEY);

                mQuestion.setText(mDifficultQuestion.getQuestionDescription());
                mFurtherReading.setText(mDifficultQuestion.getFurtherReadingURL());
                mScriptures.setText(mDifficultQuestion.getBibleVerses().toString());
                mSummary.setText(mDifficultQuestion.getResponseSummary());
                mDetails.setText(mDifficultQuestion.getResponseDetail());
                loadPhotoImage(mDifficultQuestion.getPhotoURL());
            }
        }


        return rootView;
    }

    private void openGalleryChooser() {
        if (Utility.askForPermission(mContext, Utility.PERMISSION_EXTERNAL_STORAGE, RC_GET_IMAGE, DifficultQuestionEditFragment.this)) {
            Intent galleryIntent = new Intent("android.intent.action.PICK");
            galleryIntent.setType("image/*");
            startActivityForResult(Intent.createChooser(galleryIntent, "Choose a Photo for your Difficult Question"), RC_GET_IMAGE);
        }
    }

    private void openPhotoChooser() {
        Intent intent = new Intent(getActivity(), PixaBayPhotoChooserActivity.class);
        startActivityForResult(intent, RC_PHOTO_CHOOSER);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_cancel, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean closeFragment = true;
        switch (item.getItemId()) {
            case R.id.menu_save:
                closeFragment = saveData();
                break;
        }
        if (closeFragment) {
            getActivity().finish();
        }

        return true;
    }

    private boolean saveData() {

        if (mUser == null) {
            Utility.showSnackBar(getView(), "Sign in to add Difficult Question");
            return false;
        }

        if (!mImageSelected && mEditMode == false) {

            Snackbar.make(getView(), "Choose Image for your Difficult Question", Snackbar.LENGTH_LONG)
                    .setAction("Add Now", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openPhotoChooser();
                        }
                    })
                    .show();
            return false;
        }

        if (!validateFields()) {
            return false;
        }
        String question = Utility.toTitleCase(this.mQuestion.getText().toString());
        String further_reading = this.mFurtherReading.getText().toString();
        String scriptures = this.mScriptures.getText().toString();
        String summary_answer = this.mSummary.getText().toString();
        String detail_answer = this.mDetails.getText().toString();
        String contributor = mUser.getDisplayName();
        String contributor_id = mUser.getUid();

        List<String> scripturesList = new ArrayList<String>();
        scripturesList.add(scriptures);

        mDifficultQuestion.setQuestionDescription(question);
        mDifficultQuestion.setFurtherReadingURL(further_reading);
        mDifficultQuestion.setBibleVerses(scripturesList);
        mDifficultQuestion.setResponseSummary(summary_answer);
        mDifficultQuestion.setResponseDetail(detail_answer);

        mDifficultQuestion.setContributorName(contributor);
        mDifficultQuestion.setContributorID(contributor_id);
        mDifficultQuestion.setDateCreated(System.currentTimeMillis());


        if (!mEditMode) {
            uploadPhotoImageAndSaveData();
        } else {
            mDatabase.getReference().child(Utility.REFERENCE_QUESTIONS).child(mFireBaseKey).setValue(mDifficultQuestion);
            Utility.showToast(getContext(), "Difficult Question " + question + " updated");
        }

        return true;
    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mQuestionLayout.setError(null);
        this.mFurtherReadingLayout.setError(null);
        this.mScripturesLayout.setError(null);
        this.mSummaryLayout.setError(null);
        this.mDetailsLayout.setError(null);

        if (this.mQuestion.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mQuestionLayout.setError("Question is required");
        }
        if (!this.mFurtherReading.getText().toString().isEmpty() && !Utility.isURLValid(mFurtherReading.getText().toString())) {
            fieldsValid = false;
            this.mFurtherReadingLayout.setError("A valid URL is required");
        }
        if (this.mScriptures.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mScripturesLayout.setError("Please add scriptural references to support your answers");
        }
        if (this.mSummary.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mSummaryLayout.setError("Please add a short summary response your answer");
        }
        if (this.mDetails.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mDetailsLayout.setError("Please add a detailed response for your answer");
        }
        return fieldsValid;
    }

    private void uploadPhotoImageAndSaveData() {

        String fileName = Utility.getGenericFileName("diff_ques", "jpeg");

        mImageStorageReference = mStorage.getReference().child(Utility.REFERENCE_QUESTIONS_IMAGES).child(fileName);

        //get image from ImageView
        mPhoto.setDrawingCacheEnabled(true);
        mPhoto.buildDrawingCache();
        Bitmap bitmap = mPhoto.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        //upload image
        UploadTask uploadTask = mImageStorageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if (getView() == null) {
                    Utility.showToast(mContext, "Difficult Question creation failed");
                } else {
                    Snackbar.make(getView(), "Difficult Question creation failed", Snackbar.LENGTH_LONG)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uploadPhotoImageAndSaveData();
                                }
                            })
                            .show();
                }


            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                @SuppressWarnings("VisibleForTests")
                Uri downloadUri = taskSnapshot.getDownloadUrl();

                mDifficultQuestion.setPhotoURL(downloadUri.toString());

                String fireBaseKey = (String) mDatabase.getReference().child(Utility.REFERENCE_QUESTIONS).push().getKey();

                //save contributor
                mDatabase.getReference().child(Utility.REFERENCE_QUESTIONS).child(fireBaseKey).setValue(mDifficultQuestion);

                Utility.showToast(mContext, "Difficult Question added");

            }
        });

    }

    private void loadPhotoImage(String url) {
        Glide.with(getContext()).load(url).into(mPhoto);
        mPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GET_IMAGE) {
            if (resultCode == ResultCodes.OK) {
                //retrieve selected image
                Uri imageData = data.getData();
                mSelectedImage = imageData;
                if (mSelectedImage != null) {
                    Glide.with(mContext).load(mSelectedImage).centerCrop().into(mPhoto);
                    mImageSelected = true;
                }
            }
        }
        if (requestCode == RC_PHOTO_CHOOSER) {
            if (resultCode == ResultCodes.OK) {
                //retrieve selected image
                Bundle bundle = data.getExtras();

                mSelectedImageContent = new ImageContent();
                mSelectedImageContent = (ImageContent) bundle.get(ImageContent.IMAGE_CONTENT_KEY);

                if (mSelectedImageContent != null) {
                    loadPhotoImage(mSelectedImageContent.getPreviewURL());
                    mImageSelected = true;
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int i;
        switch (requestCode) {
            case RC_GET_IMAGE:
                if (grantResults.length > 0) {
                    i = grantResults[0];
                    getActivity().getPackageManager();
                    if (i == 0) {
                        openGalleryChooser();
                        return;
                    }
                    return;
                }
                return;

            default:
                return;
        }

    }
}
