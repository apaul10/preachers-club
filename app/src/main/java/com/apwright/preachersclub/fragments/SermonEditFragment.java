package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.SermonReadActivity;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.EmptyRecyclerView;
import com.apwright.preachersclub.model.ImageContent;
import com.apwright.preachersclub.model.Sermon;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

/**
 * Created by apwright on 6/16/2017.
 */

public class SermonEditFragment extends Fragment {

    private EmptyRecyclerView mRecyclerView;
    View mEmptyView;
    View mOfflineView;

    private ImageView mSermonPhoto;
    private TextInputLayout mSermonTopicLayout;
    private TextInputLayout mKeyTextLayout;
    private TextInputLayout mSubjectLayout;
    private TextInputLayout mComplementLayout;
    private TextInputLayout mIdeaLayout;
    private TextInputLayout mTakeawayLayout;
    private TextInputEditText mSermonTopic;
    private TextInputEditText mKeyText;
    private TextInputEditText mSubject;
    private TextInputEditText mComplement;
    private TextInputEditText mIdea;
    private TextInputEditText mTakeaway;

    private Button mSave;
    private Button mCancel;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private FirebaseStorage mStorage;
    private StorageReference mImageStorageReference;

    private String mFireBaseSermonKey;

    private boolean mEditMode = false;

    private Context mContext;
    private View mView;
    private Sermon mSermon;
    private ImageContent mSelectedImage;

    public SermonEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sermon_edit, container, false);

        setRetainInstance(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        mContext = getContext();
        mSermon = new Sermon();

        mSermonTopicLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_edit_topic_text_input_layout);
        mKeyTextLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_edit_key_text_text_input_layout);
        mSubjectLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_edit_subject_text_input_layout);
        mComplementLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_edit_complement_text_input_layout);
        mIdeaLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_edit_idea_text_input_layout);
        mTakeawayLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_edit_congregation_takeaway_text_input_layout);

        mSermonTopic = (TextInputEditText) rootView.findViewById(R.id.sermon_edit_topic_text_view);
        mKeyText = (TextInputEditText) rootView.findViewById(R.id.sermon_edit_key_text_text_view);
        mSubject = (TextInputEditText) rootView.findViewById(R.id.sermon_edit_subject_text_view);
        mComplement = (TextInputEditText) rootView.findViewById(R.id.sermon_edit_complement_text_view);
        mIdea = (TextInputEditText) rootView.findViewById(R.id.sermon_edit_idea_text_view);
        mTakeaway = (TextInputEditText) rootView.findViewById(R.id.sermon_edit_congregation_takeaway_text_view);

        mSave = (Button) rootView.findViewById(R.id.sermon_edit_save_button);
        mCancel = (Button) rootView.findViewById(R.id.sermon_edit_cancel_button);


        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveData()) {
                    //ask user if they would like to continue editing sermon
                    MaterialDialog materialDialog = new MaterialDialog.Builder(mContext)
                            .title("Continue building Sermon")
                            .content("Do you wish to continue working on this sermon")
                            .positiveText("Continue")
                            .negativeText("No")
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    if (mFireBaseSermonKey != null) {
                                        if (!mFireBaseSermonKey.isEmpty() && mSermon != null) {
                                            //save data, and load sermon detail editor
                                            Intent intent = new Intent(getActivity(), SermonReadActivity.class);
                                            intent.putExtra(Sermon.SERMON_KEY, mSermon);
                                            intent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseSermonKey);
                                            startActivity(intent);
                                        }
                                    }
                                }
                            }).onNegative(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    //save and close activity
                                    getActivity().finish();
                                }
                            })
                            .build();

                    //show dialog
                    materialDialog.show();
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //close activity
                getActivity().finish();
            }
        });

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_KEY) && getActivity().getIntent().hasExtra(Sermon.SERMON_FIRE_BASE_KEY)) {

            Sermon sermon = (Sermon) getActivity().getIntent().getExtras().getParcelable(Sermon.SERMON_KEY);
            if (sermon != null) {
                mSermon = sermon;
                mEditMode = true;
                mFireBaseSermonKey = (String) getActivity().getIntent().getStringExtra(Sermon.SERMON_FIRE_BASE_KEY);

                mSermonTopic.setText(mSermon.getSermonTitle());
                mKeyText.setText(mSermon.getSermonKeyText());
                mSubject.setText(mSermon.getSermonSubject());
                mComplement.setText(mSermon.getSermonComplement());
                mIdea.setText(mSermon.getSermonIdea());
                mTakeaway.setText(mSermon.getSermonTakeaway());
            }
        }

        return rootView;
    }

    private boolean saveData() {
        if (mUser == null) {
            Utility.showSnackBar(getView(), "Sign in to add Sermon");
            return false;
        }

        if (!validateFields()) {
            return false;
        }

        String topic = Utility.toTitleCase(mSermonTopic.getText().toString());
        String subject = Utility.toTitleCase(mSubject.getText().toString());
        String complement = mComplement.getText().toString();
        String idea = mIdea.getText().toString();
        String takeaway = mTakeaway.getText().toString();
        String keyText = mKeyText.getText().toString();
        String photoURL = "";

        String contributorId = mUser.getUid();
        String contributorName = mUser.getDisplayName();
        String contributorEmail = mUser.getEmail();

        mSermon.setSermonTitle(topic);
        mSermon.setSermonKeyText(keyText);
        mSermon.setSermonSubject(subject);
        mSermon.setSermonComplement(complement);
        mSermon.setSermonIdea(idea);
        mSermon.setSermonTakeaway(takeaway);

        mSermon.setContributorId(contributorId);
        mSermon.setContributorName(contributorName);
        mSermon.setContributorEmailAddress(contributorEmail);

        if (!mEditMode) {
            uploadPhotoImageAndSaveData();
        } else {
            mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseSermonKey).setValue(mSermon);
            //Utility.showToast(getContext(), "Sermon " + mSermon.getSermonTitle() + " updated");
        }

        return true;
    }

    private void uploadPhotoImageAndSaveData() {

        mFireBaseSermonKey = mDatabase.getReference().child(Utility.REFERENCE_SERMONS).push().getKey();
        mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseSermonKey).setValue(mSermon);
        //Utility.showToast(mContext, "Sermon created: " + mSermon.getSermonTitle());

        //TODO: upload image
//
//
//        mImageStorageReference = mStorage.getReference().child(Utility.REFERENCE_SERMONS_IMAGES).child(mSelectedImage.getImageName());
//
//        //get image from ImageView
//        mSermonPhoto.setDrawingCacheEnabled(true);
//        mSermonPhoto.buildDrawingCache();
//        Bitmap bitmap = mSermonPhoto.getDrawingCache();
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
//        byte[] data = baos.toByteArray();
//
//        //upload image
//        UploadTask uploadTask = mImageStorageReference.putBytes(data);
//        uploadTask.addOnFailureListener(new OnFailureListener() {
//            @Override
//            public void onFailure(@NonNull Exception e) {
//
//                if (getView() == null) {
//
//                    Utility.showToast(mContext, "Sermon creation failed");
//
//                } else {
//
//                    Snackbar.make(getView(), "Sermon creation failed", Snackbar.LENGTH_LONG)
//                            .setAction("Retry", new View.OnClickListener() {
//                                @Override
//                                public void onClick(View v) {
//                                    uploadPhotoImageAndSaveData();
//                                }
//                            })
//                            .show();
//                }
//
//            }
//        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
//            @Override
//            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
//
//                @SuppressWarnings("VisibleForTests")
//                Uri downloadUri = taskSnapshot.getDownloadUrl();
//
//                mSermon.setPhotoURL(downloadUri.toString());
//
//                String fireBaseKey = (String) mDatabase.getReference().child(Utility.REFERENCE_SERMONS).push().getKey();
//
//                mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(fireBaseKey).setValue(mSermon);
//
//
//                Utility.showToast(mContext, "Sermon created: " + mSermon.getSermonTitle());
//
//            }
//        });

    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mSermonTopicLayout.setError(null);
        this.mKeyTextLayout.setError(null);
        this.mSubjectLayout.setError(null);
        this.mComplementLayout.setError(null);
        this.mIdeaLayout.setError(null);
        this.mTakeawayLayout.setError(null);

        if (mSermonTopic.getText().toString().isEmpty()) {
            fieldsValid = false;
            mSermonTopicLayout.setError("A Topic is required");
        }

        if (mKeyText.getText().toString().isEmpty()) {
            fieldsValid = false;
            mKeyTextLayout.setError("A Key Text is required");
        }

        if (mIdea.getText().toString().isEmpty()) {
            fieldsValid = false;
            mIdeaLayout.setError("An Idea is required");
        }

        return fieldsValid;
    }


}
