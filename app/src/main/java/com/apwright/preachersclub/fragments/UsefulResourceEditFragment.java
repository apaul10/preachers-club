package com.apwright.preachersclub.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.apwright.preachersclub.PixaBayPhotoChooserActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.api.GetWebsiteMetaData;
import com.apwright.preachersclub.model.ActivityFeed;
import com.apwright.preachersclub.model.ImageContent;
import com.apwright.preachersclub.model.UsefulResource;
import com.apwright.preachersclub.model.WebMetaData;
import com.bumptech.glide.Glide;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.util.concurrent.ExecutionException;

/**
 * Created by apwright on 6/24/2017.
 */

public class UsefulResourceEditFragment extends Fragment {

    private static int RC_PHOTO_CHOOSER = 4013;

    private ImageView mPhoto;
    private TextInputLayout mResourceUrlLayout;
    private TextInputLayout mResourceTitleLayout;
    private TextInputLayout mResourceDescriptionLayout;
    private TextInputEditText mResourceURL;
    private TextInputEditText mResourceDescription;
    private TextInputEditText mResourceTitle;
    private Button mGetWebsiteDataButton;

    private UsefulResource mUsefulResource;
    private ImageContent mImageContent;
    private String mImageURL;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private FirebaseStorage mStorage;
    private StorageReference mImageStorageReference;

    private String mFireBaseUsefulResourceKey;

    private boolean mEditMode = false;

    private Context mContext;
    private View mView;

    ProgressDialog mProgressDialog;

    public UsefulResourceEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_useful_resource_edit, container, false);

        setRetainInstance(true);
        setHasOptionsMenu(true);

        mImageURL = "";

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        mUsefulResource = new UsefulResource();
        mContext = getContext();
        mView = getView();

        mProgressDialog = new ProgressDialog(mContext);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        mPhoto = (ImageView) rootView.findViewById(R.id.useful_resource_edit_photo_image_view);
        mResourceURL = (TextInputEditText) rootView.findViewById(R.id.useful_resource_edit_url_text_view);
        mResourceTitle = (TextInputEditText) rootView.findViewById(R.id.useful_resource_edit_title_text_view);
        mResourceDescription = (TextInputEditText) rootView.findViewById(R.id.useful_resource_edit_description_text_view);
        mGetWebsiteDataButton = (Button) rootView.findViewById(R.id.useful_resource_get_data_button);
        mResourceUrlLayout = (TextInputLayout) rootView.findViewById(R.id.useful_resource_edit_url_input_layout);
        mResourceTitleLayout = (TextInputLayout) rootView.findViewById(R.id.useful_resource_edit_title_text_input_layout);
        mResourceDescriptionLayout = (TextInputLayout) rootView.findViewById(R.id.useful_resource_edit_description_text_input_layout);


        //open photo chooser when photo icon is selected
        mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPhotoChooser();
            }
        });

        //search for website meta data
        mGetWebsiteDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String resourceURL = mResourceURL.getText().toString().trim();
                if (Utility.isURLValid(resourceURL)) {
                    showProgressDialog();
                    if (Utility.isOnline(mContext)) {
                        searchForMetaDataFromURL(resourceURL);
                    } else {
                        Utility.showSnackBar(v, "No Internet Connection");
                    }
                    hideProgressDialog();
                } else {
                    Utility.showSnackBar(v, "Please enter a valid URL");
                }
            }
        });

        //check if edit mode is applicable
        if (getActivity().getIntent().hasExtra(UsefulResource.USEFUL_RESOURCE_KEY) && getActivity().getIntent().hasExtra(UsefulResource.USEFUL_RESOURCE_FIRE_BASE_KEY)) {
            UsefulResource usefulResource = (UsefulResource) getActivity().getIntent().getExtras().getParcelable(UsefulResource.USEFUL_RESOURCE_KEY);
            if (usefulResource != null) {
                mUsefulResource = usefulResource;
                mEditMode = true;
                mFireBaseUsefulResourceKey = (String) getActivity().getIntent().getStringExtra(UsefulResource.USEFUL_RESOURCE_FIRE_BASE_KEY);

                mResourceURL.setText(mUsefulResource.getResourceURL());
                mResourceTitle.setText(mUsefulResource.getResourceTitle());
                mResourceDescription.setText(mUsefulResource.getResourceDescription());
                loadPhotoImage(mUsefulResource.getResourceImageURL());

            }
        }


        return rootView;
    }

    private void showProgressDialog() {
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    private void hideProgressDialog() {
        if (mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

    private void searchForMetaDataFromURL(String resourceURL) {

        if (resourceURL != null) {
            if (!resourceURL.isEmpty()) {

                GetWebsiteMetaData websiteMetaDataTask = new GetWebsiteMetaData();
                WebMetaData metaData = new WebMetaData();
                try {
                    metaData = websiteMetaDataTask.execute(resourceURL).get();

                    if (metaData != null) {

                        if (!metaData.getImageURL().isEmpty()) {

                            mUsefulResource.preloadMetaData(metaData);
                            mResourceTitle.setText(mUsefulResource.getResourceTitle());
                            mResourceDescription.setText(mUsefulResource.getResourceDescription());
                            mImageURL = mUsefulResource.getResourceImageURL();
                            loadPhotoImage(mImageURL);

                        } else {

                            Utility.showSnackBar(mView, "Could not get any data from provided URL");
                        }
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private void loadPhotoImage(String url) {
        Glide.with(getContext()).load(url).into(mPhoto);
        mPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    private void openPhotoChooser() {
        Intent intent = new Intent(getActivity(), PixaBayPhotoChooserActivity.class);
        startActivityForResult(intent, RC_PHOTO_CHOOSER);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_cancel, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean closeFragment = true;
        switch (item.getItemId()) {
            case R.id.menu_save:
                closeFragment = saveData();
                break;
        }
        if (closeFragment) {
            getActivity().finish();
        }

        return true;
    }

    private boolean saveData() {
        if (mUser == null) {
            Utility.showSnackBar(mView, "Sign in to add Useful Resource");
            return false;
        }

        if (mImageURL.isEmpty()) {

            Snackbar.make(getView(), "Choose Image for your Useful Resource", Snackbar.LENGTH_LONG)
                    .setAction("Add Now", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openPhotoChooser();
                        }
                    })
                    .show();
            return false;
        }

        if (!validateFields()) {
            return false;
        }
        String title = Utility.toTitleCase(this.mResourceTitle.getText().toString());
        String resourceURL = Utility.toTitleCase(this.mResourceURL.getText().toString());
        String description = this.mResourceDescription.getText().toString();
        String contributorId = mUser.getUid();
        String contributorName = mUser.getDisplayName();


        mUsefulResource.setResourceTitle(title);
        mUsefulResource.setResourceURL(resourceURL);
        mUsefulResource.setResourceDescription(description);
        mUsefulResource.setResourceImageURL(mImageURL);
        mUsefulResource.setContributorName(contributorName);
        mUsefulResource.setContributorID(contributorId);
        mUsefulResource.setSubmissionDate(System.currentTimeMillis());

        if (!mEditMode) {
            //create new record
            uploadPhotoImageAndSaveData();
        } else {
            //update existing record
            mDatabase.getReference().child(Utility.REFERENCE_USEFUL_RESOURCES).child(mFireBaseUsefulResourceKey).setValue(mUsefulResource);
            Utility.showToast(getContext(), "Useful Resource " + mUsefulResource.getResourceTitle() + " updated");
        }

        return true;
    }

    private void uploadPhotoImageAndSaveData() {

        mImageStorageReference = mStorage.getReference().child(Utility.REFERENCE_USEFUL_RESOURCE_IMAGES).child(Utility.getImageFileName(mImageURL));

        //get image from ImageView
        mPhoto.setDrawingCacheEnabled(true);
        mPhoto.buildDrawingCache();
        Bitmap bitmap = mPhoto.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        //upload image
        UploadTask uploadTask = mImageStorageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if(mView == null){
                    Utility.showToast(mContext, "Useful Resource creation failed");
                }else{
                    Snackbar.make(mView, "Useful Resource creation failed", Snackbar.LENGTH_LONG)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uploadPhotoImageAndSaveData();
                                }
                            })
                            .show();
                }


            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                @SuppressWarnings("VisibleForTests")
                Uri downloadUri = taskSnapshot.getDownloadUrl();

                mUsefulResource.setResourceImageURL(downloadUri.toString());

                String fireBaseKey = (String) mDatabase.getReference().child(Utility.REFERENCE_USEFUL_RESOURCES).push().getKey();

                mDatabase.getReference().child(Utility.REFERENCE_USEFUL_RESOURCES).child(fireBaseKey).setValue(mUsefulResource);

                Utility.showToast(mContext, "Useful Resource added: " + mUsefulResource.getResourceTitle());

            }
        });


    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mResourceTitleLayout.setError(null);
        this.mResourceUrlLayout.setError(null);
        this.mResourceDescriptionLayout.setError(null);

        if (mResourceURL.getText().toString().isEmpty() || !Utility.isURLValid(mResourceURL.getText().toString())) {
            fieldsValid = false;
            mResourceUrlLayout.setError("Valid URL is required");
        }
        if (mResourceTitle.getText().toString().isEmpty()) {
            fieldsValid = false;
            mResourceTitleLayout.setError("Title is required");
        }
        if (mResourceDescription.getText().toString().isEmpty()) {
            fieldsValid = false;
            mResourceDescriptionLayout.setError("Description is required");
        }
        return fieldsValid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PHOTO_CHOOSER) {
            if (resultCode == ResultCodes.OK) {
                //retrieve selected image
                Bundle bundle = data.getExtras();

                mImageContent = new ImageContent();
                mImageContent = (ImageContent) bundle.get(ImageContent.IMAGE_CONTENT_KEY);

                if (mImageContent != null) {
                    mImageURL = mImageContent.getPreviewURL();
                    loadPhotoImage(mImageContent.getPreviewURL());
                }
            }
        }
    }

}
