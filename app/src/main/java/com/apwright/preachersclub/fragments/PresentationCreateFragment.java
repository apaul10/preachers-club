package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apwright.preachersclub.NoteEditActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Sermon;
import com.apwright.preachersclub.model.SermonPresentation;
import com.firebase.ui.auth.ResultCodes;
import com.github.irshulx.Editor;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by apwright on 9/15/2017.
 */

public class PresentationCreateFragment extends Fragment {

    Button mUpdateIntroduction;
    Button mUpdateBody;
    Button mUpdateConclusion;
    Button mSelectSermon;

    Editor mIntroduction;
    Editor mBody;
    Editor mConclusion;

    TextInputLayout mTitleLayout;
    TextInputEditText mTitle;

    Sermon mRelatedSermon;
    String mFireBaseKey = "";

    FirebaseDatabase mDatabase;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    public static final int RC_INTRO = 455;
    public static final int RC_BODY = 456;
    public static final int RC_CONCLUSION = 457;

    SermonPresentation mSermonPresentation;


    public PresentationCreateFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_presentation_create, container, false);

        mSermonPresentation = new SermonPresentation();

        initializeViews(rootView);
        setHasOptionsMenu(true);

        if (getActivity().getIntent().hasExtra(SermonPresentation.PRESENTATION_FIRE_BASE_KEY)) {
            mFireBaseKey = getActivity().getIntent().getStringExtra(SermonPresentation.PRESENTATION_FIRE_BASE_KEY);
        }

        if (getActivity().getIntent().hasExtra(SermonPresentation.PRESENTATION_KEY)) {
            Bundle bundle = getActivity().getIntent().getExtras();
            SermonPresentation presentation = bundle.getParcelable(SermonPresentation.PRESENTATION_KEY);
            mSermonPresentation = presentation;
            loadPresentationContent(presentation);
        }


        return rootView;
    }

    private void initializeViews(View rootView) {
        mUpdateIntroduction = (Button) rootView.findViewById(R.id.updateIntroductionButton);
        mUpdateBody = (Button) rootView.findViewById(R.id.updateBodyButton);
        mUpdateConclusion = (Button) rootView.findViewById(R.id.updateConclusionButton);
        mSelectSermon = (Button) rootView.findViewById(R.id.selectSermonButton);

        mIntroduction = rootView.findViewById(R.id.create_introduction_text_view);
        mBody = rootView.findViewById(R.id.create_body_text_view);
        mConclusion = rootView.findViewById(R.id.create_conclusion_text_view);

        mTitle = (TextInputEditText) rootView.findViewById(R.id.create_introduction_title_edit_text);
        mTitleLayout = (TextInputLayout) rootView.findViewById(R.id.create_introduction_title_layout);

//        mIntroduction.render();
//        mBody.render();
//        mConclusion.render();

        mRelatedSermon = new Sermon();

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mUpdateIntroduction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openEditNote(RC_INTRO, mIntroduction == null ? "" : mIntroduction.getContentAsHTML());
                openEditNote(RC_INTRO, mSermonPresentation == null ? "" : mSermonPresentation.getIntroduction());
            }
        });

        mUpdateBody.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openEditNote(RC_BODY, mBody == null ? "" : mBody.getContentAsHTML());
                openEditNote(RC_BODY, mSermonPresentation == null ? "" : mSermonPresentation.getBody());
            }
        });

        mUpdateConclusion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openEditNote(RC_CONCLUSION, mConclusion == null ? "" : mConclusion.getContentAsHTML());
                openEditNote(RC_CONCLUSION, mSermonPresentation == null ? "" : mSermonPresentation.getConclusion());
            }
        });
    }

    private void loadPresentationContent(SermonPresentation presentation) {
        String title = presentation.getPresentationTitle();
        String introduction = presentation.getIntroduction();
        String body = presentation.getBody();
        String conclusion = presentation.getConclusion();

        if (!title.isEmpty()) {
            mTitle.setText(title);
        }
        if (!introduction.isEmpty()) {
            mIntroduction.render(introduction);
        }
        if (!body.isEmpty()) {
            mBody.render(body);
        }
        if (!conclusion.isEmpty()) {
            mConclusion.render(conclusion);
        }
        if (presentation.getReferenceSermonTemplate() != null) {
            mRelatedSermon = presentation.getReferenceSermonTemplate();
        }
    }

    private void openEditNote(int requestCode, String notes) {
        Intent intent = new Intent(getContext(), NoteEditActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, notes);
        startActivityForResult(intent, requestCode);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_save, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:
                if (saveData()) {
                    getActivity().finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean saveData() {
        boolean isSaved = false;
        if (validateFields()) {

            SermonPresentation sermonPresentation = new SermonPresentation();
            String title = mTitle.getText().toString().trim();

            String introduction = mSermonPresentation.getIntroduction().toString().trim();
            String body = mSermonPresentation.getBody().toString().trim();
            String conclusion = mSermonPresentation.getConclusion().toString().trim();

            String contributorId = mUser.getUid();

            if (mFireBaseKey.isEmpty()) {
                sermonPresentation.setCreationDate(System.currentTimeMillis());
            } else {
                sermonPresentation.setModifiedDate(System.currentTimeMillis());
            }
            sermonPresentation.setContributorId(contributorId);
            sermonPresentation.setPresentationTitle(title);
            sermonPresentation.setIntroduction(introduction);
            sermonPresentation.setBody(body);
            sermonPresentation.setConclusion(conclusion);

            if (mRelatedSermon.getSermonTitle() != null) {
                sermonPresentation.setReferenceSermonTemplate(mRelatedSermon);
            }

            //update database
            if (mFireBaseKey.isEmpty()) {
                //new entry
                mDatabase.getReference().child(Utility.REFERENCE_PRESENTATIONS).push().setValue(sermonPresentation);
            } else {
                //existing entry
                mDatabase.getReference().child(Utility.REFERENCE_PRESENTATIONS).child(mFireBaseKey).setValue(sermonPresentation);
            }

            isSaved = true;

        }
        return isSaved;
    }

    private boolean validateFields() {
        boolean fieldsValid = true;

        mTitleLayout.setError(null);
//        mIntroduction.setError(null);
//        mBody.setError(null);
//        mConclusion.setError(null);

        if (mTitle.getText().toString().isEmpty()) {
            fieldsValid = false;
            mTitleLayout.setError("Please enter presentation title");
        }

        return fieldsValid;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ResultCodes.OK) {
            String returnData = data.getStringExtra(Intent.EXTRA_TEXT);
            switch (requestCode) {
                case RC_INTRO:

                    mIntroduction.clearAllContents();

                    mIntroduction.render(returnData);
                    mSermonPresentation.setIntroduction(returnData);
                    break;

                case RC_BODY:

                    mBody.clearAllContents();

                    mBody.render(returnData);
                    mSermonPresentation.setBody(returnData);
                    break;

                case RC_CONCLUSION:

                    mConclusion.clearAllContents();

                    mConclusion.render(returnData);
                    mSermonPresentation.setConclusion(returnData);
                    break;
            }
        }
    }
}
