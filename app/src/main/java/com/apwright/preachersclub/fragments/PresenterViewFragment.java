package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
//import android.support.v7.widget.PopupMenu;
import android.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apwright.preachersclub.PresentationCreateActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.EmptyRecyclerView;
import com.apwright.preachersclub.model.SermonPresentation;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.github.irshulx.Editor;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

/**
 * Created by apwright on 6/16/2017.
 */

public class PresenterViewFragment extends Fragment {

    private EmptyRecyclerView mRecyclerView;
    View mEmptyView;
    View mOfflineView;

    private FirebaseDatabase mDatabase;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    private Query mPresentationQuery;

    public PresenterViewFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_presenter_view, container, false);

        mDatabase = FirebaseDatabase.getInstance();
        mPresentationQuery = mDatabase.getReference()
                .child(Utility.REFERENCE_PRESENTATIONS)
                .orderByChild("modifiedDateReversed");

        mRecyclerView = (EmptyRecyclerView) rootView.findViewById(R.id.presenter_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mEmptyView = rootView.findViewById(R.id.empty_view_presenter);
        mOfflineView = rootView.findViewById(R.id.empty_view_offline);

        configureEmptyView();

        mFirebaseAdapter = new FirebaseRecyclerAdapter<SermonPresentation, PresentationViewHolder>(
                SermonPresentation.class
                , R.layout.item_presentation
                , PresentationViewHolder.class
                , mPresentationQuery
        ) {
            @Override
            protected void populateViewHolder(final PresentationViewHolder viewHolder, final SermonPresentation model, int position) {

                viewHolder.setTitle(model.getPresentationTitle());
                viewHolder.setContributor(model.getContributorId());
                viewHolder.setPresentationDate(model.getFriendlyDate());
                viewHolder.setSummary(model.getSummary());

                final String fireBaseKey = getRef(position).getKey();

                viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        loadPresentationForEdit(fireBaseKey, model);
                    }
                });

                viewHolder.mMenu.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        PopupMenu popupMenu = new PopupMenu(getContext(), v);
                        popupMenu.getMenuInflater().inflate(R.menu.menu_overflow, popupMenu.getMenu());
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                switch (item.getItemId()) {
                                    case R.id.menu_read:
                                        loadPresentationForEdit(fireBaseKey, model);

                                        break;

                                    case R.id.menu_share:
                                        break;

                                    case R.id.menu_print:
                                        break;
                                }
                                return false;
                            }
                        });
                        popupMenu.show();

                    }
                });

            }
        };
        mRecyclerView.setAdapter(mFirebaseAdapter);

        return rootView;
    }

    private void loadPresentationForEdit(String fireBaseKey, SermonPresentation presentation) {
        if (!fireBaseKey.isEmpty() && presentation != null) {
            Intent intent = new Intent(getContext(), PresentationCreateActivity.class);
            intent.putExtra(SermonPresentation.PRESENTATION_FIRE_BASE_KEY, fireBaseKey);
            intent.putExtra(SermonPresentation.PRESENTATION_KEY, presentation);
            startActivity(intent);
        }

    }

    private void configureEmptyView() {
        if (mOfflineView != null && mEmptyView != null) {
            if (Utility.isOnline(getActivity())) {
                mOfflineView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
                mRecyclerView.setEmptyView(mEmptyView);
            } else {
                mOfflineView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
                mRecyclerView.setEmptyView(mOfflineView);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            configureEmptyView();
        }
    }

    public static class PresentationViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        private TextView mPresentationTitle;
        private TextView mPresentationContributor;
        private TextView mPresentationDate;
        //private TextView mPresentationSummary;
        private Editor mPresentationSummary;
        ImageView mMenu;
        ImageView mIcon;

        public PresentationViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.presentationCardView);
            mPresentationTitle = (TextView) itemView.findViewById(R.id.presentationTitle);
            mPresentationContributor = (TextView) itemView.findViewById(R.id.presentationContributor);
            mPresentationDate = (TextView) itemView.findViewById(R.id.presentationDate);
            mPresentationSummary = (Editor) itemView.findViewById(R.id.presentationSummary);
            mMenu = (ImageView) itemView.findViewById(R.id.presentationMenu);
            mIcon = (ImageView) itemView.findViewById(R.id.presentationIcon);

        }

        public void setTitle(String title) {
            mPresentationTitle.setText(Utility.toTitleCase(title));
        }

        public void setContributor(String contributorName) {
            mPresentationContributor.setText(contributorName);
        }

        public void setPresentationDate(String date) {
            mPresentationDate.setText(date);
        }

        public void setSummary(String summary) {
            mPresentationSummary.clearAllContents();
            mPresentationSummary.render(summary);
        }
    }
}
