package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.ActivityFeed;
import com.apwright.preachersclub.model.Contributor;
import com.bumptech.glide.Glide;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

/**
 * Created by apwright on 6/18/2017.
 */

public class ContributorEditFragment extends Fragment {

    private static final int RC_GET_IMAGE = 105;

    private ImageView mContributorPhoto;
    private TextInputLayout mNameLayout;
    private TextInputLayout mEmailLayout;
    private TextInputLayout mBioLayout;
    private TextInputEditText mName;
    private TextInputEditText mEmail;
    private TextInputEditText mBio;

    private Contributor mContributor;
    private Uri mSelectedImage;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private FirebaseStorage mStorage;
    private StorageReference mImageStorageReference;

    private String mFireBaseContributorKey;

    private boolean mEditMode = false;

    private Context mContext;
    private View mView;

    public ContributorEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contributor_edit, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        mContributor = new Contributor();
        mContext = getContext();
        mView = getView();

        mContributorPhoto = (ImageView) rootView.findViewById(R.id.contributor_edit_photo_image_view);
        mNameLayout = (TextInputLayout) rootView.findViewById(R.id.contributor_edit_name_text_input_layout);
        mEmailLayout = (TextInputLayout) rootView.findViewById(R.id.contributor_edit_email_text_input_layout);
        mBioLayout = (TextInputLayout) rootView.findViewById(R.id.contributor_edit_bio_text_input_layout);
        mName = (TextInputEditText) rootView.findViewById(R.id.contributor_edit_name_text_view);
        mEmail = (TextInputEditText) rootView.findViewById(R.id.contributor_edit_email_text_view);
        mBio = (TextInputEditText) rootView.findViewById(R.id.contributor_edit_bio_text_view);

        //open gallery chooser when photo icon is selected
        mContributorPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openGalleryChooser();
            }
        });

        return rootView;
    }

    private void openGalleryChooser() {
        if (Utility.askForPermission(mContext, Utility.PERMISSION_EXTERNAL_STORAGE, RC_GET_IMAGE, ContributorEditFragment.this)) {
            Intent galleryIntent = new Intent("android.intent.action.PICK");
            galleryIntent.setType("image/*");
            startActivityForResult(Intent.createChooser(galleryIntent, "Choose a Photo for Contributor"), RC_GET_IMAGE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_cancel, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean closeFragment = true;
        switch (item.getItemId()) {
            case R.id.menu_save:
                closeFragment = saveData();
                break;
        }
        if (closeFragment) {
            getActivity().finish();
        }

        return true;
    }

    private boolean saveData() {

        if (mUser == null) {
            Utility.showSnackBar(mView, "Sign in to add Contributor");
            return false;
        }

        if (mSelectedImage == null && mEditMode == false) {

            Snackbar.make(getView(), "Choose Image for your Contributor", Snackbar.LENGTH_LONG)
                    .setAction("Add Now", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openGalleryChooser();
                        }
                    })
                    .show();
            return false;
        }

        if (!validateFields()) {
            return false;
        }
        String name = Utility.toTitleCase(this.mName.getText().toString());
        String email = Utility.toTitleCase(this.mEmail.getText().toString());
        String bio = this.mBio.getText().toString();

        mContributor.setContributorName(name);
        mContributor.setContributorEmail(email);
        mContributor.setContributorShortBio(bio);
        mContributor.setDateCreated(System.currentTimeMillis());


        if (!mEditMode) {
            uploadPhotoImageAndSaveData();
        } else {
            mDatabase.getReference().child(Utility.REFERENCE_TUTORIALS).child(mFireBaseContributorKey).setValue(mContributor);
            Utility.showToast(getContext(), "Contributor " + name + " updated");
        }

        return true;
    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mNameLayout.setError(null);
        this.mEmailLayout.setError(null);
        this.mBioLayout.setError(null);

        if (this.mName.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mNameLayout.setError("Name is required");
        }
        if (this.mEmail.getText().toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(this.mEmail.getText().toString()).matches()) {
            fieldsValid = false;
            this.mEmailLayout.setError("A valid Email is required");
        }
        if (this.mBio.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mBioLayout.setError("Tell us about the contributor");
        }
        return fieldsValid;
    }

    private void uploadPhotoImageAndSaveData() {

        String fileName = mContributor.getFileNameForImage();

        mImageStorageReference = mStorage.getReference().child(Utility.REFERENCE_CONTRIBUTOR_IMAGES).child(fileName);

        //get image from ImageView
        mContributorPhoto.setDrawingCacheEnabled(true);
        mContributorPhoto.buildDrawingCache();
        Bitmap bitmap = mContributorPhoto.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        //upload image
        UploadTask uploadTask = mImageStorageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if (mView == null) {
                    Utility.showToast(mContext, "Contributor creation failed");
                } else {
                    Snackbar.make(mView, "Contributor creation failed", Snackbar.LENGTH_LONG)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uploadPhotoImageAndSaveData();
                                }
                            })
                            .show();
                }

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                @SuppressWarnings("VisibleForTests")
                Uri downloadUri = taskSnapshot.getDownloadUrl();

                mContributor.setContributorPhotoURL(downloadUri.toString());

                String fireBaseKey = (String) mDatabase.getReference().child(Utility.REFERENCE_CONTRIBUTOR).push().getKey();


                //add tutorial to activity feed
                ActivityFeed activityFeed = new ActivityFeed(ActivityFeed.FeedType.CONTRIBUTOR, mContributor);
                activityFeed.setDateCreated(System.currentTimeMillis());
                activityFeed.setContributorName(mUser.getDisplayName());
                activityFeed.setFireBaseKey(fireBaseKey);

                //save contributor
                mDatabase.getReference().child(Utility.REFERENCE_CONTRIBUTOR).child(fireBaseKey).setValue(mContributor);
                //add contributor update to news feed
                //mDatabase.getReference().child(Utility.REFERENCE_ACTIVITY_FEED).child(fireBaseKey).setValue(activityFeed);

                Utility.showToast(mContext, "Contributor added");

            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_GET_IMAGE) {
            if (resultCode == ResultCodes.OK) {
                //retrieve selected image
                Uri imageData = data.getData();
                mSelectedImage = imageData;
                Glide.with(mContext).load(mSelectedImage).centerCrop().into(mContributorPhoto);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int i;
        switch (requestCode) {
            case RC_GET_IMAGE:
                if (grantResults.length > 0) {
                    i = grantResults[0];
                    getActivity().getPackageManager();
                    if (i == 0) {
                        openGalleryChooser();
                        return;
                    }
                    return;
                }
                return;

            default:
                return;
        }

    }
}
