package com.apwright.preachersclub.fragments;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.firebase.ui.auth.ResultCodes;
import com.github.irshulx.Editor;
import com.github.irshulx.EditorListener;
import com.github.irshulx.models.EditorTextStyle;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

/**
 * Created by apwright on 9/22/2017.
 */

public class NoteEditFragment extends Fragment {

    TextInputLayout mNotesLayout;
    TextInputEditText mNotesEditText;
    Editor mEditor;

    public NoteEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_note_edit, container, false);

        mNotesLayout = (TextInputLayout) rootView.findViewById(R.id.note_text_layout);
        mNotesEditText = (TextInputEditText) rootView.findViewById(R.id.note_text);

        setHasOptionsMenu(true);

        //configure editor
        mEditor = (Editor) rootView.findViewById(R.id.editor);
        setUpEditor(rootView);
        boolean dataExists = false;

        if (getActivity().getIntent() != null) {
            if (getActivity().getIntent().hasExtra(Intent.EXTRA_TEXT)) {

                String notes = getActivity().getIntent().getStringExtra(Intent.EXTRA_TEXT);
                if (!notes.isEmpty()) {
                    //mNotesEditText.append(notes);
                    //load html content
                    dataExists = true;
                    mEditor.render();
                    mEditor.render(notes);
                }
            }
        }

        //render editor if no data is loaded
        if (!dataExists) {
            mEditor.render();
        }


        return rootView;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_cancel, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_save:

                mNotesLayout.setError(null);
                String notes = mNotesEditText.getText().toString().trim();

                notes = mEditor.getContentAsHTML();

                    if (notes.isEmpty()) {
                    mNotesLayout.setError("Please enter content before saving");
                } else {
                    //return updated text
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(Intent.EXTRA_TEXT, notes);
                    getActivity().setResult(ResultCodes.OK, resultIntent);
                    getActivity().finish();

                    Utility.hideKeyboard(getContext());
                }

                break;


        }

        return super.onOptionsItemSelected(item);
    }

    private void setUpEditor(View view) {
        view.findViewById(R.id.action_h1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.H1);
            }
        });

        view.findViewById(R.id.action_h2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.H2);
            }
        });

        view.findViewById(R.id.action_h3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.H3);
            }
        });

        view.findViewById(R.id.action_bold).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.BOLD);
            }
        });

        view.findViewById(R.id.action_Italic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.ITALIC);
            }
        });

        view.findViewById(R.id.action_indent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.INDENT);
            }
        });

        view.findViewById(R.id.action_outdent).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.updateTextStyle(EditorTextStyle.OUTDENT);
            }
        });

        view.findViewById(R.id.action_bulleted).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertList(false);
            }
        });

        view.findViewById(R.id.action_unordered_numbered).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertList(true);
            }
        });

        view.findViewById(R.id.action_hr).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertDivider();
            }
        });

        view.findViewById(R.id.action_insert_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.openImagePicker();
            }
        });

        view.findViewById(R.id.action_insert_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertLink();
            }
        });

        view.findViewById(R.id.action_map).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.insertMap();
            }
        });

        view.findViewById(R.id.action_erase).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mEditor.clearAllContents();
            }
        });
        //editor.dividerBackground=R.drawable.divider_background_dark;
        //editor.setFontFace(R.string.fontFamily__serif);
        Map<Integer, String> headingTypeface = getHeadingTypeface();
        Map<Integer, String> contentTypeface = getContentface();
        mEditor.setHeadingTypeface(headingTypeface);
        mEditor.setContentTypeface(contentTypeface);
        mEditor.setDividerLayout(R.layout.tmpl_divider_layout);
        mEditor.setEditorImageLayout(R.layout.tmpl_image_view);
        mEditor.setListItemLayout(R.layout.tmpl_list_item);
        //editor.StartEditor();
        mEditor.setEditorListener(new EditorListener() {
            @Override
            public void onTextChanged(EditText editText, Editable text) {
                // Toast.makeText(EditorTestActivity.this, text, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUpload(Bitmap image, String uuid) {
                Toast.makeText(getContext(), uuid, Toast.LENGTH_LONG).show();
                mEditor.onImageUploadComplete("http://www.videogamesblogger.com/wp-content/uploads/2015/08/metal-gear-solid-5-the-phantom-pain-cheats-640x325.jpg", uuid);
                // editor.onImageUploadFailed(uuid);
            }
        });

        //mEditor.render();  // this method must be called to start the editor

//        view.findViewById(R.id.btnRender).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                /*
//                Retrieve the content as serialized, you could also say getContentAsHTML();
//                */
//                String text = mEditor.getContentAsSerialized();
//                Intent intent = new Intent(getContext(), RenderTestActivity.class);
//                intent.putExtra("content", text);
//                startActivity(intent);
//            }
//        });

        //hide controls
        view.findViewById(R.id.action_insert_image).setVisibility(View.GONE);
        view.findViewById(R.id.action_insert_link).setVisibility(View.GONE);
        view.findViewById(R.id.action_map).setVisibility(View.GONE);
    }

    public Map<Integer, String> getHeadingTypeface() {
        Map<Integer, String> typefaceMap = new HashMap<>();
        typefaceMap.put(Typeface.NORMAL, "fonts/GreycliffCF-Bold.ttf");
        typefaceMap.put(Typeface.BOLD, "fonts/GreycliffCF-Heavy.ttf");
        typefaceMap.put(Typeface.ITALIC, "fonts/GreycliffCF-Heavy.ttf");
        typefaceMap.put(Typeface.BOLD_ITALIC, "fonts/GreycliffCF-Bold.ttf");
        return typefaceMap;
    }

    public Map<Integer, String> getContentface() {
        Map<Integer, String> typefaceMap = new HashMap<>();
        typefaceMap.put(Typeface.NORMAL, "fonts/Lato-Medium.ttf");
        typefaceMap.put(Typeface.BOLD, "fonts/Lato-Bold.ttf");
        typefaceMap.put(Typeface.ITALIC, "fonts/Lato-MediumItalic.ttf");
        typefaceMap.put(Typeface.BOLD_ITALIC, "fonts/Lato-BoldItalic.ttf");
        return typefaceMap;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == mEditor.PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                mEditor.insertImage(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (resultCode == Activity.RESULT_CANCELED) {
            // editor.RestoreState();
        } else if (requestCode == mEditor.MAP_MARKER_REQUEST) {
            mEditor.insertMap(data.getStringExtra("cords"));
        }
    }


}
