package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.apwright.preachersclub.AssignmentEditActivity;
import com.apwright.preachersclub.AssignmentReadActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Assignment;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

/**
 * Created by apwright on 6/22/2017.
 */

public class AssignmentReadFragment extends Fragment {

    private ImageView mAssignmentPhoto;
    private TextView mName;
    private TextView mDueDate;
    private TextView mDetails;


    private ImageButton mLoveImageButton;
    private ImageButton mShareImageButton;
    private ImageButton mChatImageButton;
    private ImageButton mNotesImageButton;
    private ImageButton mEditImageButton;

    private Assignment mAssignment;

    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private AssignmentReadActivity mActivity;

    private String mFireBaseKey;

    private RoundedImageView mContributorPhoto;
    private TextView mContributor;
    private TextView mContributorBio;


    public AssignmentReadFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_assignment_read, container, false);

        mActivity = (AssignmentReadActivity) getActivity();
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsingToolbar);

        mActivity.setSupportActionBar(mToolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLoveImageButton = (ImageButton) rootView.findViewById(R.id.assignment_read_love_image_button);
        mEditImageButton = (ImageButton) rootView.findViewById(R.id.assignment_read_edit_image_button);
        mChatImageButton = (ImageButton) rootView.findViewById(R.id.assignment_read_chat_image_button);
        mNotesImageButton = (ImageButton) rootView.findViewById(R.id.assignment_read_note_image_button);
        mShareImageButton = (ImageButton) rootView.findViewById(R.id.assignment_read_share_image_button);

        mAssignmentPhoto = (ImageView) rootView.findViewById(R.id.assignment_read_photo_image_view);
        mName = (TextView) rootView.findViewById(R.id.assignment_read_name_text_view);
        mDueDate = (TextView) rootView.findViewById(R.id.assignment_read_due_date_text_view);
        mDetails = (TextView) rootView.findViewById(R.id.assignment_read_description_text_view);

        mContributorPhoto = (RoundedImageView) rootView.findViewById(R.id.about_contributor_photo_rounded_image_view);
        mContributor = (TextView) rootView.findViewById(R.id.about_contributor_name_text_view);
        mContributorBio = (TextView) rootView.findViewById(R.id.about_contributor_bio_text_view);

        if (getActivity().getIntent().hasExtra(Assignment.ASSIGNMENT_KEY)) {
            mAssignment = (Assignment) getActivity().getIntent().getExtras().getParcelable(Assignment.ASSIGNMENT_KEY);
        }

        if (getActivity().getIntent().hasExtra(Assignment.ASSIGNMENT_FIRE_BASE_KEY)) {
            mFireBaseKey = (String) getActivity().getIntent().getStringExtra(Assignment.ASSIGNMENT_FIRE_BASE_KEY);
        }

        if (mAssignment != null) {
            loadAssignmentContent();
        }

        mShareImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareAssignmentContent();
            }
        });

        mEditImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editAssignmentContent();
            }
        });

        return rootView;
    }

    private void shareAssignmentContent() {
        if (mAssignment != null) {

            Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                    .setEmailTo(new String[]{})
                    .setType("text/plain")
                    .setHtmlText(mAssignment.getShareContent(0))
                    .setText(mAssignment.getShareContent(300))
                    .setSubject(mAssignment.getShareContentSubject())
                    .getIntent();

            if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_assignment)));
            }
        }
    }

    private void editAssignmentContent() {

        if (mAssignment != null) {
            Intent editAssignmentIntent = new Intent(getActivity(), AssignmentEditActivity.class);
            editAssignmentIntent.putExtra(Assignment.ASSIGNMENT_KEY, mAssignment);
            editAssignmentIntent.putExtra(Assignment.ASSIGNMENT_FIRE_BASE_KEY, mFireBaseKey);
            startActivity(editAssignmentIntent);
        }

    }

    private void loadAssignmentContent() {

        mName.setText(mAssignment.getAssignmentName());
        mDueDate.setText(mAssignment.getFormattedDueDate() + " - " + mAssignment.getDueDateAsString());
        mDetails.setText(mAssignment.getAssignmentDescription());

        //update contributor stub
        Utility.getContributorByEmail(getContext(),  mAssignment.getContributorEmail(), mContributor, mContributorBio, mContributorPhoto);

        mCollapsingToolbarLayout.setTitle(mAssignment.getAssignmentName());
        mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        Glide.with(this.mAssignmentPhoto.getContext())
                .load(mAssignment.getDefaultImageURL())
                .centerCrop()
                .into(mAssignmentPhoto);

    }
}
