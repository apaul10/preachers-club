package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.SermonEditActivity;
import com.apwright.preachersclub.SermonIllustrationEditActivity;
import com.apwright.preachersclub.SermonReadActivity;
import com.apwright.preachersclub.SermonRelatedMaterialEditActivity;
import com.apwright.preachersclub.SermonSupportingPointEditActivity;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Sermon;
import com.firebase.ui.auth.ResultCodes;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.makeramen.roundedimageview.RoundedImageView;

/**
 * Created by apwright on 8/11/2017.
 */

public class SermonReadFragment extends Fragment {

    private TextView mSermonTopic;
    private TextView mKeyText;
    private TextView mSubject;
    private TextView mComplement;
    private TextView mIdea;
    private TextView mTakeaway;

    private ImageButton mLoveImageButton;
    private ImageButton mShareImageButton;
    private ImageButton mEditImageButton;

    private Sermon mSermon;

    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private SermonReadActivity mActivity;

    private String mFireBaseKey;

    private RoundedImageView mContributorPhoto;
    private TextView mContributor;
    private TextView mContributorBio;

    private RecyclerView mSupportingPointRecyclerView;
    private RecyclerView mIllustrationRecyclerView;
    private RecyclerView mRelatedMaterialRecyclerView;

    private FirebaseRecyclerAdapter mSupportingPointRecyclerAdapter;
    private FirebaseRecyclerAdapter mIllustrationRecyclerAdapter;
    private FirebaseRecyclerAdapter mRelatedMaterialRecyclerAdapter;

    private FirebaseDatabase mDatabase;
    private Query mSupportingPointQuery;
    private Query mIllustrationQuery;
    private Query mRelatedMaterialQuery;


    private Button mAddSupportingPointButton;
    private Button mAddIllustrationButton;
    private Button mAddRelatedMaterialButton;
    private int mPosition;

    public static int RC_SUPPORTING_POINT = 486;
    public static int RC_ILLUSTRATION = 487;
    public static int RC_RELATED_MATERIAL = 488;

    private int lengthOfTextToDisplay = 100;

    public SermonReadFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sermon_read, container, false);

        mActivity = (SermonReadActivity) getActivity();
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsingToolbar);

        mActivity.setSupportActionBar(mToolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mDatabase = FirebaseDatabase.getInstance();

        mLoveImageButton = (ImageButton) rootView.findViewById(R.id.sermon_read_love_image_button);
        mEditImageButton = (ImageButton) rootView.findViewById(R.id.sermon_read_edit_image_button);
        mShareImageButton = (ImageButton) rootView.findViewById(R.id.sermon_read_share_image_button);

        mSermonTopic = (TextView) rootView.findViewById(R.id.sermon_read_topic_text_view);
        mKeyText = (TextView) rootView.findViewById(R.id.sermon_read_key_text_text_view);
        mSubject = (TextView) rootView.findViewById(R.id.sermon_read_subject_text_view);
        mComplement = (TextView) rootView.findViewById(R.id.sermon_read_complement_text_view);
        mIdea = (TextView) rootView.findViewById(R.id.sermon_read_idea_text_view);
        mTakeaway = (TextView) rootView.findViewById(R.id.sermon_read_takeaway_text_view);

        mContributorPhoto = (RoundedImageView) rootView.findViewById(R.id.about_contributor_photo_rounded_image_view);
        mContributor = (TextView) rootView.findViewById(R.id.about_contributor_name_text_view);
        mContributorBio = (TextView) rootView.findViewById(R.id.about_contributor_bio_text_view);

        mSupportingPointRecyclerView = (RecyclerView) rootView.findViewById(R.id.sermon_read_supporting_point_recycler_view);
        mIllustrationRecyclerView = (RecyclerView) rootView.findViewById(R.id.sermon_read_illustrations_recycler_view);
        mRelatedMaterialRecyclerView = (RecyclerView) rootView.findViewById(R.id.sermon_read_related_material_recycler_view);

        mAddSupportingPointButton = (Button) rootView.findViewById(R.id.add_supporting_point_button);
        mAddIllustrationButton = (Button) rootView.findViewById(R.id.add_illustration_button);
        mAddRelatedMaterialButton = (Button) rootView.findViewById(R.id.add_related_material_button);

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_KEY)) {
            mSermon = (Sermon) getActivity().getIntent().getExtras().getParcelable(Sermon.SERMON_KEY);
        }

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_FIRE_BASE_KEY)) {
            mFireBaseKey = (String) getActivity().getIntent().getStringExtra(Sermon.SERMON_FIRE_BASE_KEY);
        }

        if (mSermon != null) {
            loadSermonContent();
        }

        mKeyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openKeyText();
            }
        });

        mShareImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareSermonContent();
            }
        });

        mEditImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editSermonContent();
            }
        });

        mLoveImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loveSermonContent();
            }
        });

        mAddSupportingPointButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddSupportingPoint(false);
            }
        });

        mAddIllustrationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddIllustration(false);
            }
        });

        mAddRelatedMaterialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddRelatedMaterial(false);
            }
        });

        return rootView;
    }

    private void openKeyText() {
        if (mKeyText.getText() == null) return;

        String verse = mKeyText.getText().toString();

        //Toast.makeText(getContext(), verse, Toast.LENGTH_SHORT).show();

        if (!TextUtils.isEmpty(verse)) {
            Utility.openBibleVerse(getContext(), verse);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (mFireBaseKey != null) {
            loadSupportingPoints();
            loadIllustrations();
            loadRelatedMaterials();
        }
    }

    private void loadRelatedMaterials() {
        mRelatedMaterialQuery = mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).child("referenceMaterialList");
        mRelatedMaterialRecyclerAdapter = new FirebaseRecyclerAdapter<Sermon.ReferenceMaterial, SermonPillarViewHolder>(
                Sermon.ReferenceMaterial.class,
                R.layout.item_sermon_pillar,
                SermonPillarViewHolder.class,
                mRelatedMaterialQuery
        ) {
            @Override
            protected void populateViewHolder(SermonPillarViewHolder viewHolder, final Sermon.ReferenceMaterial model, final int position) {
                viewHolder.setPillarDescription(model.getTitle());
                viewHolder.mEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPosition = position;
                        openAddRelatedMaterial(true);
                    }
                });
                viewHolder.mDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String content = model.getTitle();
                        MaterialDialog materialDialog = new MaterialDialog.Builder(getContext())
                                .title("Remove Related Material?")
                                .content("Do you wish to delete this related material\n\n" + content.substring(0, Math.min(content.length(), lengthOfTextToDisplay)) + "...")
                                .positiveText("Agree")
                                .negativeText("Disagree")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        //remove data from fireBase
                                        mSermon.getReferenceMaterialList().remove(position);
                                        mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).setValue(mSermon);
                                        //refresh adapter
                                        mRelatedMaterialRecyclerAdapter.notifyDataSetChanged();

                                    }
                                }).build();
                        materialDialog.show();
                    }
                });
            }
        };
        mRelatedMaterialRecyclerView.setHasFixedSize(true);
        mRelatedMaterialRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRelatedMaterialRecyclerView.setAdapter(mRelatedMaterialRecyclerAdapter);
    }

    private void loadIllustrations() {
        mIllustrationQuery = mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).child("illustrationList");
        mIllustrationRecyclerAdapter = new FirebaseRecyclerAdapter<Sermon.Illustration, SermonPillarViewHolder>(
                Sermon.Illustration.class,
                R.layout.item_sermon_pillar,
                SermonPillarViewHolder.class,
                mIllustrationQuery
        ) {
            @Override
            protected void populateViewHolder(SermonPillarViewHolder viewHolder, final Sermon.Illustration model, final int position) {
                viewHolder.setPillarDescription(model.getTitle());
                viewHolder.mEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPosition = position;
                        openAddIllustration(true);
                    }
                });
                viewHolder.mDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String content = model.getTitle();
                        MaterialDialog materialDialog = new MaterialDialog.Builder(getContext())
                                .title("Remove Sermon Illustration?")
                                .content("Do you wish to delete this illustration\n\n" + content.substring(0, Math.min(content.length(), lengthOfTextToDisplay)) + "...")
                                .positiveText("Agree")
                                .negativeText("Disagree")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        //remove data from fireBase
                                        mSermon.getIllustrationList().remove(position);
                                        mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).setValue(mSermon);
                                        //refresh adapter
                                        mIllustrationRecyclerAdapter.notifyDataSetChanged();

                                    }
                                }).build();
                        materialDialog.show();
                    }
                });
            }
        };
        mIllustrationRecyclerView.setHasFixedSize(true);
        mIllustrationRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mIllustrationRecyclerView.setAdapter(mIllustrationRecyclerAdapter);
    }

    private void loadSupportingPoints() {
        mSupportingPointQuery = mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).child("supportingPointList").orderByKey();
        mSupportingPointRecyclerAdapter = new FirebaseRecyclerAdapter<Sermon.SupportingPoint, SermonPillarViewHolder>(
                Sermon.SupportingPoint.class,
                R.layout.item_sermon_pillar,
                SermonPillarViewHolder.class,
                mSupportingPointQuery
        ) {
            @Override
            protected void populateViewHolder(SermonPillarViewHolder viewHolder, final Sermon.SupportingPoint model, final int position) {
                viewHolder.setPillarDescription(model.getDescription());
                viewHolder.mEdit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPosition = position;
                        openAddSupportingPoint(true);
                    }
                });
                viewHolder.mDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String content = model.getDescription();
                        MaterialDialog materialDialog = new MaterialDialog.Builder(getContext())
                                .title("Remove Sermon Pillar?")
                                .content("Do you wish to delete this supporting point\n\n" + content.substring(0, Math.min(content.length(), lengthOfTextToDisplay)) + "...")
                                .positiveText("Agree")
                                .negativeText("Disagree")
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                        //remove data from fireBase
                                        mSermon.getSupportingPointList().remove(position);
                                        mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).setValue(mSermon);
                                        //refresh adapter
                                        mSupportingPointRecyclerAdapter.notifyDataSetChanged();

                                    }
                                }).build();
                        materialDialog.show();
                    }
                });
            }
        };
        mSupportingPointRecyclerView.setHasFixedSize(true);
        mSupportingPointRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mSupportingPointRecyclerView.setAdapter(mSupportingPointRecyclerAdapter);
    }

    private void openAddRelatedMaterial(boolean editMode) {
        if (mSermon != null) {
            Intent intent = new Intent(getActivity(), SermonRelatedMaterialEditActivity.class);
            intent.putExtra(Sermon.SERMON_KEY, mSermon);
            intent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseKey);
            if (editMode) {
                intent.putExtra("EditMode", mPosition);
            }
            startActivityForResult(intent, RC_RELATED_MATERIAL);
        }
    }

    private void openAddIllustration(boolean editMode) {
        if (mSermon != null) {
            Intent intent = new Intent(getActivity(), SermonIllustrationEditActivity.class);
            intent.putExtra(Sermon.SERMON_KEY, mSermon);
            intent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseKey);
            if (editMode) {
                intent.putExtra("EditMode", mPosition);
            }
            startActivityForResult(intent, RC_ILLUSTRATION);
        }
    }

    private void openAddSupportingPoint(boolean editMode) {
        if (mSermon != null) {
            Intent intent = new Intent(getActivity(), SermonSupportingPointEditActivity.class);
            intent.putExtra(Sermon.SERMON_KEY, mSermon);
            intent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseKey);
            if (editMode) {
                intent.putExtra("EditMode", mPosition);
            }
            startActivityForResult(intent, RC_SUPPORTING_POINT);
        }
    }

    private void loveSermonContent() {

    }

    private void editSermonContent() {
        if (mSermon != null) {
            Intent intent = new Intent(getActivity(), SermonEditActivity.class);
            intent.putExtra(Sermon.SERMON_KEY, mSermon);
            intent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseKey);
            startActivity(intent);
        }
    }

    private void shareSermonContent() {
        if (mSermon != null) {

            Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                    .setEmailTo(new String[]{})
                    .setType("text/plain")
                    .setHtmlText(mSermon.getShareContent())
                    .setText(mSermon.getShareContent())
                    .setSubject(mSermon.getShareContentSubject())
                    .getIntent();

            if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_sermon)));
            }
        }
    }

    private void loadSermonContent() {
        if (mSermon != null) {
            mSermonTopic.setText(mSermon.getSermonTitle());
            mKeyText.setText(mSermon.getSermonKeyText());
            mSubject.setText(mSermon.getSermonSubject());
            mComplement.setText(mSermon.getSermonComplement());
            mIdea.setText(mSermon.getSermonIdea());
            mTakeaway.setText(mSermon.getSermonTakeaway());

            //update contributor stub
            Utility.getContributorByID(getContext(), mSermon.getContributorId(), mContributor, mContributorBio, mContributorPhoto);

            mCollapsingToolbarLayout.setTitle(mSermon.getSermonTitle());
            mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        }
    }

    public static class SermonPillarViewHolder extends RecyclerView.ViewHolder {
        TextView mPillar;
        public ImageButton mEdit;
        public ImageButton mDelete;

        public SermonPillarViewHolder(View itemView) {
            super(itemView);
            mPillar = (TextView) itemView.findViewById(R.id.sermon_pillar_text_view);
            mEdit = (ImageButton) itemView.findViewById(R.id.sermon_pillar_edit_button);
            mDelete = (ImageButton) itemView.findViewById(R.id.sermon_pillar_delete_button);
        }

        public void setPillarDescription(String description) {
            mPillar.setText(description);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == ResultCodes.OK && requestCode == RC_SUPPORTING_POINT) {

            Bundle bundle = data.getExtras();
            Sermon sermon = bundle.getParcelable(Sermon.SERMON_KEY);
            String firebaseKey = bundle.getString(Sermon.SERMON_FIRE_BASE_KEY);

            //retrieve sermon class
            if (sermon != null) {
                mSermon = sermon;
            }

            //retrieve firebase key
            if (firebaseKey != null) {
                mFireBaseKey = firebaseKey;
            }

            mSupportingPointRecyclerAdapter.notifyDataSetChanged();
        }

        if (resultCode == ResultCodes.OK && requestCode == RC_ILLUSTRATION) {

            Bundle bundle = data.getExtras();
            Sermon sermon = bundle.getParcelable(Sermon.SERMON_KEY);
            String firebaseKey = bundle.getString(Sermon.SERMON_FIRE_BASE_KEY);

            //retrieve sermon class
            if (sermon != null) {
                mSermon = sermon;
            }

            //retrieve firebase key
            if (firebaseKey != null) {
                mFireBaseKey = firebaseKey;
            }

            mIllustrationRecyclerAdapter.notifyDataSetChanged();
        }

        if (resultCode == ResultCodes.OK && requestCode == RC_RELATED_MATERIAL) {

            Bundle bundle = data.getExtras();
            Sermon sermon = bundle.getParcelable(Sermon.SERMON_KEY);
            String firebaseKey = bundle.getString(Sermon.SERMON_FIRE_BASE_KEY);

            //retrieve sermon class
            if (sermon != null) {
                mSermon = sermon;
            }

            //retrieve firebase key
            if (firebaseKey != null) {
                mFireBaseKey = firebaseKey;
            }

            mRelatedMaterialRecyclerAdapter.notifyDataSetChanged();
        }


    }
}
