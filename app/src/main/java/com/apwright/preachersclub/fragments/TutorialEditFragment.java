package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apwright.preachersclub.PixaBayPhotoChooserActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.ActivityFeed;
import com.apwright.preachersclub.model.ImageContent;
import com.apwright.preachersclub.model.Tutorial;
import com.bumptech.glide.Glide;
import com.firebase.ui.auth.ResultCodes;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;


/**
 * Created by apwright on 6/2/2017.
 */

public class TutorialEditFragment extends Fragment {

    private static int RC_PHOTO_CHOOSER = 401;

    private ImageView mTutorialPhoto;
    private TextInputLayout mTitleLayout;
    private TextInputLayout mSubTitleLayout;
    private TextInputLayout mDetailsLayout;
    private TextInputEditText mTitle;
    private TextInputEditText mSubTitle;
    private TextInputEditText mDetails;

    private Tutorial mTutorial;
    private ImageContent mSelectedImage;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private FirebaseStorage mStorage;
    private StorageReference mImageStorageReference;

    private String mFireBaseTutorialKey;

    private boolean mEditMode = false;

    private Context mContext;
    private View mView;

    public TutorialEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial_edit, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        mTutorial = new Tutorial();
        mContext = getContext();
        mView = getView();

        mTutorialPhoto = (ImageView) rootView.findViewById(R.id.tutorial_edit_photo_image_view);
        mTitleLayout = (TextInputLayout) rootView.findViewById(R.id.tutorial_edit_title_text_input_layout);
        mSubTitleLayout = (TextInputLayout) rootView.findViewById(R.id.tutorial_edit_subtitle_text_input_layout);
        mDetailsLayout = (TextInputLayout) rootView.findViewById(R.id.tutorial_edit_details_text_input_layout);
        mTitle = (TextInputEditText) rootView.findViewById(R.id.tutorial_edit_title_text_view);
        mSubTitle = (TextInputEditText) rootView.findViewById(R.id.tutorial_edit_subtitle_text_view);
        mDetails = (TextInputEditText) rootView.findViewById(R.id.tutorial_edit_details_text_view);

        //open photo chooser when photo icon is selected
        mTutorialPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPhotoChooser();
            }
        });

        //check if edit mode is applicable
        if (getActivity().getIntent().hasExtra(Tutorial.TUTORIAL_KEY) && getActivity().getIntent().hasExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY)) {

            Tutorial tutorial = (Tutorial) getActivity().getIntent().getExtras().getParcelable(Tutorial.TUTORIAL_KEY);
            if (tutorial != null) {
                mTutorial = tutorial;
                mEditMode = true;
                mFireBaseTutorialKey = (String) getActivity().getIntent().getStringExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY);

                mTitle.setText(mTutorial.getTitle());
                mSubTitle.setText(mTutorial.getSubTitle());
                mDetails.setText(mTutorial.getDetails());

                loadPhotoImage(mTutorial.getPhotoURL());
            }
        }


        return rootView;
    }

    private void openPhotoChooser() {
        Intent intent = new Intent(getActivity(), PixaBayPhotoChooserActivity.class);
        startActivityForResult(intent, RC_PHOTO_CHOOSER);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_cancel, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean closeFragment = true;
        switch (item.getItemId()) {
            case R.id.menu_save:
                closeFragment = saveData();
                break;
        }
        if (closeFragment) {
            //getActivity().getSupportFragmentManager().popBackStack();
            getActivity().finish();
        }

        return true;
    }

    private boolean saveData() {

        if (mUser == null) {
            Utility.showSnackBar(getView(), "Sign in to add tutorial");
            return false;
        }

        if (mSelectedImage == null && mEditMode == false) {

            Snackbar.make(getView(), "Choose Image for your Tutorial", Snackbar.LENGTH_LONG)
                    .setAction("Add Now", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            openPhotoChooser();
                        }
                    })
                    .show();
            return false;
        }

        if (!validateFields()) {
            return false;
        }
        String title = Utility.toTitleCase(this.mTitle.getText().toString());
        String subtitle = Utility.toTitleCase(this.mSubTitle.getText().toString());
        String details = this.mDetails.getText().toString();
        String contributorId = mUser.getUid();
        String contributorName = mUser.getDisplayName();
        String photoURL = "";

        mTutorial.setTitle(title);
        mTutorial.setSubTitle(subtitle);
        mTutorial.setDetails(details);
        mTutorial.setContributorId(contributorId);
        mTutorial.setContributorName(contributorName);
        mTutorial.setCreationDate(System.currentTimeMillis());


        if (!mEditMode) {
            uploadPhotoImageAndSaveData();
        } else {
            mDatabase.getReference().child(Utility.REFERENCE_TUTORIALS).child(mFireBaseTutorialKey).setValue(mTutorial);
            mDatabase.getReference().child(Utility.REFERENCE_ACTIVITY_FEED).child(mFireBaseTutorialKey).child("feedContent").setValue(mTutorial);
            Utility.showToast(getContext(), "Tutorial " + mTutorial.getTitle() + " updated");
        }

        return true;
    }


    private void uploadPhotoImageAndSaveData() {

        mImageStorageReference = mStorage.getReference().child(Utility.REFERENCE_TUTORIAL_IMAGES).child(mSelectedImage.getImageName());

        //get image from ImageView
        mTutorialPhoto.setDrawingCacheEnabled(true);
        mTutorialPhoto.buildDrawingCache();
        Bitmap bitmap = mTutorialPhoto.getDrawingCache();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] data = baos.toByteArray();

        //upload image
        UploadTask uploadTask = mImageStorageReference.putBytes(data);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                if (getView() == null) {

                    Utility.showToast(mContext, "Tutorial creation failed");

                } else {

                    Snackbar.make(getView(), "Tutorial creation failed", Snackbar.LENGTH_LONG)
                            .setAction("Retry", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    uploadPhotoImageAndSaveData();
                                }
                            })
                            .show();
                }

            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                @SuppressWarnings("VisibleForTests")
                Uri downloadUri = taskSnapshot.getDownloadUrl();

                mTutorial.setPhotoURL(downloadUri.toString());

                String fireBaseKey = (String) mDatabase.getReference().child(Utility.REFERENCE_TUTORIALS).push().getKey();


                //add tutorial to activity feed
                ActivityFeed activityFeed = new ActivityFeed(ActivityFeed.FeedType.TUTORIAL, mTutorial);
                activityFeed.setDateCreated(System.currentTimeMillis());
                activityFeed.setContributorName(mUser.getDisplayName());
                activityFeed.setFireBaseKey(fireBaseKey);

                mDatabase.getReference().child(Utility.REFERENCE_TUTORIALS).child(fireBaseKey).setValue(mTutorial);
                mDatabase.getReference().child(Utility.REFERENCE_ACTIVITY_FEED).child(fireBaseKey).setValue(activityFeed);

                Utility.showToast(mContext, "Tutorial created: " + mTutorial.getTitle());

            }
        });


    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mTitleLayout.setError(null);
        this.mSubTitleLayout.setError(null);
        this.mDetailsLayout.setError(null);

        if (this.mTitle.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mTitleLayout.setError("Title is required");
        }
        if (this.mSubTitle.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mSubTitleLayout.setError("Subtitle is required");
        }
        if (this.mDetails.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mDetailsLayout.setError("Tutorial content is required");
        }
        return fieldsValid;
    }

    private void loadPhotoImage(String url) {
        Glide.with(getContext()).load(url).into(mTutorialPhoto);
        mTutorialPhoto.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_PHOTO_CHOOSER) {
            if (resultCode == ResultCodes.OK) {
                //retrieve selected image
                Bundle bundle = data.getExtras();

                mSelectedImage = new ImageContent();
                mSelectedImage = (ImageContent) bundle.get(ImageContent.IMAGE_CONTENT_KEY);

                if (mSelectedImage != null) {
                    loadPhotoImage(mSelectedImage.getPreviewURL());
                }
            }
        }
    }
}
