package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.TutorialEditActivity;
import com.apwright.preachersclub.TutorialReadActivity;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Tutorial;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

/**
 * Created by apwright on 6/8/2017.
 */

public class TutorialReadFragment extends Fragment {

    private ImageView mTutorialPhoto;
    private TextView mTitle;
    private TextView mSubTitle;
    private TextView mDetails;

    private ImageButton mLoveImageButton;
    private ImageButton mShareImageButton;
    private ImageButton mEditImageButton;

    private Tutorial mTutorial;

    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private TutorialReadActivity mActivity;

    private String mTutorialItemFireBaseKey;

    private RoundedImageView mContributorPhoto;
    private TextView mContributor;
    private TextView mContributorBio;


    public TutorialReadFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial_read, container, false);

        mActivity = (TutorialReadActivity) getActivity();
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsingToolbar);

        mActivity.setSupportActionBar(mToolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLoveImageButton = (ImageButton) rootView.findViewById(R.id.tutorial_read_love_image_button);
        mEditImageButton = (ImageButton) rootView.findViewById(R.id.tutorial_read_edit_image_button);
        mShareImageButton = (ImageButton) rootView.findViewById(R.id.tutorial_read_share_image_button);

        mTutorialPhoto = (ImageView) rootView.findViewById(R.id.tutorial_read_photo_image_view);
        mTitle = (TextView) rootView.findViewById(R.id.tutorial_read_title_text_view);
        mSubTitle = (TextView) rootView.findViewById(R.id.tutorial_read_sub_title_text_view);
        mDetails = (TextView) rootView.findViewById(R.id.tutorial_read_description_text_view);

        mContributorPhoto = (RoundedImageView) rootView.findViewById(R.id.about_contributor_photo_rounded_image_view);
        mContributor = (TextView) rootView.findViewById(R.id.about_contributor_name_text_view);
        mContributorBio = (TextView) rootView.findViewById(R.id.about_contributor_bio_text_view);

        if(getActivity().getIntent().hasExtra(Tutorial.TUTORIAL_KEY)){
            mTutorial = (Tutorial) getActivity().getIntent().getExtras().getParcelable(Tutorial.TUTORIAL_KEY);
        }

        if(getActivity().getIntent().hasExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY)){
            mTutorialItemFireBaseKey = (String) getActivity().getIntent().getStringExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY);
        }

        if (mTutorial != null) {
            loadTutorialContent();
        }

        mShareImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareTutorialContent();
            }
        });

        mEditImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTutorialContent();
            }
        });

        mLoveImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loveTutorialContent();
            }
        });

        return rootView;
    }

    private void loveTutorialContent() {
        if (mTutorial != null) {
            if (mLoveImageButton.getTag() != null) {

                if (mLoveImageButton.getTag().toString() == "love_yes") {
                    //user loves tutorial
                    mLoveImageButton.setImageResource(R.drawable.ic_love);
                    mLoveImageButton.setTag("love_no");
                } else {
                    //user does not love tutorial
                    mLoveImageButton.setImageResource(R.drawable.ic_love_full);
                    mLoveImageButton.setTag("love_yes");
                }
            }
        }
    }

    private void editTutorialContent() {
        if (mTutorial != null) {

            Intent editTutorialIntent = new Intent(getActivity(), TutorialEditActivity.class);
            editTutorialIntent.putExtra(Tutorial.TUTORIAL_KEY, mTutorial);
            editTutorialIntent.putExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY, mTutorialItemFireBaseKey);
            startActivity(editTutorialIntent);

        }
    }

    private void shareTutorialContent() {
        if (mTutorial != null) {

            Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                    .setEmailTo(new String[]{})
                    .setType("text/plain")
                    .setHtmlText(mTutorial.getShareContent(0))
                    .setText(mTutorial.getShareContent(300))
                    .setSubject(mTutorial.getShareContentSubject())
                    .getIntent();

            if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_tutorial)));
            }
        }
    }

    private void loadTutorialContent() {
        mTitle.setText(mTutorial.getTitle());
        mSubTitle.setText(mTutorial.getSubTitle());
        mDetails.setText(mTutorial.getDetails());

        //update contributor stub
        Utility.getContributorByID(getContext(),  mTutorial.getContributorId(), mContributor, mContributorBio, mContributorPhoto);

        mCollapsingToolbarLayout.setTitle(mTutorial.getTitle());
        mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        Glide.with(this.mTutorialPhoto.getContext())
                .load(mTutorial.getPhotoURL())
                .centerCrop()
                .into(mTutorialPhoto);
    }
}
