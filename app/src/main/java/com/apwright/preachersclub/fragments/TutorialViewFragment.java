package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.TutorialEditActivity;
import com.apwright.preachersclub.TutorialReadActivity;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.EmptyRecyclerView;
import com.apwright.preachersclub.model.Tutorial;
import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;


/**
 * Created by apwright on 6/7/2017.
 */

public class TutorialViewFragment extends Fragment {
    private Context mContext;
    private EmptyRecyclerView mRecyclerView;

    View mEmptyView;
    View mOfflineView;

    public static final String TUTORIAL_KEY = "tutorial_data";
    private FirebaseDatabase mDatabase;
    private FirebaseRecyclerAdapter mFirebaseAdapter;
    private Query mTutorialQuery;

    public TutorialViewFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial_view, container, false);

        mDatabase = FirebaseDatabase.getInstance();
        mTutorialQuery = mDatabase.getReference()
                .child(Utility.REFERENCE_TUTORIALS)
                .orderByChild("lastModifiedDateReversed");

        mRecyclerView = (EmptyRecyclerView) rootView.findViewById(R.id.tutorials_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        mEmptyView = rootView.findViewById(R.id.empty_view_tutorial);
        mOfflineView = rootView.findViewById(R.id.empty_view_offline);

        configureEmptyView();

        mFirebaseAdapter = new FirebaseRecyclerAdapter<Tutorial, TutorialsViewHolder>(
                Tutorial.class,
                R.layout.item_tutorial,
                TutorialsViewHolder.class,
                mTutorialQuery
        ) {
            @Override
            protected void populateViewHolder(TutorialsViewHolder viewHolder, final Tutorial model, int position) {

                final String fireBaseKey = getRef(position).getKey();

                viewHolder.setTitle(model.getTitle());
                viewHolder.setSubTitle(model.getSubTitle());
                viewHolder.setImage(model.getPhotoURL());

                viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), TutorialReadActivity.class);
                        intent.putExtra(Tutorial.TUTORIAL_KEY, model);
                        intent.putExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY, fireBaseKey);
                        startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), v, "tutorialImage").toBundle());
                    }
                });
            }
        };

        mRecyclerView.setAdapter(mFirebaseAdapter);


        return rootView;
    }

    private void configureEmptyView() {
        if (mOfflineView != null && mEmptyView != null) {
            if (Utility.isOnline(getActivity())) {
                mOfflineView.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
                mRecyclerView.setEmptyView(mEmptyView);
            } else {
                mOfflineView.setVisibility(View.VISIBLE);
                mEmptyView.setVisibility(View.GONE);
                mRecyclerView.setEmptyView(mOfflineView);
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser){
            configureEmptyView();
        }
    }

    public static class TutorialsViewHolder extends ViewHolder {
        static final String default_url = "https://firebasestorage.googleapis.com/v0/b/preachers-club.appspot.com/o/images%2Ftutorials%2Fe834b00e20f5093ed95c4518b7494393e470e1d704b0154893f8c97ea1ecb4_640.jpg?alt=media&token=dea44dca-cb68-42dc-b5b6-19fd90b7a7b2";
        CardView mCardView;
        private ImageView mPhoto;
        private TextView mSubTitle;
        private TextView mTitle;

        public TutorialsViewHolder(View itemView) {
            super(itemView);
            this.mTitle = (TextView) itemView.findViewById(R.id.tutorial_title_text_view);
            this.mSubTitle = (TextView) itemView.findViewById(R.id.tutorial_sub_title_text_view);
            this.mPhoto = (ImageView) itemView.findViewById(R.id.tutorial_photo_image_view);
            this.mCardView = (CardView) itemView.findViewById(R.id.tutorial_card_view);
        }

        public void setTitle(String title) {
            this.mTitle.setText(title);
        }

        public void setSubTitle(String subtitle) {
            this.mSubTitle.setText(subtitle);
        }

        public void setImage(String url) {
            if (url == null) {
                url = default_url;
            } else if (url.isEmpty()) {
                url = default_url;
            }

//            StorageReference reference = FirebaseStorage
//                    .getInstance()
//                    .getReference()
//                    .child(Utility.REFERENCE_TUTORIAL_IMAGES)
//                    .child(url);
//
//            Glide.with(mPhoto.getContext())
//                    .using(new FirebaseImageLoader())
//                    .load(reference)
//                    .centerCrop()
//                    .into(mPhoto);

            Glide.with(this.mPhoto.getContext()).load(url).centerCrop().into(this.mPhoto);

            /*
            StorageReference ref = FirebaseStorage.getInstance().getReference().child("myimage");
 *     ImageView iv = (ImageView) findViewById(R.id.my_image_view);
 *
 *     Glide.with(this)
 *         .using(new FirebaseImageLoader())
 *         .load(ref)
 *         .into(iv);
             */
        }
    }
}
