package com.apwright.preachersclub.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Sermon;
import com.firebase.ui.auth.ResultCodes;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by apwright on 8/13/2017.
 */

public class SermonSupportingPointEditFragment extends Fragment {

    private TextInputLayout mExampleLayout;
    private TextInputLayout mApplicationLayout;
    private TextInputLayout mOutcomeLayout;
    private TextInputLayout mReferenceLayout;

    private TextInputEditText mExample;
    private TextInputEditText mApplication;
    private TextInputEditText mOutcome;
    private TextInputEditText mReference;

    private Button mSave;
    private Button mCancel;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mDatabaseReference;

    private Sermon mSermon;
    private String mFireBaseKey;
    private int mPosition;

    private boolean mEditMode;

    private Context mContext;

    public SermonSupportingPointEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_sermon_supporting_point_edit, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();

        mContext = getContext();
        mSermon = new Sermon();

        mExampleLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_supporting_point_edit_example_text_input_layout);
        mApplicationLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_supporting_point_edit_application_text_input_layout);
        mOutcomeLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_supporting_point_edit_outcome_text_input_layout);
        mReferenceLayout = (TextInputLayout) rootView.findViewById(R.id.sermon_supporting_point_edit_reference_text_input_layout);

        mExample = (TextInputEditText) rootView.findViewById(R.id.sermon_supporting_point_edit_example_text_view);
        mApplication = (TextInputEditText) rootView.findViewById(R.id.sermon_supporting_point_edit_application_text_view);
        mOutcome = (TextInputEditText) rootView.findViewById(R.id.sermon_supporting_point_edit_outcome_text_view);
        mReference = (TextInputEditText) rootView.findViewById(R.id.sermon_supporting_point_edit_reference_text_view);

        mSave = (Button) rootView.findViewById(R.id.sermon_supporting_point_edit_save_button);
        mCancel = (Button) rootView.findViewById(R.id.sermon_supporting_point_edit_cancel_button);


        mSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (saveData()) {
                    //save and close activity
                    Intent resultIntent = new Intent();
                    resultIntent.putExtra(Sermon.SERMON_KEY, mSermon);
                    resultIntent.putExtra(Sermon.SERMON_FIRE_BASE_KEY, mFireBaseKey);
                    getActivity().setResult(ResultCodes.OK, resultIntent);
                    getActivity().finish();
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_KEY)) {
            mSermon = (Sermon) getActivity().getIntent().getExtras().getParcelable(Sermon.SERMON_KEY);
        }

        if (getActivity().getIntent().hasExtra(Sermon.SERMON_FIRE_BASE_KEY)) {
            mFireBaseKey = (String) getActivity().getIntent().getStringExtra(Sermon.SERMON_FIRE_BASE_KEY);
        }

        if (getActivity().getIntent().hasExtra("EditMode")) {
            if (mSermon != null) {

                mEditMode = true;
                mPosition = (Integer) getActivity().getIntent().getIntExtra("EditMode", 0);

                //load data
                Sermon.SupportingPoint supportingPoint = mSermon.getSupportingPointList().get(mPosition);
                mExample.setText(supportingPoint.getDescription());
                mApplication.setText(supportingPoint.getApplication());
                mOutcome.setText(supportingPoint.getOutcome());
                mReference.setText(supportingPoint.getReference());
            }

        }

        return rootView;
    }

    private boolean saveData() {
        if (mUser == null) {
            Utility.showSnackBar(getView(), "Sign in to add Supporting Point");
            return false;
        }

        if (!validateFields()) {
            return false;
        }

        String description = Utility.toTitleCase(mExample.getText().toString());
        String application = mApplication.getText().toString();
        String outcome = mOutcome.getText().toString();
        String reference = mReference.getText().toString();

        Sermon.SupportingPoint supportingPoint = new Sermon.SupportingPoint();
        supportingPoint.setDescription(description);
        supportingPoint.setApplication(application);
        supportingPoint.setOutcome(outcome);
        supportingPoint.setReference(reference);

        if (mEditMode) {
            //update supporting point
            mSermon.setSupportingPointByPosition(supportingPoint, mPosition);
        } else {
            //add supporting point
            mSermon.addSupportingPoint(supportingPoint);
        }


        if (!mFireBaseKey.isEmpty()) {
            mDatabase.getReference().child(Utility.REFERENCE_SERMONS).child(mFireBaseKey).setValue(mSermon);
            Utility.showToast(getContext(), "Sermon " + mSermon.getSermonTitle() + " updated");
        }


        return true;
    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mExampleLayout.setError(null);
        this.mApplicationLayout.setError(null);
        this.mOutcomeLayout.setError(null);
        this.mReferenceLayout.setError(null);

        if (mExample.getText().toString().isEmpty()) {
            fieldsValid = false;
            mExampleLayout.setError("Please describe your supporting point");
        }

        if (mApplication.getText().toString().isEmpty()) {
            fieldsValid = false;
            mApplicationLayout.setError("Please explain how this point can be applied");
        }

        if (mOutcome.getText().toString().isEmpty()) {
            fieldsValid = false;
            mOutcomeLayout.setError("Please explain what will happen when this point is applied");
        }

        return fieldsValid;
    }
}
