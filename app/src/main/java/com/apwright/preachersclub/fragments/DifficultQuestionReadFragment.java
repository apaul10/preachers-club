package com.apwright.preachersclub.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.ShareCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.apwright.preachersclub.DifficultQuestionEditActivity;
import com.apwright.preachersclub.DifficultQuestionReadActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.DifficultQuestion;
import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

/**
 * Created by apwright on 6/27/2017.
 */

public class DifficultQuestionReadFragment extends Fragment {

    private ImageView mPhoto;
    private TextView mSummary;
    private TextView mScripture;
    private TextView mDetails;

    private ImageButton mLoveImageButton;
    private ImageButton mShareImageButton;
    private ImageButton mChatImageButton;
    private ImageButton mNotesImageButton;
    private ImageButton mEditImageButton;

    private DifficultQuestion mDifficultQuestion;

    private CollapsingToolbarLayout mCollapsingToolbarLayout;
    private Toolbar mToolbar;
    private DifficultQuestionReadActivity mActivity;

    private String mFireBaseKey;

    private RoundedImageView mContributorPhoto;
    private TextView mContributor;
    private TextView mContributorBio;

    public DifficultQuestionReadFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_difficult_question_read, container, false);

        mActivity = (DifficultQuestionReadActivity) getActivity();
        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) rootView.findViewById(R.id.collapsingToolbar);

        mActivity.setSupportActionBar(mToolbar);
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mLoveImageButton = (ImageButton) rootView.findViewById(R.id.difficult_question_read_love_image_button);
        mEditImageButton = (ImageButton) rootView.findViewById(R.id.difficult_question_read_edit_image_button);
        mChatImageButton = (ImageButton) rootView.findViewById(R.id.difficult_question_read_chat_image_button);
        mNotesImageButton = (ImageButton) rootView.findViewById(R.id.difficult_question_read_note_image_button);
        mShareImageButton = (ImageButton) rootView.findViewById(R.id.difficult_question_read_share_image_button);

        mPhoto = (ImageView) rootView.findViewById(R.id.difficult_question_read_photo_image_view);
        mSummary = (TextView) rootView.findViewById(R.id.difficult_question_read_summary_text_view);
        mScripture = (TextView) rootView.findViewById(R.id.difficult_question_read_scriptures_date_text_view);
        mDetails = (TextView) rootView.findViewById(R.id.difficult_question_read_details_text_view);

        mContributorPhoto = (RoundedImageView) rootView.findViewById(R.id.about_contributor_photo_rounded_image_view);
        mContributor = (TextView) rootView.findViewById(R.id.about_contributor_name_text_view);
        mContributorBio = (TextView) rootView.findViewById(R.id.about_contributor_bio_text_view);

        if (getActivity().getIntent().hasExtra(DifficultQuestion.DIFFICULT_QUESTION_KEY)) {
            mDifficultQuestion = (DifficultQuestion) getActivity().getIntent().getExtras().getParcelable(DifficultQuestion.DIFFICULT_QUESTION_KEY);
        }

        if (getActivity().getIntent().hasExtra(DifficultQuestion.DIFFICULT_QUESTION_FIRE_BASE_KEY)) {
            mFireBaseKey = (String) getActivity().getIntent().getStringExtra(DifficultQuestion.DIFFICULT_QUESTION_FIRE_BASE_KEY);
        }

        if (mDifficultQuestion != null) {
            loadContent();
        }

        mShareImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareDifficultQuestion();
            }
        });

        mEditImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editDifficultQuestion();
            }
        });

        mScripture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openKeyText();
            }
        });


        return rootView;
    }

    private void openKeyText() {
        if (mScripture.getText() == null) return;

        String verse = mScripture.getText().toString();

        //Toast.makeText(getContext(), verse, Toast.LENGTH_SHORT).show();

        if (!TextUtils.isEmpty(verse)) {
            Utility.openBibleVerse(getContext(), verse);
        }
    }

    private void editDifficultQuestion() {
        if (mDifficultQuestion != null) {
            Intent editDifficultQuestionIntent = new Intent(getActivity(), DifficultQuestionEditActivity.class);
            editDifficultQuestionIntent.putExtra(DifficultQuestion.DIFFICULT_QUESTION_KEY, mDifficultQuestion);
            editDifficultQuestionIntent.putExtra(DifficultQuestion.DIFFICULT_QUESTION_FIRE_BASE_KEY, mFireBaseKey);
            startActivity(editDifficultQuestionIntent);
        }
    }

    private void shareDifficultQuestion() {
        if (mDifficultQuestion != null) {

            Intent shareIntent = ShareCompat.IntentBuilder.from(getActivity())
                    .setEmailTo(new String[]{})
                    .setType("text/plain")
                    .setHtmlText(mDifficultQuestion.getShareContent(0))
                    .setText(mDifficultQuestion.getShareContent(300))
                    .setSubject(mDifficultQuestion.getShareContentSubject())
                    .getIntent();

            if (shareIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(Intent.createChooser(shareIntent, getString(R.string.share_question)));
            }

        }
    }

    private void loadContent() {

        mSummary.setText(mDifficultQuestion.getResponseSummary());
        mScripture.setText(mDifficultQuestion.getBibleVerses().toString());
        mDetails.setText(mDifficultQuestion.getResponseDetail());

        //update contributor stub
        Utility.getContributorByID(getContext(),  mDifficultQuestion.getContributorID(), mContributor, mContributorBio, mContributorPhoto);

        mCollapsingToolbarLayout.setTitle(mDifficultQuestion.getQuestionDescription());
        //mCollapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        Glide.with(this.mPhoto.getContext())
                .load(mDifficultQuestion.getPhotoURL())
                .centerCrop()
                .into(mPhoto);
    }
}
