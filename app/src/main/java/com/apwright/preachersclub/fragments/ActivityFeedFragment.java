package com.apwright.preachersclub.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.apwright.preachersclub.AssignmentReadActivity;
import com.apwright.preachersclub.DifficultQuestionReadActivity;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.TutorialReadActivity;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.ActivityFeed;
import com.apwright.preachersclub.model.Assignment;
import com.apwright.preachersclub.model.Contributor;
import com.apwright.preachersclub.model.DifficultQuestion;
import com.apwright.preachersclub.model.EmptyRecyclerView;
import com.apwright.preachersclub.model.Tutorial;
import com.apwright.preachersclub.model.UsefulResource;
import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.gson.Gson;
import com.makeramen.roundedimageview.RoundedImageView;

/**
 * Created by apwright on 6/11/2017.
 */

public class ActivityFeedFragment extends Fragment {

    private FirebaseDatabase mDatabase;

    private EmptyRecyclerView mUsefulResourceRecyclerView;
    private EmptyRecyclerView mAssignmentsRecyclerView;
    private EmptyRecyclerView mDifficultQuestionsRecyclerView;
    private RecyclerView mActivityFeedRecyclerView;

    private FirebaseRecyclerAdapter mUsefulResourceAdapter;
    private FirebaseRecyclerAdapter mAssignmentsAdapter;
    private FirebaseRecyclerAdapter mDifficultQuestionsAdapter;
    private FirebaseRecyclerAdapter mActivityFeedAdapter;

    private Query mUsefulResourceQuery;
    private Query mAssignmentsQuery;
    private Query mDifficultQuestionsQuery;
    private Query mActivityFeedQuery;

    public ActivityFeedFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_activity_feed, container, false);

        mDatabase = FirebaseDatabase.getInstance();

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //load horizontal recycler views
        loadUsefulResources();
        loadAssignments();
        loadDifficultQuestions();
        loadActivityFeed();

    }

    private void loadUsefulResources() {
        //useful resources recycler view
        mUsefulResourceRecyclerView = (EmptyRecyclerView) getActivity().findViewById(R.id.useful_resources_recycler_view);
        mUsefulResourceRecyclerView.setHasFixedSize(true);
        mUsefulResourceRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mUsefulResourceQuery = mDatabase.getReference().child(Utility.REFERENCE_USEFUL_RESOURCES).orderByChild("submissionDateReversed");
        mUsefulResourceAdapter = new FirebaseRecyclerAdapter<UsefulResource, UsefulResourceViewHolder>(
                UsefulResource.class,
                R.layout.item_useful_resource,
                UsefulResourceViewHolder.class,
                mUsefulResourceQuery
        ) {
            @Override
            protected void populateViewHolder(UsefulResourceViewHolder viewHolder, final UsefulResource model, int position) {

                viewHolder.setTitle(model.getResourceTitle());
                viewHolder.setImage(model.getResourceImageURL());
                viewHolder.mLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent openResource = new Intent(Intent.ACTION_VIEW, Uri.parse(model.getResourceURL()));
                        if (openResource.resolveActivity(getActivity().getPackageManager()) != null) {
                            String shareMessageTitle = "";
                            if (model.getResourceTitle().length() > 50) {
                                shareMessageTitle = "Open Useful Resource with";
                            } else {
                                shareMessageTitle = "Open " + model.getResourceTitle() + " with";
                            }
                            startActivity(Intent.createChooser(openResource, shareMessageTitle));
                        }
                    }
                });

            }
        };
        mUsefulResourceRecyclerView.setAdapter(mUsefulResourceAdapter);
        View emptyView = getActivity().findViewById(R.id.useful_resource_empty_view);
        mUsefulResourceRecyclerView.setEmptyView(emptyView);

    }

    private void loadAssignments() {
        //assignments recycler view
        mAssignmentsRecyclerView = (EmptyRecyclerView) getActivity().findViewById(R.id.assignments_recycler_view);
//        mAssignmentsRecyclerView.setHasFixedSize(true);
        mAssignmentsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        long startDate = System.currentTimeMillis() - 604800000L; //up to 7 days ago : 7 * 24 * 60 * 60 * 1000
        //mAssignmentsQuery = mDatabase.getReference().child(Utility.REFERENCE_ASSIGNMENTS).orderByChild("dueDate").startAt(startDate);
        mAssignmentsQuery = mDatabase.getReference().child(Utility.REFERENCE_ASSIGNMENTS).orderByChild("dueDate");
        mAssignmentsAdapter = new FirebaseRecyclerAdapter<Assignment, AssignmentViewHolder>(
                Assignment.class,
                R.layout.item_assignment,
                AssignmentViewHolder.class,
                mAssignmentsQuery
        ) {
            @Override
            protected void populateViewHolder(final AssignmentViewHolder viewHolder, final Assignment model, int position) {

                final String fireBaseKey = getRef(position).getKey();

                viewHolder.setAssignmentName(model.getAssignmentName());
                viewHolder.setDescription(model.getAssignmentDescription());
                viewHolder.setDueDate(model.getFormattedDueDate());

                int randomColor = Utility.getRandomColor(80);
                viewHolder.mDescription.setBackgroundColor(randomColor);
                ImageViewCompat.setImageTintList(viewHolder.mIcon, ColorStateList.valueOf(Utility.getRandomColor(95)));

                viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), AssignmentReadActivity.class);
                        intent.putExtra(Assignment.ASSIGNMENT_KEY, model);
                        intent.putExtra(Assignment.ASSIGNMENT_FIRE_BASE_KEY, fireBaseKey);
                        //configure scene transitions
                        Pair<View, String> pairName = Pair.create((View) viewHolder.mName, "assignmentName");
                        Pair<View, String> pairDueDate = Pair.create((View) viewHolder.mDueDate, "assignmentDueDate");
                        Pair<View, String> pairDescription = Pair.create((View) viewHolder.mDescription, "assignmentDescription");
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairName, pairDueDate, pairDescription);
                        getActivity().startActivity(intent, options.toBundle());
                    }
                });

            }
        };
        mAssignmentsRecyclerView.setAdapter(mAssignmentsAdapter);
        View emptyView = getActivity().findViewById(R.id.assignments_empty_view);
        mAssignmentsRecyclerView.setEmptyView(emptyView);
    }

    private void loadDifficultQuestions() {
        //difficult questions recycler view
        mDifficultQuestionsRecyclerView = (EmptyRecyclerView) getActivity().findViewById(R.id.difficult_questions_recycler_view);
        mDifficultQuestionsRecyclerView.setHasFixedSize(true);
        mDifficultQuestionsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mDifficultQuestionsQuery = mDatabase.getReference().child(Utility.REFERENCE_QUESTIONS).orderByChild("dateUpdatedReversed");
        mDifficultQuestionsAdapter = new FirebaseRecyclerAdapter<DifficultQuestion, QuestionViewHolder>(
                DifficultQuestion.class,
                R.layout.item_question,
                QuestionViewHolder.class,
                mDifficultQuestionsQuery
        ) {
            @Override
            protected void populateViewHolder(final QuestionViewHolder viewHolder, final DifficultQuestion model, int position) {

                final String fireBaseKey = getRef(position).getKey();
                viewHolder.setImage(model.getPhotoURL());
                viewHolder.setTitle(model.getQuestionDescription());
                viewHolder.setSummaryResponse(model.getResponseSummary());
                viewHolder.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getContext(), DifficultQuestionReadActivity.class);
                        intent.putExtra(DifficultQuestion.DIFFICULT_QUESTION_KEY, model);
                        intent.putExtra(DifficultQuestion.DIFFICULT_QUESTION_FIRE_BASE_KEY, fireBaseKey);
                        //configure scene transitions
                        Pair<View, String> pairQuestion = Pair.create((View) viewHolder.mTitle, "questionDescription");
                        Pair<View, String> pairImage = Pair.create((View) viewHolder.mQuestionImage, "questionImage");
                        Pair<View, String> pairSummary = Pair.create((View) viewHolder.mSummaryResponse, "questionSummary");
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(getActivity(), pairQuestion, pairImage, pairSummary);
                        getActivity().startActivity(intent, options.toBundle());
                    }
                });
            }
        };
        mDifficultQuestionsRecyclerView.setAdapter(mDifficultQuestionsAdapter);
        View emptyView = getActivity().findViewById(R.id.difficult_questions_empty_view);
        mDifficultQuestionsRecyclerView.setEmptyView(emptyView);
    }

    private void loadActivityFeed() {
        //activity feed recycler view
        mActivityFeedRecyclerView = (RecyclerView) getActivity().findViewById(R.id.activity_feed_recycler_view);
        mActivityFeedRecyclerView.setHasFixedSize(true);
        mActivityFeedRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mActivityFeedQuery = mDatabase.getReference().child(Utility.REFERENCE_ACTIVITY_FEED).orderByChild("dateCreatedReversed");
        mActivityFeedAdapter = new FirebaseRecyclerAdapter(
                ActivityFeed.class, R.layout.item_tutorial, ActivityFeedViewHolder.class, mActivityFeedQuery
        ) {
            @Override
            protected void populateViewHolder(RecyclerView.ViewHolder viewHolder, final Object model, int position) {
                ActivityFeed activityFeed = (ActivityFeed) model;
                ActivityFeedViewHolder activityFeedViewHolder = (ActivityFeedViewHolder) viewHolder;
                String jsonObject = new Gson().toJson(activityFeed.getFeedContent());

                switch (activityFeed.getActivityFeedType()) {
                    case TUTORIAL:
                        Tutorial tutorial = new Gson().fromJson(jsonObject, Tutorial.class);
                        activityFeedViewHolder.bindTutorial(tutorial, activityFeed.getFireBaseKey(), getActivity());
                        break;
                    case CONTRIBUTOR:
                        Contributor contributor = new Gson().fromJson(jsonObject, Contributor.class);
                        activityFeedViewHolder.bindContributor(contributor, activityFeed.getFireBaseKey(), getActivity());
                        break;
                }
            }

            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                switch (viewType) {
                    case ActivityFeed.TUTORIAL:
                        View tutorialView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tutorial, parent, false);
                        return new ActivityFeedViewHolder(tutorialView);
                    case ActivityFeed.CONTRIBUTOR:
                        View contributorView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contributor, parent, false);
                        return new ActivityFeedViewHolder(contributorView);
                }
                return super.onCreateViewHolder(parent, viewType);
            }

            @Override
            public int getItemViewType(int position) {
                ActivityFeed activityFeed = (ActivityFeed) getItem(position);
                return activityFeed.getActivityFeedTypeOrdinal();
            }
        };
        mActivityFeedRecyclerView.setAdapter(mActivityFeedAdapter);

    }

    public static class UsefulResourceViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mLayout;
        private ImageView mResourceImage;
        private TextView mTitle;

        public UsefulResourceViewHolder(View itemView) {
            super(itemView);

            mResourceImage = (ImageView) itemView.findViewById(R.id.useful_resource_image_view);
            mTitle = (TextView) itemView.findViewById(R.id.useful_resource_title_text_view);
            mLayout = (LinearLayout) itemView.findViewById(R.id.useful_resource_layout);
        }

        public void setImage(String url) {
            if (url != null) {
                if (!url.isEmpty()) {
                    Glide.with(this.mResourceImage.getContext()).load(url).centerCrop().into(this.mResourceImage);
                }
            }
        }

        public void setTitle(String title) {
            if (title != null) {
                if (!title.isEmpty()) {
                    mTitle.setText(title);
                }
            }
        }
    }

    public static class AssignmentViewHolder extends RecyclerView.ViewHolder {
        LinearLayout mLayout;
        CardView mCardView;
        private TextView mName;
        TextView mDescription;
        private TextView mDueDate;
        ImageView mIcon;

        public AssignmentViewHolder(View itemView) {
            super(itemView);
            mLayout = (LinearLayout) itemView.findViewById(R.id.assignment_layout);
            mCardView = (CardView) itemView.findViewById(R.id.assignment_card_view);
            mName = (TextView) itemView.findViewById(R.id.assignment_name_text_view);
            mDescription = (TextView) itemView.findViewById(R.id.assignment_description_text_view);
            mDueDate = (TextView) itemView.findViewById(R.id.assignment_due_date_text_view);
            mIcon = (ImageView) itemView.findViewById(R.id.assignment_icon_image_view);

        }

        public void setAssignmentName(String name) {
            if (name != null) {
                if (!name.isEmpty()) {
                    mName.setText(name);
                }
            }
        }

        public void setDescription(String description) {
            if (description != null) {
                if (!description.isEmpty()) {
                    mDescription.setText(description);
                }
            }
        }

        public void setDueDate(String dueDate) {
            if (dueDate != null) {
                if (!dueDate.isEmpty()) {
                    mDueDate.setText(dueDate);
                }
            }
        }

    }

    public static class QuestionViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        private ImageView mQuestionImage;
        private TextView mTitle;
        private TextView mSummaryResponse;

        public QuestionViewHolder(View itemView) {
            super(itemView);

            mCardView = (CardView) itemView.findViewById(R.id.question_card_view);
            mQuestionImage = (ImageView) itemView.findViewById(R.id.question_image_view);
            mTitle = (TextView) itemView.findViewById(R.id.question_title_text_view);
            mSummaryResponse = (TextView) itemView.findViewById(R.id.question_summary_text_view);
        }

        public void setImage(String url) {
            if (url != null) {
                if (!url.isEmpty()) {
                    Glide.with(this.mQuestionImage.getContext()).load(url).centerCrop().into(this.mQuestionImage);
                }
            }
        }

        public void setTitle(String title) {
            mTitle.setText(title);
        }

        public void setSummaryResponse(String summaryResponse) {
            mSummaryResponse.setText(summaryResponse);
        }

    }

    public static class ActivityFeedViewHolder extends RecyclerView.ViewHolder {

        View mView;
        Context mContext;

        public ActivityFeedViewHolder(View itemView) {
            super(itemView);
            mView = itemView;
            mContext = itemView.getContext();
        }

        public void bindTutorial(final Tutorial tutorial, final String fireBaseKey, final Activity activity) {
            final String default_url = "https://firebasestorage.googleapis.com/v0/b/preachers-club.appspot.com/o/images%2Ftutorials%2Fe834b00e20f5093ed95c4518b7494393e470e1d704b0154893f8c97ea1ecb4_640.jpg?alt=media&token=dea44dca-cb68-42dc-b5b6-19fd90b7a7b2";

            TextView mTitle = (TextView) itemView.findViewById(R.id.tutorial_title_text_view);
            TextView mSubTitle = (TextView) itemView.findViewById(R.id.tutorial_sub_title_text_view);
            ImageView mPhoto = (ImageView) itemView.findViewById(R.id.tutorial_photo_image_view);
            CardView mCardView = (CardView) itemView.findViewById(R.id.tutorial_card_view);

            String url = (tutorial.getPhotoURL().isEmpty() ? default_url : tutorial.getPhotoURL());

            mTitle.setText(tutorial.getTitle());
            mSubTitle.setText(tutorial.getSubTitle());
            Glide.with(mPhoto.getContext()).load(tutorial.getPhotoURL()).centerCrop().into(mPhoto);

            mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, TutorialReadActivity.class);
                    intent.putExtra(Tutorial.TUTORIAL_KEY, tutorial);
                    intent.putExtra(Tutorial.TUTORIAL_FIRE_BASE_KEY, fireBaseKey);
                    activity.startActivity(intent, ActivityOptionsCompat.makeSceneTransitionAnimation(activity, v, "tutorialImage").toBundle());
                }
            });
        }

        public void bindContributor(final Contributor contributor, final String fireBaseKey, final Activity activity) {

            RoundedImageView mPhoto = (RoundedImageView) itemView.findViewById(R.id.contributor_photo_image_view);
            CardView mCardView = (CardView) itemView.findViewById(R.id.contributor_card_view);
            TextView mName = (TextView) itemView.findViewById(R.id.contributor_name_text_view);
            TextView mBio = (TextView) itemView.findViewById(R.id.contributor_bio_text_view);

            Glide.with(mPhoto.getContext()).load(contributor.getContributorPhotoURL()).centerCrop().into(mPhoto);
            mName.setText(contributor.getContributorName());
            mBio.setText(contributor.getContributorShortBio());

        }
    }

    public static class ContributorViewHolder extends RecyclerView.ViewHolder {
        CardView mCardView;
        RoundedImageView mPhoto;
        TextView mName;
        TextView mBio;

        public ContributorViewHolder(View itemView) {
            super(itemView);
            mCardView = (CardView) itemView.findViewById(R.id.contributor_photo_image_view);
            mName = (TextView) itemView.findViewById(R.id.contributor_name_text_view);
            mBio = (TextView) itemView.findViewById(R.id.contributor_bio_text_view);
            mPhoto = (RoundedImageView) itemView.findViewById(R.id.contributor_photo_image_view);
        }
    }
}
