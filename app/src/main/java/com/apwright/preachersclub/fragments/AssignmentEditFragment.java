package com.apwright.preachersclub.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;


import com.apwright.preachersclub.R;
import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.Assignment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import android.app.DatePickerDialog.OnDateSetListener;
import android.text.format.Time;

/**
 * Created by apwright on 6/20/2017.
 */

public class AssignmentEditFragment extends Fragment {

    static final int DATE_DIALOG_ID = 123;
    public static final String DATE_FORMAT = "EEE, MMM d yyyy";
    private long mSelectedDate;
    private OnDateSetListener mOnDateSetListener;
    private long selectedDate;

    private TextInputLayout mNameLayout;
    private TextInputLayout mDueDateLayout;
    private TextInputLayout mDescriptionLayout;
    private TextInputEditText mName;
    private TextInputEditText mDueDate;
    private TextInputEditText mDescription;

    private Assignment mAssignment;

    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private FirebaseStorage mStorage;

    private String mFireBaseKey;

    private boolean mEditMode = false;

    private Context mContext;
    private View mView;

    public AssignmentEditFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_assignment_edit, container, false);

        setHasOptionsMenu(true);
        setRetainInstance(true);

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mStorage = FirebaseStorage.getInstance();

        mAssignment = new Assignment();
        mContext = getContext();
        mView = getView();

        mNameLayout = (TextInputLayout) rootView.findViewById(R.id.assignment_edit_name_text_input_layout);
        mDueDateLayout = (TextInputLayout) rootView.findViewById(R.id.assignment_edit_due_date_text_input_layout);
        mDescriptionLayout = (TextInputLayout) rootView.findViewById(R.id.assignment_edit_description_text_input_layout);
        mName = (TextInputEditText) rootView.findViewById(R.id.assignment_edit_name_text_view);
        mDueDate = (TextInputEditText) rootView.findViewById(R.id.assignment_edit_due_date_text_view);
        mDescription = (TextInputEditText) rootView.findViewById(R.id.assignment_edit_description_text_view);

        //check if edit mode is applicable
        if (getActivity().getIntent().hasExtra(Assignment.ASSIGNMENT_KEY) && getActivity().getIntent().hasExtra(Assignment.ASSIGNMENT_FIRE_BASE_KEY)) {

            Assignment assignment = (Assignment) getActivity().getIntent().getExtras().getParcelable(Assignment.ASSIGNMENT_KEY);
            if (assignment != null) {
                mAssignment = assignment;
                mEditMode = true;
                mFireBaseKey = (String) getActivity().getIntent().getStringExtra(Assignment.ASSIGNMENT_FIRE_BASE_KEY);

                mName.setText(mAssignment.getAssignmentName());
                mDueDate.setText(mAssignment.getDueDateAsString());
                mSelectedDate = mAssignment.getDueDate();
                mDescription.setText(mAssignment.getAssignmentDescription());

            }
        }

        mOnDateSetListener = new OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                Calendar calendar = new GregorianCalendar(year, month, dayOfMonth);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(AssignmentEditFragment.DATE_FORMAT);
                mSelectedDate = calendar.getTimeInMillis();
                Time time = new Time();
                time.setToNow();
                if (Time.getJulianDay(mSelectedDate, time.gmtoff) >= Time.getJulianDay(System.currentTimeMillis(), time.gmtoff)) {
                    mDueDate.setText(simpleDateFormat.format(calendar.getTime()));
                }
            }
        };


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //call date picker dialog when due date is selected
        mDueDate.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    Calendar c = Calendar.getInstance();
                    DatePickerDialog datePickerDialog = new DatePickerDialog(AssignmentEditFragment.this.getContext(),
                            AssignmentEditFragment.this.mOnDateSetListener, c.get(Calendar.YEAR),
                            c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

                    datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    datePickerDialog.show();
                }
                return true;
            }
        });

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_cancel, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean closeFragment = true;
        switch (item.getItemId()) {
            case R.id.menu_save:
                closeFragment = saveData();
                break;
        }
        if (closeFragment) {
            getActivity().finish();
        }

        return true;
    }

    private boolean saveData() {

        if (mUser == null) {
            Utility.showSnackBar(mView, "Sign in to add contributor");
            return false;
        }

        if (!validateFields()) {
            return false;
        }
        String name = Utility.toTitleCase(mName.getText().toString().trim());
        long dueDate = mSelectedDate;
        String description = this.mDescription.getText().toString().trim();
        String contributorEmail = mUser.getEmail();
        String contributorName = mUser.getDisplayName();

        mAssignment.setAssignmentName(name);
        mAssignment.setContributorName(contributorName);
        mAssignment.setDueDate(dueDate);
        mAssignment.setAssignmentDescription(description);
        mAssignment.setContributorEmail(contributorEmail);
        mAssignment.setContributorName(contributorName);
        mAssignment.setDateCreated(System.currentTimeMillis());


        if (!mEditMode) {
            //create new record
            mDatabase.getReference().child(Utility.REFERENCE_ASSIGNMENTS).push().setValue(mAssignment);
            Utility.showToast(getContext(), "Assignment " + mAssignment.getAssignmentName() + " added");

        } else {
            //update existing record
            mDatabase.getReference().child(Utility.REFERENCE_ASSIGNMENTS).child(mFireBaseKey).setValue(mAssignment);
            Utility.showToast(getContext(), "Assignment " + mAssignment.getAssignmentName() + " updated");
        }

        return true;
    }

    private boolean validateFields() {
        boolean fieldsValid = true;
        this.mNameLayout.setError(null);
        this.mDueDateLayout.setError(null);
        this.mDescriptionLayout.setError(null);

        if (this.mName.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mNameLayout.setError("Name is required");
        }
        if (this.mDueDate.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mDueDateLayout.setError("Due Date is required");
        }
        if (this.mDescription.getText().toString().isEmpty()) {
            fieldsValid = false;
            this.mDescriptionLayout.setError("Please state assignment requirements");
        }
        return fieldsValid;
    }
}
