package com.apwright.preachersclub;

import android.app.Application;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatDelegate;
import android.text.TextUtils;
import android.util.LruCache;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by apwright on 5/27/2017.
 */

public class PreachersApp extends Application {

    public static final String LOG_TAG = PreachersApp.class.getSimpleName();

    public static int currentTab = 0;
    public static boolean enableThemeChange = false;
    public static boolean setTitleForTabs = false;
    public static boolean enableSwipeOnHomePage = true;
    public static boolean closeFAB = true;

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private static PreachersApp mInstance;

    public static boolean enableOfflineStorage = true;

    @Override
    public void onCreate() {
        super.onCreate();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

        mInstance = this;
        mRequestQueue = getRequestQueue();
        mImageLoader = new ImageLoader(mRequestQueue,
                new ImageLoader.ImageCache() {
                    private final LruCache<String, Bitmap>
                            cache = new LruCache<String, Bitmap>(20);

                    @Override
                    public Bitmap getBitmap(String url) {
                        return cache.get(url);
                    }

                    @Override
                    public void putBitmap(String url, Bitmap bitmap) {
                        cache.put(url, bitmap);
                    }
                });

        if (enableOfflineStorage) {
            FirebaseDatabase.getInstance().setPersistenceEnabled(enableOfflineStorage);
        }
    }

    public static synchronized PreachersApp getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? LOG_TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(LOG_TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
