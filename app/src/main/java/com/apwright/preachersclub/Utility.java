package com.apwright.preachersclub;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v13.app.FragmentCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.URLUtil;
import android.widget.TextView;
import android.widget.Toast;

import com.apwright.preachersclub.model.Contributor;
import com.apwright.preachersclub.model.User;
import com.apwright.preachersclub.model.WebMetaData;
import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.makeramen.roundedimageview.RoundedImageView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.regex.Pattern;

/**
 * Created by apwright on 5/27/2017.
 */

public class Utility {

    private static int mTheme;

    public static String PERMISSION_CAMERA = "android.permission.CAMERA";
    public static String PERMISSION_EXTERNAL_STORAGE = "android.permission.READ_EXTERNAL_STORAGE";

    public final static int THEME_DEFAULT = 0;
    public final static int THEME_COLOR_SET_TWO = 1;
    public final static int THEME_COLOR_SET_THREE = 2;
    public final static int THEME_COLOR_SET_FOUR = 3;

    public final static String REFERENCE_USERS = "users";
    public final static String REFERENCE_TUTORIALS = "tutorials";
    public final static String REFERENCE_SERMONS = "sermons";
    public final static String REFERENCE_PRESENTATIONS = "presentations";
    public final static String REFERENCE_USEFUL_RESOURCES = "resources";
    public final static String REFERENCE_ASSIGNMENTS = "assignments";
    public final static String REFERENCE_ACTIVITY_FEED = "activity_feed";
    public final static String REFERENCE_QUESTIONS = "questions";
    public final static String REFERENCE_CONTRIBUTOR = "contributors";
    public final static String REFERENCE_TUTORIAL_IMAGES = "images/tutorials/";
    public final static String REFERENCE_CONTRIBUTOR_IMAGES = "images/contributors/";
    public final static String REFERENCE_ASSIGNMENT_IMAGES = "images/assignments/";
    public final static String REFERENCE_USEFUL_RESOURCE_IMAGES = "images/resources/";
    public final static String REFERENCE_QUESTIONS_IMAGES = "images/questions/";
    public final static String REFERENCE_SERMONS_IMAGES = "images/sermons/";

    public static void changeToTheme(Activity activity, int theme) {
        mTheme = theme;
        activity.finish();
        activity.startActivity(new Intent(activity, activity.getClass()));
//        activity.overridePendingTransition(0, 0);
        activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

//        activity.recreate();
//        activity.overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);

//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//            activity.getWindow().setExitTransition(null);
//            activity.recreate();
//            activity.getWindow().setEnterTransition(null);
//        }else{
//            activity.recreate();
//        }

    }

    public static void onActivityCreateSetTheme(Activity activity) {
        int themeID;

        switch (mTheme) {
            default:
            case THEME_DEFAULT:
                themeID = R.style.AppTheme;
                break;
            case THEME_COLOR_SET_TWO:
                themeID = R.style.AppTheme_ColorSet2;
                break;
            case THEME_COLOR_SET_THREE:
                themeID = R.style.AppTheme_ColorSet3;
                break;
            case THEME_COLOR_SET_FOUR:
                themeID = R.style.AppTheme_ColorSet4;
                break;
        }

        activity.setTheme(themeID);
    }

    public static void showSnackBar(View v, String message, boolean addDismiss) {
        if (v != null) {
            Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_INDEFINITE);
            if (addDismiss) {
                snackbar.setAction("DISMISS", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
            }
            snackbar.show();
        }
    }

    public static void showSnackBar(View v, String message) {
        if (v != null) {
            Snackbar snackbar = Snackbar.make(v, message, Snackbar.LENGTH_LONG);
            snackbar.show();
        }
    }

    public static void showToast(Context context, String message) {
        if (context != null) {
            Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        }
    }

    public static String toTitleCase(String str) {

        if (str == null) {
            return null;
        }

        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }

        return builder.toString();
    }

    public enum MetaDataType {TITLE, IMAGE, DESCRIPTION}

    public static WebMetaData getWebsiteMetaData(String url) {
        WebMetaData webMetaData = new WebMetaData();

        String title = "";
        String imageURL = "";
        String description = "";

        try {
            Document document = Jsoup.connect(url).get();

            Elements eTitle = document.select("meta[property=og:title]");
            Elements eImage = document.select("meta[property=og:image]");
            Elements eDescription = document.select("meta[property=og:description]");

            if (eTitle != null) {
                title = eTitle.attr("content");
            }

            if (eImage != null) {
                imageURL = eImage.attr("content");
            }

            if (eDescription != null) {
                description = eDescription.attr("content");
            }

            webMetaData = new WebMetaData(title, imageURL, description, url);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return webMetaData;
    }

    public static int getRandomColor(int alpha) {
        alpha = (alpha <= 0 ? 80 : alpha);
        Random rnd = new Random();
//        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
        return Color.argb(alpha, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }

    public static boolean isOnline(Context context) {

        if (context == null) {
            return false;
        }

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        return isConnected;
    }

    public static String getImageFileName(String url) {
        String name = url.substring(url.lastIndexOf('/') + 1, url.length());
        return name;
    }

    public static String getGenericFileName(String prefix, String fileType) {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        return prefix.toLowerCase().trim() + "_" + System.currentTimeMillis() + "_" + randomUUIDString + "." + fileType.toLowerCase().trim();
    }

    public static boolean isURLValid(String input) {
        try {
            URL url = new URL(input);
            return URLUtil.isValidUrl(input) && Patterns.WEB_URL.matcher(input).matches();
        } catch (MalformedURLException e) {

        }

        return false;
    }

    @TargetApi(16)
    public static boolean askForPermission(Context context, String permission, Integer requestCode, Fragment fragment) {
        if (Build.VERSION.SDK_INT < 23) {
            return true;
        }
        int checkSelfPermission = ContextCompat.checkSelfPermission(context, permission);
        context.getPackageManager();
        if (checkSelfPermission == 0) {
            return true;
        }
        Activity activity;
        String[] strArr;
        if (fragment.shouldShowRequestPermissionRationale(permission)) {
            activity = (Activity) context;
            strArr = new String[]{permission};
            strArr[0] = permission;
            //FragmentCompat.requestPermissions(fragment, strArr, requestCode.intValue());
            fragment.requestPermissions(strArr, requestCode.intValue());

        } else {
            activity = (Activity) context;
            strArr = new String[]{permission};
            strArr[0] = permission;
            fragment.requestPermissions(strArr, requestCode.intValue());
        }
        return false;
    }

    public static String getDateAsString(long inputDate, String date_format) {
        String formattedDate = "";

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(inputDate);

        Date date = calendar.getTime();

        SimpleDateFormat dateFormat = new SimpleDateFormat(date_format);

        formattedDate = dateFormat.format(date);

        return formattedDate;
    }

    public static void getContributorByEmail(Context context, String contributorEmail, final TextView mName, final TextView mBio, final RoundedImageView mPhoto) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Query query = database.getReference().child(Utility.REFERENCE_USERS).orderByChild("emailAddress").equalTo(contributorEmail);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    updateContributorStub(dataSnapshot, mName, mBio, mPhoto);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public static void getContributorByID(Context context, String contributorID, final TextView mName, final TextView mBio, final RoundedImageView mPhoto) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        Query query = database.getReference().child(Utility.REFERENCE_USERS).orderByChild("userId").equalTo(contributorID);
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    updateContributorStub(dataSnapshot, mName, mBio, mPhoto);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private static void updateContributorStub(DataSnapshot dataSnapshot, TextView mName, TextView mBio, RoundedImageView mPhoto) {
        User data = new User();
        for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
            data = snapshot.getValue(User.class);
        }
        mName.setText(data.getDisplayName());
        //mBio.setText("Information about the contributor will be placed here");

        if (data.getPhotoUrl() != null) {
            if (!data.getPhotoUrl().isEmpty()) {
                Glide.with(mPhoto.getContext())
                        .load(data.getPhotoUrl())
                        .centerCrop()
                        .into(mPhoto);
            }
        }
    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static void openBibleVerse(Context context, String verse) {
        if (verse == null || context == null) return;
        if (!verse.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse("bible_with_egw://path?passage=" + verse));

            //check if app is installed to receive intent
            if (intent.resolveActivity(context.getPackageManager()) != null) {
                context.startActivity(intent);
            } else {
                final String appPackageName = "com.tinashe.bible";
                try {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        }
    }


}
