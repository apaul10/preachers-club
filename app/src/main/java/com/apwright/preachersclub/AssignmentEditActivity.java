package com.apwright.preachersclub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

public class AssignmentEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assignment_edit);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Edit Assignment</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
