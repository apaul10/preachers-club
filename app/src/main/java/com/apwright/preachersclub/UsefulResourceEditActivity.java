package com.apwright.preachersclub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

public class UsefulResourceEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_useful_resource_edit);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Edit Useful Resource</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
