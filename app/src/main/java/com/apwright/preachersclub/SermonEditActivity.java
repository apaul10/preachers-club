package com.apwright.preachersclub;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;

public class SermonEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sermon_edit);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Sermon Builder</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
