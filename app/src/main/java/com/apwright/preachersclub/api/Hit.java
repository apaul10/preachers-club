package com.apwright.preachersclub.api;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Hit {

    @SerializedName("previewHeight")
    @Expose
    public Integer previewHeight;
    @SerializedName("likes")
    @Expose
    public Integer likes;
    @SerializedName("favorites")
    @Expose
    public Integer favorites;
    @SerializedName("tags")
    @Expose
    public String tags;
    @SerializedName("webformatHeight")
    @Expose
    public Integer webformatHeight;
    @SerializedName("views")
    @Expose
    public Integer views;
    @SerializedName("webformatWidth")
    @Expose
    public Integer webformatWidth;
    @SerializedName("previewWidth")
    @Expose
    public Integer previewWidth;
    @SerializedName("comments")
    @Expose
    public Integer comments;
    @SerializedName("downloads")
    @Expose
    public Integer downloads;
    @SerializedName("pageURL")
    @Expose
    public String pageURL;
    @SerializedName("previewURL")
    @Expose
    public String previewURL;
    @SerializedName("webformatURL")
    @Expose
    public String webformatURL;
    @SerializedName("imageWidth")
    @Expose
    public Integer imageWidth;
    @SerializedName("user_id")
    @Expose
    public Integer userId;
    @SerializedName("user")
    @Expose
    public String user;
    @SerializedName("type")
    @Expose
    public String type;
    @SerializedName("id")
    @Expose
    public Integer id;
    @SerializedName("userImageURL")
    @Expose
    public String userImageURL;
    @SerializedName("imageHeight")
    @Expose
    public Integer imageHeight;

    public Hit withPreviewHeight(Integer previewHeight) {
        this.previewHeight = previewHeight;
        return this;
    }

    public Hit withLikes(Integer likes) {
        this.likes = likes;
        return this;
    }

    public Hit withFavorites(Integer favorites) {
        this.favorites = favorites;
        return this;
    }

    public Hit withTags(String tags) {
        this.tags = tags;
        return this;
    }

    public Hit withWebformatHeight(Integer webformatHeight) {
        this.webformatHeight = webformatHeight;
        return this;
    }

    public Hit withViews(Integer views) {
        this.views = views;
        return this;
    }

    public Hit withWebformatWidth(Integer webformatWidth) {
        this.webformatWidth = webformatWidth;
        return this;
    }

    public Hit withPreviewWidth(Integer previewWidth) {
        this.previewWidth = previewWidth;
        return this;
    }

    public Hit withComments(Integer comments) {
        this.comments = comments;
        return this;
    }

    public Hit withDownloads(Integer downloads) {
        this.downloads = downloads;
        return this;
    }

    public Hit withPageURL(String pageURL) {
        this.pageURL = pageURL;
        return this;
    }

    public Hit withPreviewURL(String previewURL) {
        this.previewURL = previewURL;
        return this;
    }

    public Hit withWebformatURL(String webformatURL) {
        this.webformatURL = webformatURL;
        return this;
    }

    public Hit withImageWidth(Integer imageWidth) {
        this.imageWidth = imageWidth;
        return this;
    }

    public Hit withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public Hit withUser(String user) {
        this.user = user;
        return this;
    }

    public Hit withType(String type) {
        this.type = type;
        return this;
    }

    public Hit withId(Integer id) {
        this.id = id;
        return this;
    }

    public Hit withUserImageURL(String userImageURL) {
        this.userImageURL = userImageURL;
        return this;
    }

    public Hit withImageHeight(Integer imageHeight) {
        this.imageHeight = imageHeight;
        return this;
    }

}