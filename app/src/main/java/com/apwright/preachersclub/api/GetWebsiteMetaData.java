package com.apwright.preachersclub.api;

import android.os.AsyncTask;

import com.apwright.preachersclub.Utility;
import com.apwright.preachersclub.model.WebMetaData;

/**
 * Created by apwright on 6/24/2017.
 */

public class GetWebsiteMetaData extends AsyncTask<String, String, WebMetaData> {

    @Override
    protected WebMetaData doInBackground(String... params) {
        String webURL = params[0];
        WebMetaData metaData = Utility.getWebsiteMetaData(webURL);
        return metaData;
    }

}
