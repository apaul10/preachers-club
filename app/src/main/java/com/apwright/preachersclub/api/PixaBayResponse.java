package com.apwright.preachersclub.api;

import com.apwright.preachersclub.model.ImageContent;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PixaBayResponse {

    @SerializedName("totalHits")
    @Expose
    public Integer totalHits;
    @SerializedName("hits")
    @Expose
    public List<Hit> hits = null;
    @SerializedName("total")
    @Expose
    public Integer total;

    public PixaBayResponse withTotalHits(Integer totalHits) {
        this.totalHits = totalHits;
        return this;
    }

    public PixaBayResponse withHits(List<Hit> hits) {
        this.hits = hits;
        return this;
    }

    public PixaBayResponse withTotal(Integer total) {
        this.total = total;
        return this;
    }

    public ArrayList<ImageContent> convertToImageContentList(PixaBayResponse response){
        ArrayList<ImageContent> imageContents = new ArrayList<ImageContent>();

        for (Hit hit: response.hits){
            ImageContent imageContent = new ImageContent();

            imageContent.setImageID(hit.id.toString());
            imageContent.setImageName(hit.id.toString());
            imageContent.setPreviewURL(hit.webformatURL);
            imageContent.setThumbnailURL(hit.previewURL);

            imageContents.add(imageContent);

        }

        return imageContents;
    }

}
