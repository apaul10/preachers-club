package com.apwright.preachersclub.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.apwright.preachersclub.PreachersApp;
import com.apwright.preachersclub.R;
import com.apwright.preachersclub.model.ImageContent;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by apwright on 6/6/2017.
 */

public class ImageContentAdapter extends RecyclerView.Adapter<ImageContentAdapter.ViewHolder> {

    private ArrayList<ImageContent> mImageContents;
    private Context mContext;
    ImageLoader mImageLoader;
    RequestQueue mRequestQueue;
    private Random mRandom = new Random();

    public ImageContentAdapter(Context mContext, ArrayList<ImageContent> mImageContents) {
        this.mImageContents = mImageContents;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo_chooser, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        final ImageContent selectedContent = mImageContents.get(position);

        mImageLoader = PreachersApp.getInstance().getImageLoader();

        String url = selectedContent.getThumbnailURL();

        mImageLoader.get(url, ImageLoader.getImageListener(holder.mPhoto, 0, 0));

//        holder.mPhoto.getLayoutParams().height = getRandomIntInRange(1,4);
        holder.mPhoto.setImageUrl(url, mImageLoader);

        holder.mPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(mContext, "Image Selected: " + position, Toast.LENGTH_LONG).show();
                MaterialDialog materialDialog = new MaterialDialog.Builder(mContext)
                        .title("Confirm Image Selection")
                        .content("Do you wish to use this image as a header for your content")
                        .positiveText("Agree")
                        .negativeText("Disagree")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                //store results to be returned
                                Intent data = new Intent();
                                data.putExtra(ImageContent.IMAGE_CONTENT_KEY ,selectedContent);
                                //set the result of the activity, and add the data
                                ((Activity)mContext).setResult(Activity.RESULT_OK, data);
                                //close the activity
                                ((Activity)mContext).finish();
                            }
                        })
                        .build();

                materialDialog.show();

            }
        });

    }

    // Custom method to get a random number between a range
    protected int getRandomIntInRange(int max, int min){
        int randomInt = mRandom.nextInt((max-min)+min)+min;
        int randomValue = 130 + (randomInt * 20);

//        return mRandom.nextInt((max-min)+min)+min;
        return randomValue;
    }

    // Custom method to generate random HSV color
    protected int getRandomHSVColor(){
        // Generate a random hue value between 0 to 360
        int hue = mRandom.nextInt(361);
        // We make the color depth full
        float saturation = 1.0f;
        // We make a full bright color
        float value = 1.0f;
        // We avoid color transparency
        int alpha = 255;
        // Finally, generate the color
        int color = Color.HSVToColor(alpha, new float[]{hue, saturation, value});
        // Return the color
        return color;
    }

    @Override
    public int getItemCount() {
        return mImageContents.size();
    }

    public void add(ArrayList<ImageContent> imageContents){
        mImageContents.addAll(imageContents);
        notifyDataSetChanged();
    }

    public void update(ArrayList<ImageContent> imageContents){
        mImageContents.clear();
        mImageContents.addAll(imageContents);
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        NetworkImageView mPhoto;
        public ViewHolder(View itemView) {
            super(itemView);
            mPhoto = (NetworkImageView) itemView.findViewById(R.id.photo_network_image_view);
        }
    }
}
