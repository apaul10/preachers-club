package com.apwright.preachersclub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

public class DifficultQuestionEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficult_question_edit);
        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Edit Difficult Question</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
