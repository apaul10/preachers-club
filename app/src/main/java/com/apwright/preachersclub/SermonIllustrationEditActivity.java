package com.apwright.preachersclub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;

public class SermonIllustrationEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sermon_illustration_edit);

        getSupportActionBar().setTitle(Html.fromHtml("<font color='#ffffff'>Add Sermon Illustration</font>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
